TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp
QMAKE_CXXFLAGS += -std=c++0x

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/release/ -lLibRes
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/debug/ -lLibRes
else:unix: LIBS += -L$$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/ -lLibRes

INCLUDEPATH += $$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/debug
DEPENDPATH += $$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/debug

INCLUDEPATH += ./LibRes

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/release/libLibRes.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/debug/libLibRes.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/release/LibRes.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/debug/LibRes.lib
else:unix: PRE_TARGETDEPS += $$PWD/build-LibRes-Desktop_Qt_5_11_2_MinGW_32bit-Debug/libLibRes.a
