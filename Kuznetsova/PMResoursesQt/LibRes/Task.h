#pragma once
#include "PlannedAss.h"
#include "RealAss.h"
#include <vector>

class Task

{
protected:

	string name;
	int index;
    vector<RealAss> real;
    vector<PlannedAss> plan;
public:
    Task(int indexx = 0, string namex = "no_name") :name(namex), index(indexx), real(vector<RealAss>(0)), plan(vector<PlannedAss>(0)) {}
    Task(const Task &g) :name(g.name),index(g.index), real(vector<RealAss>(g.real.size()) = g.real), plan(vector<PlannedAss>(g.plan.size()) = g.plan) {}
	~Task();

	int getIndex() { return index; }
	string getName() { return name; }
    vector<RealAss> getReal()
	{
        return vector<RealAss>(real.size()) = real;
	}
    vector<PlannedAss> getPlanned()
	{
        return vector<PlannedAss>(plan.size()) = plan;
	}

	int setIndex(int indexx)
	{
		index = indexx;
		return 0;
	}
	int setName(string namex)
	{
		name = namex;
		return 0;
	}


    int addReal(Resourse * resoursex, int ammountx);
	int deactivateReal(RealAss* em);
	int changeReal(RealAss* em,int ammount);
    int addPlanned(Resourse * resoursex, int ammountx);
	int changePlanned(PlannedAss* em, int ammount);
	int deletePlanned(PlannedAss* em);
	std::ostream & print(std::ostream & os) const;
	friend std::ostream& operator <<(std::ostream&, const Task &);

};
