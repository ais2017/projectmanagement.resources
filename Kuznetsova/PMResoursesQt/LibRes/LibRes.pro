#-------------------------------------------------
#
# Project created by QtCreator 2018-11-12T21:13:57
#
#-------------------------------------------------

QT       -= gui

TARGET = LibRes

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -std=c++0x
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Resourse.cpp \
    Money.cpp \
    Group.cpp \
    Materials.cpp \
    PlannedAss.cpp \
    Project.cpp \
    RealAss.cpp \
    Staff.cpp \
    Task.cpp \
    Resourse.cpp

HEADERS += \
    Money.h \
    Group.h \
    Materials.h \
    PlannedAss.h \
    Project.h \
    RealAss.h \
    Staff.h \
    Task.h \
    Resourse.h \


