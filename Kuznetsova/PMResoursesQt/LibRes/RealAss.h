#pragma once
#include <string>
#include <iostream>
//using std::string;
#include "Resourse.h"

class RealAss // Real Assignment (�������� ���������� �������� )
{
protected:
    int  ammount;
	Resourse* resourse;
	bool isActive;
public:

	RealAss() : ammount(0), resourse(nullptr), isActive(0) {}
	RealAss(Resourse * resoursex, int ammountx) : ammount(ammountx), resourse(resoursex), isActive(1) {}
	RealAss(const RealAss &r) : ammount(r.ammount), resourse(r.resourse), isActive(r.isActive) {};
	~RealAss();


	int getAmmount() { return ammount; }
	Resourse* getResourse() { return resourse; }
	bool getIsActive() { return isActive; }

	int setAmmount(int ammountx)
	{
		if (ammountx <= 0)
		{
			throw logic_error("Negative ammount!");
			return 1;
		}
		ammount = ammountx;
		return 0;
	}
	int setResourse(Resourse* resoursex)
	{
		if(resoursex->getType() != 0)
		{
			resourse = resoursex;
			isActive = 1;
			return 0;
		}		
		throw logic_error("This resourse does not exist!");
		return 1;
	}
	int setIsActive(bool isActivex)
	{
		isActive = isActivex;
		return 0;
	}


	std::ostream & print(std::ostream &) const;

	friend std::ostream& operator <<(std::ostream&, const RealAss &);

};
