
#include <string>
#include <stdexcept>
#include <iostream>
#include <vector>

using std::string;
using namespace std;

#ifndef RESOURSE_H
#define RESOURSE_H


class Resourse
{
    protected:

    int typeR; // 0 - Resourse, 1 - Money, 2 - Materials, 3 - Staff, 4 - Group of people
    int idR;
    string nameR;
    int initnumber;

    public:

        Resourse(string nameRx = "no_name", int id = 0, int initnumberx = 0, int typeRx = 0);
        Resourse( int typeRx) :
         typeR(typeRx), idR(0), nameR("no_name"), initnumber(0) {}
        Resourse(const Resourse &r)
        {
            idR = r.idR;
            nameR = r.idR;

        }
        virtual ~Resourse();

        int changeAmmount(int n)
        {

            if ((typeR == 3) && (initnumber + n > 168))
            {
                throw logic_error("Too many working hours!");
                return initnumber;
            }

            if (initnumber + n >= 0)
            {
                return initnumber += n;
            }
            else
            {
                throw logic_error("Negative ammount!");
                return initnumber;
            }

            return 0;
        }

        int getId()	{return idR;}
        string getName() { return nameR; }
        int getInitNumber() { return initnumber; }
        int getType() { return typeR; }

        int setId(int id)
        {
            if (id > 0)
            {
                idR = id;
                return 0;
            }
            throw logic_error("Negative id!");
            return 1;
        }
        int setName(string name)
        {
            nameR = name;
            return 0;
        }
        int setInitNumber(int num)
        {
            if ((typeR == 3) && (num > 168))
            {
                throw logic_error("Too many working hours!");
                return 1;
            }

            if (num <= 0)
            {
                throw logic_error("Negative ammount!");
                return 1;
            }
            else
            initnumber = num;
            return 0;
        }
        int setType(int type)
        {
            this->typeR= type;
            return 0;
        }

    virtual	std::ostream & print(std::ostream &) const;
    friend std::ostream& operator <<(std::ostream&, const Resourse &);
//	virtual vector<Resourse*> getGroup() {};
};

#endif //RESOURSE_H
