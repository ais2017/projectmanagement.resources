#pragma once
#include <string>
#include <iostream>
#include "Resourse.h"


class Staff :
	public Resourse
{
protected:
	string nameS;
	string patronymic;
	string description;
    string surname;
	string position;
	string speciality;
public:
	//Staff(string desc = "") : Resourse(), description(desc) {}
	Staff(string nameRx = "no_name", int id = 0, int initnumberx = 0, string descriptionx = "", 
        string nameSx = "no_name", string patronymicx  = "", string surnamex = "no_surname",
		string positionx = "", string specialityx = "") :
		Resourse(nameRx, id, initnumberx,3), description(descriptionx), nameS(nameSx), patronymic(patronymicx),
    surname(surnamex),position(positionx),speciality(specialityx){}
    Staff(const Staff &m) : Resourse(m.nameR, m.idR, m.initnumber,3), nameS(m.nameS), patronymic(m.patronymic), surname(m.surname), position(m.position), speciality(m.speciality), description(m.description) {};
	~Staff();


	string getDescription() { return description; }
	string getNameS() { return nameS; }
	string getPatronymic() { return patronymic; }
    string getsurname() { return surname; }
	string getPosition() { return position; }
	string getSpeciality() { return speciality; }

	int setDescription(string descriptionx)
	{
		description = descriptionx;
		return 0;
	}
	int setNameS(string nameSx)
	{
		nameS = nameSx;
		return 0;
	}
	int setPatronymic(string patronymicx)
	{
		patronymic = patronymicx;
		return 0;
	}
    int setsurname(string surnamex)
	{
        surname = surnamex;
		return 0;
	}
	int setPosition(string positionx)
	{
		position = positionx;
		return 0;
	}
	int setSpeciality(string specialityx)
	{
		speciality = specialityx;
		return 0;
	}

	std::ostream & print(std::ostream &) const;

};
