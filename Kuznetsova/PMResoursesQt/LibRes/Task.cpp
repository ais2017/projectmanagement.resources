//#include "stdafx.h"
#include "Task.h"


Task::~Task()
{
}



int Task::addReal(Resourse * resoursex, int ammountx)
{

    RealAss em(resoursex,ammountx);


		for (int i = 0; i < real.size(); i++)
		{
            if (real[i].getResourse()->getId() == em.getResourse()->getId())
			{

                real[i].setAmmount(real[i].getIsActive()*real[i].getAmmount() + em.getAmmount());
              //  if(real[i].getAmmount() ==0)
                   real[i].setIsActive(1);
                return i;
			}
		}
     //                       cout <<endl<<real.size()<<endl;
        real.push_back(em);
    //    cout<<"kkk"<<endl;

        return real.size()-1;
}

int Task::deactivateReal(RealAss * em)
{

	for (int i = 0; i < real.size(); i++)
	{
        if (real[i].getResourse()->getId() == em->getResourse()->getId())
		{
            real[i].setIsActive(0);
			return 0;
		}
	}
	
	throw logic_error("There is no such assignment!");
}

int Task::changeReal(RealAss * em, int ammount)
{
	for (int i = 0; i < real.size(); i++)
	{
        if (real[i].getResourse()->getId() == em->getResourse()->getId())
		{
            real[i].setAmmount(real[i].getAmmount() + ammount);
			return 0;
		}
	}
	
	throw logic_error("There is no such assignment!");
}

int Task::addPlanned(Resourse * resoursex, int ammountx)
{
    PlannedAss em(resoursex, ammountx);

		for (int i = 0; i < plan.size(); i++)
		{
            if (plan[i].getResourse()->getId() == em.getResourse()->getId())
			{
                plan[i].setAmmount(plan[i].getAmmount() + em.getAmmount());
                return i;
			}
		}
		
        plan.push_back(em);
        return plan.size()-1;
}

int Task::changePlanned(PlannedAss * em, int ammount)
{
	
	for (int i = 0; i < plan.size(); i++)
	{
        if (plan[i].getResourse()->getId() == em->getResourse()->getId())
		{
            plan[i].setAmmount(plan[i].getAmmount() + ammount);
			return 0;
		}
	}
	
	throw logic_error("There is no such assignment!");
}

int Task::deletePlanned(PlannedAss * em)
{
	
	for (int i = 0; i < plan.size(); i++)
	{
        if (plan[i].getResourse()->getId() == em->getResourse()->getId())
		{
			plan.erase(plan.begin() + i);
			return 0;
		}
	}
	
	throw logic_error("There is no such assignment!");
}


std::ostream & Task::print(std::ostream &os) const
{
	os << endl << "Task " << endl << "Name: " << name << endl << "index: " << index << endl;
	if (real.size() == 0)
		 os << endl << "No real assignments" << endl;
	else
	{
		os << endl << real.size() << " real assignments: " << endl;
		for (int i = 0; i < real.size(); i++)
		{
            os << real[i];
		}
		os << endl;
	}	
	
	if (plan.size() == 0)
		os << endl << "No desirable assignments" << endl;
	else
	{
		os << endl << plan.size() << " desirable assignments: " << endl;
		for (int i = 0; i < plan.size(); i++)
		{
            os << plan[i];
		}
	}
	return os;
}

std::ostream & operator<<(std::ostream &os, const Task &t)
{
	return t.print(os);
}
