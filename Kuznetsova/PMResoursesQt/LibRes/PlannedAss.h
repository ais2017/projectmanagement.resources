#pragma once
#include <string>
#include <iostream>
#include "Resourse.h"

class PlannedAss // Planned Assignment (Предполагаемое назначение ресурсов)
{
protected:
    int  ammount;
    Resourse* resourse;
public:

    PlannedAss() : ammount(0), resourse(nullptr) {}
    PlannedAss(Resourse * resoursex, int ammountx) : ammount(ammountx), resourse(resoursex) {}
    PlannedAss(const PlannedAss &r) : ammount(r.ammount), resourse(r.resourse) {}
    ~PlannedAss();


    int getAmmount() { return ammount; }
    Resourse* getResourse() { return resourse; }

    int setAmmount(int ammountx)
    {
        if (ammountx <= 0)
        {
            throw logic_error("Negative ammount!");
            return 1;
        }
        ammount = ammountx;
        return 0;
    }
    int setResourse(Resourse* resoursex)
    {
        resourse = resoursex;
        return 0;
    }


    std::ostream & print(std::ostream &) const;

    friend std::ostream& operator <<(std::ostream&, const PlannedAss &);


};
