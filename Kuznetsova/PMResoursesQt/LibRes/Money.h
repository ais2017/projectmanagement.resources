#pragma once
#include <string>
#include <iostream>
#include "Resourse.h"


class Money :
	public Resourse
{
private:

	string description;

public:
	//Money(string desc = "") : Resourse(), description(desc) {}
	Money(string name = "no_name", int id = 0, int num = 0, string desc = "") :
		Resourse(name, id, num, 1), description(desc) {}
	Money(const Money &m) :	Resourse(m.nameR, m.idR, m.initnumber, 1), description(m.description) {};
	~Money();


	string getDescription() { return description; }

	int setDescription(string desc)
	{
		description = desc;
		return 0;
	}

	std::ostream & print(std::ostream &) const;
	//friend std::ostream& operator <<(std::ostream&, const Money &);

};

