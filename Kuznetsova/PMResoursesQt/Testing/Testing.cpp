#include "../LibRes/Resourse.h"
#include "../LibRes/Money.h"
#include "../LibRes/Materials.h"
#include "../LibRes/Staff.h"
#include "../LibRes/Group.h"
#include "../LibRes/Task.h"
#include "../LibRes/PlannedAss.h"
#include "../LibRes/RealAss.h"
#include "../LibRes/Project.h"
#include <fstream>
#include "googletest-release-1.8.1/googletest/include/gtest/gtest.h"


//  To run statistics:
//  cd /home/anna/AIS/Kuznetsova/PMResoursesQt/Testing
//  lcov -t "result" -o ex_test.info -c -d .
//  genhtml -o res ex_test.info


TEST(MoneyTesting, CheckConstructorDefaultInitialization)
{
    Money m;
    EXPECT_EQ(m.getDescription(), "");
    EXPECT_EQ(m.getId(),0);
    EXPECT_EQ(m.getName(), "no_name");
    EXPECT_EQ(m.getInitNumber(),0);
    EXPECT_EQ(m.getType(),1);
};

TEST(MoneyTesting, CheckConstructor)
{
    Money m("Ruble", 77, 803, "russian ruble");
    EXPECT_EQ(m.getDescription(), "russian ruble");
    EXPECT_EQ(m.getId(), 77);
    EXPECT_EQ(m.getName(), "Ruble");
    EXPECT_EQ(m.getInitNumber(), 803);
    EXPECT_EQ(m.getType(), 1);

    Money m1("Dollar",9,800000, "american dollar");
    EXPECT_EQ(m1.getDescription(), "american dollar");
    EXPECT_EQ(m1.getId(), 9);
    EXPECT_EQ(m1.getName(), "Dollar");
    EXPECT_EQ(m1.getInitNumber(),800000);
    EXPECT_EQ(m1.getType(), 1);
    ofstream fout;
     fout.open("file.txt");
      fout <<m1;
      fout.close();
};


TEST(MoneyTesting, CheckCopyConstructor)
{
    Money m2("Ruble",773,808,"russian ruble");
    Money m = m2;
    EXPECT_EQ(m.getDescription(), "russian ruble");
    EXPECT_EQ(m.getId(), 773);
    EXPECT_EQ(m.getName(), "Ruble");
    EXPECT_EQ(m.getInitNumber(), 808);
    EXPECT_EQ(m.getType(), 1);

    Money m3("Dollar", 9, 800000, "american dollar");
    Money m1 = m3;
    EXPECT_EQ(m1.getDescription(), "american dollar");
    EXPECT_EQ(m1.getId(), 9);
    EXPECT_EQ(m1.getName(), "Dollar");
    EXPECT_EQ(m1.getInitNumber(), 800000);
    EXPECT_EQ(m1.getType(), 1);
};

TEST(MoneyTesting, CheckConstructorNegativeAmmount)
{
    ASSERT_THROW(Money m1("Ruble", 773, -808, "russian ruble"), logic_error);

};

TEST(MoneyTesting, CheckConstructorNegativeID)
{
    ASSERT_THROW(Money m1("Ruble", -773, 808, "russian ruble"), logic_error);

};

TEST(MoneyTesting, CheckChangeAmmount)
{
    Money m("Ruble", 77, 803, "russian ruble");

    m.changeAmmount(95);
    EXPECT_EQ(m.getInitNumber(), 898);

    m.changeAmmount(-898);
    EXPECT_EQ(m.getInitNumber(), 0);

    EXPECT_EQ(m.getDescription(), "russian ruble");
    EXPECT_EQ(m.getId(), 77);
    EXPECT_EQ(m.getName(), "Ruble");
    EXPECT_EQ(m.getType(), 1);

    m.changeAmmount(10005);
    EXPECT_EQ(m.getInitNumber(), 10005);
    ASSERT_THROW(m.changeAmmount(-10006), logic_error);

};

TEST(MoneyTesting, CheckSetters)
{
    Money m;
    m.setDescription("russian ruble");
    m.setName("Ruble");
    m.setId(77);
    m.setInitNumber(803);

    Money m1("Kirpich", 56, 100, "for building buildings");
    m1.setDescription("american dollar");
    m1.setName("Dollar");
    m1.setId(9);
    m1.setInitNumber(800000);

    EXPECT_EQ(m.getDescription(), "russian ruble");
    EXPECT_EQ(m.getId(), 77);
    EXPECT_EQ(m.getName(), "Ruble");
    EXPECT_EQ(m.getInitNumber(), 803);
    EXPECT_EQ(m.getType(), 1);


    EXPECT_EQ(m1.getDescription(), "american dollar");
    EXPECT_EQ(m1.getId(), 9);
    EXPECT_EQ(m1.getName(), "Dollar");
    EXPECT_EQ(m1.getInitNumber(), 800000);
    EXPECT_EQ(m1.getType(), 1);

    ASSERT_THROW(m.setId(-9), logic_error);
    ASSERT_THROW(m.setInitNumber(-1), logic_error);
   // cout<<m1;

};



TEST(MaterialsTesting, CheckConstructorDefaultInitialization)
{
    Materials m;
    EXPECT_EQ(m.getDescription(), "");
    EXPECT_EQ(m.getId(), 0);
    EXPECT_EQ(m.getName(), "no_name");
    EXPECT_EQ(m.getInitNumber(), 0);
    EXPECT_EQ(m.getType(), 2);
};

TEST(MaterialsTesting, CheckConstructor)
{
    Materials m("Kirpich", 56, 100, "for building buildings");
    EXPECT_EQ(m.getDescription(), "for building buildings");
    EXPECT_EQ(m.getId(), 56);
    EXPECT_EQ(m.getName(), "Kirpich");
    EXPECT_EQ(m.getInitNumber(), 100);
    EXPECT_EQ(m.getType(), 2);

    Materials m1("Water", 12, 70, "for drinking");
    EXPECT_EQ(m1.getDescription(), "for drinking");
    EXPECT_EQ(m1.getId(), 12);
    EXPECT_EQ(m1.getName(), "Water");
    EXPECT_EQ(m1.getInitNumber(), 70);
    EXPECT_EQ(m1.getType(), 2);

    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m1;
    fout.close();
};

TEST(MaterialsTesting, CheckCopyConstructor)
{
    Materials m2("Kirpich", 56, 100, "for building buildings");
    Materials m = m2;
    EXPECT_EQ(m.getDescription(), "for building buildings");
    EXPECT_EQ(m.getId(), 56);
    EXPECT_EQ(m.getName(), "Kirpich");
    EXPECT_EQ(m.getInitNumber(), 100);
    EXPECT_EQ(m.getType(), 2);

    Materials m3("Water", 12, 70, "for drinking");
    Materials m1 = m3;
    EXPECT_EQ(m1.getDescription(), "for drinking");
    EXPECT_EQ(m1.getId(), 12);
    EXPECT_EQ(m1.getName(), "Water");
    EXPECT_EQ(m1.getInitNumber(), 70);
    EXPECT_EQ(m1.getType(), 2);
};

TEST(MaterialsTesting, CheckChangeAmmount)
{
    Materials m("Water", 12, 70, "for drinking");

    m.changeAmmount(30);
    EXPECT_EQ(m.getInitNumber(), 100);

    m.changeAmmount(-100);
    EXPECT_EQ(m.getInitNumber(), 0);

    m.changeAmmount(10005);
    EXPECT_EQ(m.getInitNumber(), 10005);

    ASSERT_THROW(m.changeAmmount(-10006), logic_error);

};


TEST(MaterialsTesting, CheckSetters)
{
    Materials m;
    m.setDescription("russian ruble");
    m.setName("Ruble");
    m.setId(77);
    m.setInitNumber(803);

    Materials m1("Kirpich", 56, 100, "for building buildings");
    m1.setDescription("american dollar");
    m1.setName("Dollar");
    m1.setId(9);
    m1.setInitNumber(800000);

    EXPECT_EQ(m.getDescription(), "russian ruble");
    EXPECT_EQ(m.getId(), 77);
    EXPECT_EQ(m.getName(), "Ruble");
    EXPECT_EQ(m.getInitNumber(), 803);
    EXPECT_EQ(m.getType(), 2);


    EXPECT_EQ(m1.getDescription(), "american dollar");
    EXPECT_EQ(m1.getId(), 9);
    EXPECT_EQ(m1.getName(), "Dollar");
    EXPECT_EQ(m1.getInitNumber(), 800000);
    EXPECT_EQ(m1.getType(), 2);
};

TEST(StaffTesting, CheckConstructorDefaultInitialization)
{
    Staff m;
    EXPECT_EQ(m.getNameS(), "no_name");
    EXPECT_EQ(m.getPatronymic(),"" );
    EXPECT_EQ(m.getsurname(), "no_surname");
    EXPECT_EQ(m.getPosition(), "");
    EXPECT_EQ(m.getSpeciality(), "");
    EXPECT_EQ(m.getDescription(), "");
    EXPECT_EQ(m.getId(), 0);
    EXPECT_EQ(m.getName(), "no_name");
    EXPECT_EQ(m.getInitNumber(), 0);
    EXPECT_EQ(m.getType(), 3);
};

TEST(StaffTesting, CheckConstructor)
{
    Staff m("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    EXPECT_EQ(m.getNameS(), "Ivan");
    EXPECT_EQ(m.getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m.getsurname(), "kui_i");
    EXPECT_EQ(m.getPosition(), "director");
    EXPECT_EQ(m.getSpeciality(), "maths");
    EXPECT_EQ(m.getDescription(), "borodach");
    EXPECT_EQ(m.getId(), 20);
    EXPECT_EQ(m.getName(), "Kuibishev");
    EXPECT_EQ(m.getInitNumber(), 44);
    EXPECT_EQ(m.getType(), 3);

    ASSERT_THROW(m.changeAmmount(-10006), logic_error);
    ASSERT_THROW(m.setInitNumber(500), logic_error);
};

TEST(StaffTesting, CheckCopyConstructor)
{
    Staff m1("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff m = m1;
    EXPECT_EQ(m.getNameS(), "Ivan");
    EXPECT_EQ(m.getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m.getsurname(), "kui_i");
    EXPECT_EQ(m.getPosition(), "director");
    EXPECT_EQ(m.getSpeciality(), "maths");
    EXPECT_EQ(m.getDescription(), "borodach");
    EXPECT_EQ(m.getId(), 20);
    EXPECT_EQ(m.getName(), "Kuibishev");
    EXPECT_EQ(m.getInitNumber(), 44);
    EXPECT_EQ(m.getType(), 3);

    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m1;
    fout.close();
}

TEST(StaffTesting, CheckSetters)
{
    Staff m;
    m.setDescription("borodach");
    m.setNameS("Ivan");
    m.setPatronymic("Nikolaevich");
    m.setsurname("kui_i");
    m.setPosition("director");
    m.setSpeciality("maths");
    m.setName("Kuibishev");
    m.setId(20);
    m.setInitNumber(44);


    EXPECT_EQ(m.getNameS(), "Ivan");
    EXPECT_EQ(m.getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m.getsurname(), "kui_i");
    EXPECT_EQ(m.getPosition(), "director");
    EXPECT_EQ(m.getSpeciality(), "maths");
    EXPECT_EQ(m.getDescription(), "borodach");
    EXPECT_EQ(m.getId(), 20);
    EXPECT_EQ(m.getName(), "Kuibishev");
    EXPECT_EQ(m.getInitNumber(), 44);
    EXPECT_EQ(m.getType(), 3);
}


TEST(StaffTesting, CheckChangeAmmount)
{
    Staff m("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");

    m.changeAmmount(-10);
    EXPECT_EQ(m.getInitNumber(), 34);

    m.changeAmmount(5);
    EXPECT_EQ(m.getInitNumber(), 39);

    ASSERT_THROW(m.changeAmmount(-10006), logic_error);
    ASSERT_THROW(m.changeAmmount(10006), logic_error);

};



TEST(GroupTesting, CheckConstructorDefaultInitialization)
{
    Group m;
    EXPECT_EQ(m.getGroup(),vector<Staff*>(0));
    EXPECT_EQ(m.getId(), 0);
    EXPECT_EQ(m.getName(), "no_name");
    EXPECT_EQ(m.getInitNumber(), 0);
    EXPECT_EQ(m.getType(), 4);
};

TEST(GroupTesting, CheckConstructor)
{
    Group m("Designers",678);
    EXPECT_EQ(m.getGroup(), vector<Staff*>(0));
    EXPECT_EQ(m.getId(), 678);
    EXPECT_EQ(m.getName(), "Designers");
    EXPECT_EQ(m.getInitNumber(), 0);
    EXPECT_EQ(m.getType(), 4);
};


TEST(GroupTesting, CheckCopyConstructor)
{
    Group m1("Designers", 678);
    Group m = m1;
    EXPECT_EQ(m.getGroup(), vector<Staff*>(0));
    EXPECT_EQ(m.getId(), 678);
    EXPECT_EQ(m.getName(), "Designers");
    EXPECT_EQ(m.getInitNumber(), 0);
    EXPECT_EQ(m.getType(), 4);


    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m1;
    fout.close();
};

TEST(GroupTesting, AddEmployee)
{
    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m("Designers", 678);
    m.addEmployee(&st);

    EXPECT_EQ(m.getId(), 678);
    EXPECT_EQ(m.getName(), "Designers");
    EXPECT_EQ(m.getInitNumber(), 44);
    EXPECT_EQ(m.getType(), 4);
    EXPECT_EQ(m.getGroup().size(), 1);

    EXPECT_EQ(m.getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(m.getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m.getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(m.getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(m.getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(m.getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(m.getGroup()[0]->getId(), 20);
    EXPECT_EQ(m.getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(m.getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(m.getGroup()[0]->getType(), 3);


    Staff st2("Kuibishev2", 202, 30, "borodach2", "Ivan2", "Nikolaevich2",
        "kui_i2", "director2", "maths2");

    m.addEmployee(&st2);
    st2.setNameS("newname");
    EXPECT_EQ(m.getGroup().size(), 2);
    EXPECT_EQ(m.getGroup()[1]->getNameS(), "newname");
    EXPECT_EQ(m.getGroup()[1]->getPatronymic(), "Nikolaevich2");
    EXPECT_EQ(m.getGroup()[1]->getsurname(), "kui_i2");
    EXPECT_EQ(m.getGroup()[1]->getPosition(), "director2");
    EXPECT_EQ(m.getGroup()[1]->getSpeciality(), "maths2");
    EXPECT_EQ(m.getGroup()[1]->getDescription(), "borodach2");
    EXPECT_EQ(m.getGroup()[1]->getId(), 202);
    EXPECT_EQ(m.getGroup()[1]->getName(), "Kuibishev2");
    EXPECT_EQ(m.getGroup()[1]->getInitNumber(), 30);
    EXPECT_EQ(m.getGroup()[1]->getType(), 3);

    EXPECT_EQ(m.getInitNumber(), 44+30);


    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();
};



TEST(GroupTesting, AddEmployeeNullEmployee)
{
    Staff st;
    Group m("Designers", 678);
    ASSERT_THROW(m.addEmployee(&st), logic_error);
}

TEST(GroupTesting, AddEmployeeistingID)
{
    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Staff st2("Kuibev", 20, 4, "borodach1", "Ivan1", "Nikolaevich1",
        "kui_i1", "director1", "maths1");
    Group m("Designers", 678);
    m.addEmployee(&st);
    ASSERT_THROW(m.addEmployee(&st2), logic_error);
}

TEST(GroupTesting, AddEmployeeistingsurname)
{
    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Staff st2("Kuibev", 254, 4, "borodach1", "Ivan1", "Nikolaevich1",
        "kui_i", "director1", "maths1");
    Group m("Designers", 678);
    m.addEmployee(&st);
    ASSERT_THROW(m.addEmployee(&st2), logic_error);
}



TEST(GroupTesting, DeleteEmployee)
{
    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m("Designers", 678);
    m.addEmployee(&st);

    Staff st2("Kuibishev2", 202, 30, "borodach2", "Ivan2", "Nikolaevich2",
        "kui_i2", "director2", "maths2");
    m.addEmployee(&st2);

    EXPECT_EQ(m.getGroup().size(), 2);

    m.deleteEmployee(st);

    EXPECT_EQ(m.getGroup().size(), 1);
    EXPECT_EQ(m.getGroup()[0]->getNameS(), "Ivan2");
    EXPECT_EQ(m.getGroup()[0]->getPatronymic(), "Nikolaevich2");
    EXPECT_EQ(m.getGroup()[0]->getsurname(), "kui_i2");
    EXPECT_EQ(m.getGroup()[0]->getPosition(), "director2");
    EXPECT_EQ(m.getGroup()[0]->getSpeciality(), "maths2");
    EXPECT_EQ(m.getGroup()[0]->getDescription(), "borodach2");
    EXPECT_EQ(m.getGroup()[0]->getId(), 202);
    EXPECT_EQ(m.getGroup()[0]->getName(), "Kuibishev2");
    EXPECT_EQ(m.getGroup()[0]->getInitNumber(), 30);
    EXPECT_EQ(m.getGroup()[0]->getType(), 3);

    EXPECT_EQ(m.getInitNumber(),30);

    m.deleteEmployee(st2);
    EXPECT_EQ(m.getGroup().size(), 0);
};


TEST(GroupTesting, DeleteEmployeeNotExisting)
{
    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Staff st2("Kuibev", 201, 4, "borodach1", "Ivan1", "Nikolaevich1",
        "kui_i1", "director1", "maths1");
    Group m("Designers", 678);
    m.addEmployee(&st);
    ASSERT_THROW(m.deleteEmployee(st2), logic_error);
}



TEST(ResourseTesting, CheckConstructorDefaultInitialization)
{
    Resourse m;
    EXPECT_EQ(m.getId(), 0);
    EXPECT_EQ(m.getName(), "no_name");
    EXPECT_EQ(m.getInitNumber(), 0);
    EXPECT_EQ(m.getType(), 0);
};


TEST(ResourseTesting, CheckConstructor)
{
    Resourse m("Empty", 34, 56, 1);
    EXPECT_EQ(m.getId(), 34);
    EXPECT_EQ(m.getName(), "Empty");
    EXPECT_EQ(m.getInitNumber(), 56);
    EXPECT_EQ(m.getType(), 1);

    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();
};



TEST(RealAssTesting, CheckConstructorDefaultInitialization)
{
    RealAss m;

    EXPECT_EQ(m.getAmmount(), 0);
    EXPECT_EQ(m.getIsActive(), 0);
    EXPECT_EQ(m.getResourse(), nullptr);
};

TEST(RealAssTesting, CheckConstructor)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m(&m1, 34);

    EXPECT_EQ(m.getResourse()->getId(), 56);
    EXPECT_EQ(m.getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getResourse()->getType(), 2);

    EXPECT_EQ(m.getAmmount(), 34);
    EXPECT_EQ(m.getIsActive(), 1);
};

TEST(RealAssTesting, CheckCopyConstructor)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m2(&m1, 34);
    RealAss m = m2;

    EXPECT_EQ(m.getResourse()->getId(), 56);
    EXPECT_EQ(m.getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getResourse()->getType(), 2);

    EXPECT_EQ(m.getAmmount(), 34);
    EXPECT_EQ(m.getIsActive(), 1);

    m1.setName("Name Changed");
    EXPECT_EQ(m.getResourse()->getName(), "Name Changed");
    EXPECT_EQ(m2.getResourse()->getName(), "Name Changed");


    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m1;
    fout.close();
};


TEST(RealAssTesting, CheckSetters)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m;

    m.setAmmount(34);
    EXPECT_EQ(m.getIsActive(), 0);

    m.setResourse(&m1);

    EXPECT_EQ(m.getResourse()->getId(), 56);
    EXPECT_EQ(m.getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getResourse()->getType(), 2);

    EXPECT_EQ(m.getAmmount(), 34);
    EXPECT_EQ(m.getIsActive(), 1);
    m.setIsActive(0);
    EXPECT_EQ(m.getIsActive(), 0);
};


TEST(RealAssTesting, CheckSetNegativeAmmount)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m;
    m.setResourse(&m1);

    ASSERT_THROW(m.setAmmount(-34);, logic_error);
};

TEST(RealAssTesting, CheckSetNotExistingResourse)
{
    Resourse m1("Kirpich", 56, 100);
    RealAss m;
    ASSERT_THROW(m.setResourse(&m1), logic_error);
};

TEST(PlannedAssTesting, CheckConstructorDefaultInitialization)
{
    PlannedAss m;

    EXPECT_EQ(m.getAmmount(), 0);
    EXPECT_EQ(m.getResourse(), nullptr);
};

TEST(PlannedAssTesting, CheckConstructor)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    PlannedAss m(&m1, 34);

    EXPECT_EQ(m.getResourse()->getId(), 56);
    EXPECT_EQ(m.getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getResourse()->getType(), 2);

    EXPECT_EQ(m.getAmmount(), 34);
};

TEST(PlannedAssTesting, CheckCopyConstructor)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    PlannedAss m2(&m1, 34);
    PlannedAss m = m2;

    EXPECT_EQ(m.getResourse()->getId(), 56);
    EXPECT_EQ(m.getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getResourse()->getType(), 2);

    EXPECT_EQ(m.getAmmount(), 34);

    m1.setName("Name Changed");
    EXPECT_EQ(m.getResourse()->getName(), "Name Changed");
    EXPECT_EQ(m2.getResourse()->getName(), "Name Changed");


    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();
};


TEST(PlannedAssTesting, CheckSetters)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    PlannedAss m;

    m.setAmmount(34);
    m.setResourse(&m1);

    EXPECT_EQ(m.getResourse()->getId(), 56);
    EXPECT_EQ(m.getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getResourse()->getType(), 2);

    EXPECT_EQ(m.getAmmount(), 34);
};


TEST(PlannedAssTesting, CheckSetNegativeAmmount)
{
    Materials m1("Kirpich", 56, 100, "for building buildings");
    PlannedAss m;
    m.setResourse(&m1);

    ASSERT_THROW(m.setAmmount(-34);, logic_error);
};




TEST(TaskTesting, CheckConstructorDefaultInitialization)
{
    Task m;
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 0);
    EXPECT_EQ(m.getName(), "no_name");
};

TEST(TaskTesting, CheckConstructor)
{
    Task m(345,"Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");


    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();


};

TEST(TaskTesting, CheckSetters)
{
    Task m;
    m.setIndex(345);
    m.setName("Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

};


TEST(TaskTesting, CheckAddReal)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");


    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m2(&m1, 34);
  //  Resourse* base;
    //base = &m1;

    vector<Resourse*> list(0);
    list.push_back(&m1);
//    cout <<m;
    m.addReal(&m1, 34 );
  //cout <<m;
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);

    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();
};

TEST(TaskTesting, CheckAddRealAlreadyExists)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");


    Materials m1("Kirpich", 56, 100, "for building buildings");
    //RealAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);
  //  Resourse* base;
    //base = &m1;
    m.addReal(&m1, 34 );
    //cout<<"real:     *********** "<<*m.getReal()[0];
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);

    m.addReal(&m1, 34 );
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 68);
};


TEST(TaskTesting, CheckCopyConstructor)
{
    Task m98(345, "Painting");
    Materials m1("Kirpich", 56, 100, "for building buildings");
//	RealAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m98.addReal(&m1, 34 );

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m21("Designers", 678);
    m21.addEmployee(&st);

    //PlannedAss m22(&m21, 34);

    list.push_back(&m21);

    m98.addPlanned(&m21, 34 );

    Task m = m98;

    Resourse* re = m.getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);
};

TEST(TaskTesting, DeactivateReal)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");


    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addReal(&m1, 34 );

    EXPECT_EQ(m.getReal()[0].getIsActive(), 1);
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);

    m.deactivateReal(&m2);

    EXPECT_EQ(m.getReal()[0].getIsActive(), 0);
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);


    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();
};

TEST(TaskTesting, DeactivateRealNotExists)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");


    Materials m1("Kirpich", 56, 100, "for building buildings");
    Materials m11("Kirpich1", 561, 1001, "for building buildings1");
    //RealAss m2(&m1, 34);
    RealAss m22(&m11, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addReal(&m1, 34 );

    EXPECT_EQ(m.getReal()[0].getIsActive(), 1);
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);

    ASSERT_THROW(m.deactivateReal(&m22), logic_error);
};

TEST(TaskTesting, ChangeRealNotExists)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");


    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addReal(&m1, 34 );

    EXPECT_EQ(m.getReal()[0].getIsActive(), 1);
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);

    m.changeReal(&m2, 45);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 79);
    m.changeReal(&m2, -5);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 74);
};

TEST(TaskTesting, ChangeReal)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");


    Materials m1("Kirpich", 56, 100, "for building buildings");
    Materials m11("Kirpich1", 561, 1001, "for building buildings1");
 //   RealAss m2(&m1, 34);
    RealAss m22(&m11, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addReal(&m1, 34 );

    EXPECT_EQ(m.getReal()[0].getIsActive(), 1);
    EXPECT_EQ(m.getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m.getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m.getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m.getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m.getReal()[0].getAmmount(), 34);

    ASSERT_THROW(m.changeReal(&m22, 45), logic_error);
};

TEST(TaskTesting, CheckAddPlanned)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m1("Designers", 678);
    m1.addEmployee(&st);

//	PlannedAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addPlanned(&m1, 34 );

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);

    Resourse* re = m.getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();

}


TEST(TaskTesting, CheckAddPlannedAlreadyExists)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m1("Designers", 678);
    m1.addEmployee(&st);

    PlannedAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addPlanned(&m1, 34 );

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);

    Resourse* re = m.getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

    m.addPlanned(&m1, 34 );

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);
    EXPECT_EQ(m.getPlanned()[0].getAmmount(),68);

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

}

TEST(TaskTesting, deletePlanned)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m1("Designers", 678);
    m1.addEmployee(&st);

    PlannedAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addPlanned(&m1, 34 );

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);

    Resourse* re = m.getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

    m.deletePlanned(&m2);
    EXPECT_EQ(m.getPlanned().size(), 0);
}


TEST(TaskTesting, deletePlannedNotExists)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m1("Designers", 678);
    m1.addEmployee(&st);

    PlannedAss m22(&st, 22);
    PlannedAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addPlanned(&m1, 34 );

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);

    Resourse* re = m.getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

    ASSERT_THROW(m.deletePlanned(&m22), logic_error);
}

TEST(TaskTesting, CheckChangePlanned)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m1("Designers", 678);
    m1.addEmployee(&st);

    PlannedAss m2(&m1, 34);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addPlanned(&m1, 34 );

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);

    Resourse* re = m.getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

    m.changePlanned(&m2,6);

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);
    EXPECT_EQ(m.getPlanned()[0].getAmmount(), 40);

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

}

TEST(TaskTesting, CheckChangePlannedNotExists)
{
    Task m(345, "Painting");
    EXPECT_EQ(m.getPlanned().size(), 0);
    EXPECT_EQ(m.getReal().size(), 0);
    EXPECT_EQ(m.getIndex(), 345);
    EXPECT_EQ(m.getName(), "Painting");

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m1("Designers", 678);
    m1.addEmployee(&st);

    PlannedAss m2(&m1, 34);
    PlannedAss m22(&st, 3);

    vector<Resourse*> list(0);
    list.push_back(&m1);

    m.addPlanned(&m1, 34 );

    EXPECT_EQ(m.getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m.getPlanned()[0].getResourse()->getType(), 4);

    Resourse* re = m.getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

     ASSERT_THROW(m.changePlanned(&m22,6), logic_error);

}



TEST(ProjectTesting, CheckConstructorDefaultInitialization)
{
    Project m;
    EXPECT_EQ(m.getResourseList(), vector<Resourse*>(0));
 //   EXPECT_EQ(m.getTask()size(), 0));
    EXPECT_EQ(m.getIndex(), 0);
    EXPECT_EQ(m.getName(), "no_name");
};

TEST(ProjectTesting, CheckConstructor)
{
    Project m(211,"Main");
    EXPECT_EQ(m.getResourseList(), vector<Resourse*>(0));
     EXPECT_EQ(m.getTask().size(), 0);
    EXPECT_EQ(m.getIndex(), 211);
    EXPECT_EQ(m.getName(), "Main");


    ofstream fout("file.txt", ios_base::app);
    fout.open("file.txt", ios_base::app);
    fout <<m;
    fout.close();
};

TEST(ProjectTesting, CheckSetters)
{
    Project m;

    m.setIndex(211);
    m.setName("Main");

    EXPECT_EQ(m.getResourseList(), vector<Resourse*>(0));
     EXPECT_EQ(m.getTask().size(), 0);
    EXPECT_EQ(m.getIndex(), 211);
    EXPECT_EQ(m.getName(), "Main");
};



TEST(ProjectTesting, CheckAddResourse)
{
    Project m23(211, "Main");
    EXPECT_EQ(m23.getResourseList(), vector<Resourse*>(0));
   EXPECT_EQ(m23.getTask().size(), 0);
    EXPECT_EQ(m23.getIndex(), 211);
    EXPECT_EQ(m23.getName(), "Main");

    Task m98(345, "Painting");
    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m2(&m1, 34);

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m21("Designers", 678);
    m21.addEmployee(&st);
    PlannedAss m22(&m21, 34);

    m23.addResourse(&m21);
    m23.addResourse(&m1);

    Resourse* re = m23.getResourseList()[0];
    Group *gro = (Group *)re;

    EXPECT_EQ(m23.getResourseList()[1]->getId(), 56);
    EXPECT_EQ(m23.getResourseList()[1]->getName(), "Kirpich");
    EXPECT_EQ(m23.getResourseList()[1]->getInitNumber(), 100);
    EXPECT_EQ(m23.getResourseList()[1]->getType(), 2);

    EXPECT_EQ(m23.getResourseList()[0]->getId(), 678);
    EXPECT_EQ(m23.getResourseList()[0]->getName(), "Designers");
    EXPECT_EQ(m23.getResourseList()[0]->getInitNumber(), 44);
    EXPECT_EQ(m23.getResourseList()[0]->getType(), 4);

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m23;
    fout.close();

};



TEST(ProjectTesting, CheckAddResourseExisting)
{
    Project m23(211, "Main");
    EXPECT_EQ(m23.getResourseList(), vector<Resourse*>(0));
    EXPECT_EQ(m23.getTask().size(), 0);
    EXPECT_EQ(m23.getIndex(), 211);
    EXPECT_EQ(m23.getName(), "Main");

    Task m98(345, "Painting");
    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m2(&m1, 34);

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m21("Designers", 678);
    m21.addEmployee(&st);
    PlannedAss m22(&m21, 34);

    m23.addResourse(&m21);
    EXPECT_EQ(m23.getResourseList().size(), 1);
    m23.addResourse(&m1);
    EXPECT_EQ(m23.getResourseList().size(), 2);
    m23.addResourse(&m1);
    EXPECT_EQ(m23.getResourseList().size(), 2);

    Resourse* re = m23.getResourseList()[0];
    Group *gro = (Group *)re;

    EXPECT_EQ(m23.getResourseList()[1]->getId(), 56);
    EXPECT_EQ(m23.getResourseList()[1]->getName(), "Kirpich");
    EXPECT_EQ(m23.getResourseList()[1]->getInitNumber(), 100);
    EXPECT_EQ(m23.getResourseList()[1]->getType(), 2);

    EXPECT_EQ(m23.getResourseList()[0]->getId(), 678);
    EXPECT_EQ(m23.getResourseList()[0]->getName(), "Designers");
    EXPECT_EQ(m23.getResourseList()[0]->getInitNumber(), 44);
    EXPECT_EQ(m23.getResourseList()[0]->getType(), 4);

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

};

TEST(ProjectTesting, CheckAddTask)
{
 //   cout<<"a1"<<endl;
    Project m23(211, "Main");
    EXPECT_EQ(m23.getResourseList(), vector<Resourse*>(0));
     EXPECT_EQ(m23.getTask().size(), 0);
    EXPECT_EQ(m23.getIndex(), 211);
    EXPECT_EQ(m23.getName(), "Main");

     m23.addTask(345, "Painting");


    Materials m1("Kirpich", 56, 100, "for building buildings");
    RealAss m2(&m1, 34);

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m21("Designers", 678);
    m21.addEmployee(&st);
    PlannedAss m22(&m21, 34);

    m23.addResourse(&m21);
    m23.addResourse(&m1);

    //----------------------------------------------------


    m23.tasks[0].addReal(&m1,34);

   m23.tasks[0].addPlanned(&m21, 34);

    Resourse* re = m23.tasks[0].getPlanned()[0].getResourse();

    Group *gro = (Group *)re;



    EXPECT_EQ(m23.getTask()[0].getIndex(), 345);
    EXPECT_EQ(m23.getTask()[0].getName(), "Painting");

    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getAmmount(), 34);

    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getType(), 4);

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);

//cout<<"a4"<<endl;
    ofstream fout("file.txt", ios_base::app);
   fout.open("file.txt", ios_base::app);
    fout <<m23;
    fout.close();
};
/*

TEST(ProjectTesting, CheckAddTaskExisting)
{
    Project m23(211, "Main");
    EXPECT_EQ(m23.getResourseList(), vector<Resourse*>(0));
     EXPECT_EQ(m23.getTask().size(), 0);
    EXPECT_EQ(m23.getIndex(), 211);
    EXPECT_EQ(m23.getName(), "Main");

    Task m98(345, "Painting");
    Materials m1("Kirpich", 56, 100, "for building buildings");
    //RealAss m2(&m1, 34);

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Group m21("Designers", 678);
    m21.addEmployee(&st);
    //PlannedAss m22(&m21, 34);
     m23.addTask(345, "Painting");

    m23.addResourse(&m21);
    m23.addResourse(&m1);

    m23.tasks[0].addReal(&m1,34);
   m23.tasks[0].addPlanned(&m21, 34);

    Resourse* re = m23.tasks[0].getPlanned()[0].getResourse();
    Group *gro = (Group *)re;

    m23.addResourse(&m21);

    EXPECT_EQ(m23.getTask()[0].getIndex(), 345);
    EXPECT_EQ(m23.getTask()[0].getName(), "Painting");

    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getId(), 56);
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getName(), "Kirpich");
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getInitNumber(), 100);
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getType(), 2);
    EXPECT_EQ(m23.getTask()[0].getReal()[0].getAmmount(), 34);

    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getId(), 678);
    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getName(), "Designers");
    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getInitNumber(), 44);
    EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getType(), 4);

    EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
    EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(gro->getGroup()[0]->getType(), 3);
    m23.addTask(&m98);


        EXPECT_EQ(m23.getTask()[0].getIndex(), 345);
        EXPECT_EQ(m23.getTask()[0].getName(), "Painting");

        EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getId(), 56);
        EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getName(), "Kirpich");
        EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getInitNumber(), 100);
        EXPECT_EQ(m23.getTask()[0].getReal()[0].getResourse()->getType(), 2);
        EXPECT_EQ(m23.getTask()[0].getReal()[0].getAmmount(), 34);

        EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getId(), 678);
        EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getName(), "Designers");
        EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getInitNumber(), 44);
        EXPECT_EQ(m23.getTask()[0].getPlanned()[0].getResourse()->getType(), 4);

        EXPECT_EQ(gro->getGroup()[0]->getNameS(), "Ivan");
        EXPECT_EQ(gro->getGroup()[0]->getPatronymic(), "Nikolaevich");
        EXPECT_EQ(gro->getGroup()[0]->getsurname(), "kui_i");
        EXPECT_EQ(gro->getGroup()[0]->getPosition(), "director");
        EXPECT_EQ(gro->getGroup()[0]->getSpeciality(), "maths");
        EXPECT_EQ(gro->getGroup()[0]->getDescription(), "borodach");
        EXPECT_EQ(gro->getGroup()[0]->getId(), 20);
        EXPECT_EQ(gro->getGroup()[0]->getName(), "Kuibishev");
        EXPECT_EQ(gro->getGroup()[0]->getInitNumber(), 44);
        EXPECT_EQ(gro->getGroup()[0]->getType(), 3);


};

*/

TEST(ProjectTesting, CheckResourse)
{
    Project m23(211, "Main");
    EXPECT_EQ(m23.getResourseList(), vector<Resourse*>(0));
    EXPECT_EQ(m23.getTask().size(), 0);
    EXPECT_EQ(m23.getIndex(), 211);
    EXPECT_EQ(m23.getName(), "Main");

    Task m98(345, "Painting");
    Materials m1("Kirpich", 56, 100, "for building buildings");
    //RealAss m2(&m1, 34);

    Staff st("Kuibishev", 20, 44, "borodach", "Ivan", "Nikolaevich",
        "kui_i", "director", "maths");
    Staff st2;
    Group m21("Designers", 678);
    m21.addEmployee(&st);
    //PlannedAss m22(&m21, 34);

    m23.addResourse(&m21);
    m23.addResourse(&m1);


    Resourse* re = m23.getResourseList()[0];
    Group *gro = (Group *)re;

    EXPECT_EQ(m23.checkResourses(&m21), 1);
    EXPECT_EQ(m23.checkResourses(&m1), 1);
    EXPECT_EQ(m23.checkResourses(&st), 0);
    m23.addResourse(&st2);
    EXPECT_EQ(m23.checkResourses(&st), 1);

};


//int _tmain(int argc, _TCHAR* argv[])
int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
