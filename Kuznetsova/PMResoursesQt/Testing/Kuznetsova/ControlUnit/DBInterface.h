#include <string.h>
#include <map>
#include "../PMResoursesQt/LibRes/Resourse.h"
#include "../PMResoursesQt/LibRes/Money.h"
#include "../PMResoursesQt/LibRes/Materials.h"
#include "../PMResoursesQt/LibRes/Staff.h"
#include "../PMResoursesQt/LibRes/Group.h"
#include "../PMResoursesQt/LibRes/Task.h"
#include "../PMResoursesQt/LibRes/PlannedAss.h"
#include "../PMResoursesQt/LibRes/RealAss.h"
#include "../PMResoursesQt/LibRes/Project.h"

#ifndef DBINTERFACE_H
#define DBINTERFACE_H

class DBInterface
{
public:
    DBInterface(){};
    virtual int checkPasswordDB(string login, string password) = 0;
    virtual vector<Resourse> getAllResoursesDB() = 0;
    virtual vector<Money> getAllMoneyDB() =0;
    virtual vector<Materials> getAllMaterialsDB() =0;
    virtual vector<Staff> getAllStaffDB() =0;
    virtual vector<Group> getAllGroupDB() =0;
    virtual int saveResourseDB(Resourse* res) =0;

    virtual map<int,string> showAllProjectsDB() =0;
    virtual map<int,string> showProjectTasksDB(int idproj) =0;
    virtual  map<int,int> showResourseRealAssignmentsDB(int idres) =0;
    virtual  map<int,int> showResoursePlannedAssignmentsDB(int idres) =0;

    virtual Project getProjectDB(int idproj) =0;
    virtual Task getTaskDB(int idtask) =0;
    virtual vector<int> getProjectResoursesDB(int idproj) = 0;

    virtual int saveRealAssDb(int idtask, RealAss ra) =0;
    virtual int savePlannedAssDb(int idtask, PlannedAss ra) =0;
    virtual int deletePlannedAssDb(int idtask,int idres) =0;
};

#endif // DBINTERFACE_H
