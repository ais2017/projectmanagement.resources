#include "DBPatch.h"

DBPatch::DBPatch()
{
    Resourse m("db1",34);
    Staff m1("kui", 20, 44, "borodach", "Ivan", "Nikolaevich", "Kuibishev", "director", "maths");
    Money m2("Ruble",77, 803, "russian ruble");
    Materials m3("concrete",922,1020,"concrete concrete");
    Staff m5("noname",0);
    Group m4("progers",121);

    Resourse* p = new Resourse [sizeof(m)];
    *p = m;
    resdb.push_back(p);
    Staff* p1 = new Staff [sizeof(m1)];
    *p1 = m1;
    Resourse *c1 = dynamic_cast<Resourse*>(p1);
    resdb.push_back(c1);
    Money* p2 = new Money [sizeof(m2)];
    *p2 = m2;
    Resourse *c2 = dynamic_cast<Resourse*>(p2);
    resdb.push_back(c2);
    Materials* p3 = new Materials [sizeof(m3)];
    *p3 = m3;
    Resourse *c3 = dynamic_cast<Resourse*>(p3);
    resdb.push_back(c3);
    Group* p4 = new Group [sizeof(m4)];
    *p4 = m4;
    Resourse *c4 = dynamic_cast<Resourse*>(p4);
    resdb.push_back(c4);
    Staff* p5 = new Staff [sizeof(m5)];
    *p5 = m5;
    Resourse *c5 = dynamic_cast<Resourse*>(p5);
    resdb.push_back(c5);

    //Group*g = (Group*)resdb[4];
    //Staff* st = (Staff*)resdb[1];
    p4->addEmployee(p1);
}

int DBPatch::checkPasswordDB(string login, string password)
{
    map<string,string> base;
    base["log1"] = "pas1";
    base["log2"] = "pas2";
    base["log3"] = "pas3";

    if ((base[login] == password)&&(base.find(login)!=base.end()))
        return 0;
    return 1;
}


vector<Resourse> DBPatch::getAllResoursesDB()
{
    vector<Resourse> a;
    Resourse f;
    int y=0;
    for(int i=0;i<resdb.size();i++)
    {
        if(resdb[i]->getType() == 0)
        {
            a.resize(a.size()+1);
            a[y]=*resdb[i];
            y++;
        }
    }
    return a;
}
vector<Money> DBPatch::getAllMoneyDB()
{
    vector<Money> a;
    for(int i=0;i<resdb.size();i++)
    {
        if(resdb[i]->getType() == 1)
        {
            Money* m = dynamic_cast <Money*> (resdb[i]);
            a.push_back(*m);
        }
    }
    return a;
}
vector<Materials> DBPatch::getAllMaterialsDB()
{
    vector<Materials> a;
    for(int i=0;i<resdb.size();i++)
    {
        if(resdb[i]->getType() == 2)
        {
            Materials* m = dynamic_cast <Materials*> (resdb[i]);
            a.push_back(*m);
        }
    }
    return a;
}
vector<Staff> DBPatch::getAllStaffDB()
{
    vector<Staff> a;
    for(int i=0;i<resdb.size();i++)
    {
        if(resdb[i]->getType() == 3)
        {
            Staff* m = dynamic_cast <Staff*> (resdb[i]);
            a.push_back(*m);
        }
    }
    return a;
}
vector<Group> DBPatch::getAllGroupDB()
{
    vector<Group> a;
    for(int i=0;i<resdb.size();i++)
    {
        if(resdb[i]->getType() == 4)
        {
            Group* m = dynamic_cast <Group*> (resdb[i]);
            a.push_back(*m);
        }
    }
    return a;
}

int DBPatch::saveResourseDB(Resourse* res)
{
    int n=-1;
    for(int i=0; i< resdb.size(); i++)
    {
        if(resdb[i]->getId() == res->getId())
        {

            n = i;
        }
    }
    if(n==-1)
    {
        n=resdb.size();
        resdb.resize(resdb.size()+1);
    }

    if(res->getType() == 0)
    {
        resdb[n] = new Resourse(sizeof(*res));
        *resdb[n] =*res;
    }
    else
        if(res->getType() == 1)
        {
           // new Resourse(sizeof(*res));
            Money* c1 = dynamic_cast<Money*>(res);
            Money* m1 = new Money[sizeof(Money)];
            *m1 =*c1;
            resdb[n] = dynamic_cast<Resourse*>(m1);

        }
        else
            if(res->getType() == 2)
            {
               // resdb[resdb.size()-1] = new Resourse(sizeof(*res));
                Materials* m2 = new Materials[sizeof(*res)];
                Materials* c2 = dynamic_cast<Materials*>(res);
                *m2 =*c2;
                resdb[n] = dynamic_cast<Resourse*>(m2);
            }
            else
                if(res->getType() == 3)
                {
                   // resdb[resdb.size()-1] = new Resourse(sizeof(*res));
                    Staff* m3 = new Staff[sizeof(*res)];
                    Staff* c3 = dynamic_cast<Staff*>(res);
                    *m3 =*c3;
                    resdb[n] = dynamic_cast<Resourse*>(m3);
                }
                else
                    if(res->getType() == 4)
                    {
                      //  resdb[resdb.size()-1] = new Resourse(sizeof(*res));
                        Group* m4 = new Group[sizeof(*res)];
                        Group* c4 = dynamic_cast<Group*>(res);
                        *m4 =*c4;
                        resdb[n] = dynamic_cast<Resourse*>(m4);
                    }
    return 0;

}


Task DBPatch::getTaskDB(int idtask)
{
    if(idtask ==3)
        return Task(3,"design");
    if(idtask ==110)
        return Task(110,"floor");
    if(idtask ==432)
        return Task(432,"walls");
    if(idtask ==111)
        return Task(111,"windows");

}

map<int,string> DBPatch::showAllProjectsDB()
{
    map<int,string> a;
    a[2] = "theatre";
    a[999] = "house";
    return a;
}

map<int,string> DBPatch::showProjectTasksDB(int idproj)
{
    map<int,string> a;

    if(idproj == 2)
    {
        a[3] = "design";
        return a;
    }
    if(idproj == 999)
    {
        a[110] ="floor";
        a[111] = "windows";
        a[432] = "walls";
        return a;
    }

    return a;
}

map<int,int> DBPatch::showResourseRealAssignmentsDB(int idres)
{
    map<int,int> a;
   // map<int,RealAss>::iterator it = rassdb.begin();

    for(int i=0;i<rassdb.size();i++)
    {
        if((rassdb[i].second.getIsActive())&&(rassdb[i].second.getResourse()->getId() == idres))
            a[rassdb[i].first] = rassdb[i].second.getAmmount();
    }
    return a;
}

map<int,int> DBPatch::showResoursePlannedAssignmentsDB(int idres)
{
    map<int,int> a;
  //  map<int,PlannedAss>::iterator it = passdb.begin();

    for(int i=0;i<passdb.size();i++)
    {
        if(passdb[i].second.getResourse()->getId() == idres)
            a[passdb[i].first] = passdb[i].second.getAmmount();
    }
    return a;
}


Project DBPatch::getProjectDB(int idproj)
{
    if(idproj ==2)
        return Project(2,"theatre");
    if(idproj ==999)
        return Project(999,"house");
}


vector<int> DBPatch::getProjectResoursesDB(int idproj)
{
    vector<int> vec;
    vec.push_back(0);
    vec.push_back(77);
  //  vec.push_back(922);
    return vec;
}



int DBPatch::saveRealAssDb(int idtask, RealAss ra)
{
    pair<int,RealAss> a;
    a.first = idtask;
    a.second = ra;
    for(int i=0;i<rassdb.size();i++)
    {
        if((rassdb[i].first == idtask)&&(rassdb[i].second.getResourse()->getId() == ra.getResourse()->getId() ))
        {
            rassdb[i] =a;
            return 0;
        }
    }
    rassdb.push_back(a);
    return 0;
}

int DBPatch::savePlannedAssDb(int idtask, PlannedAss ra)
{
    pair<int,PlannedAss> a;
    a.first = idtask;
    a.second = ra;
    for(int i=0;i<passdb.size();i++)
    {
        if((passdb[i].first == idtask)&&(passdb[i].second.getResourse()->getId() == ra.getResourse()->getId() ))
        {
            passdb[i] =a;
            return 0;
        }
    }
    passdb.push_back(a);
    return 0;
}

int DBPatch::deletePlannedAssDb(int idtask,int idres)
{
    vector<pair<int,PlannedAss>>::iterator it = passdb.begin();
    for(int i=0; i<passdb.size(); i++)
    {
        if((passdb[i].first == idtask)&&(passdb[i].second.getResourse()->getId() == idres))
        {
            passdb.erase(it+i);
            return 0;
        }
    }
    return 1;
}
