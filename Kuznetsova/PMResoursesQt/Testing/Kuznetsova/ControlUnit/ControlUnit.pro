CONFIG += console c++14

TARGET = Testing
LIBS += -lgcov
QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_CXXFLAGS += -std=c++0x

INCLUDEPATH += "googletest-release-1.8.1/googletest/include"
INCLUDEPATH += "googletest-release-1.8.1/googletest"
INCLUDEPATH += "../"


SOURCES += \
    Testing.cpp \
    ControlUnit.cpp \
    ../PMResoursesQt/LibRes/Group.cpp \
    ../PMResoursesQt/LibRes/Materials.cpp \
    ../PMResoursesQt/LibRes/Money.cpp \
    ../PMResoursesQt/LibRes/PlannedAss.cpp \
    ../PMResoursesQt/LibRes/Project.cpp \
    ../PMResoursesQt/LibRes/RealAss.cpp \
    ../PMResoursesQt/LibRes/Resourse.cpp \
    ../PMResoursesQt/LibRes/Staff.cpp \
    ../PMResoursesQt/LibRes/Task.cpp \
    DBPatch.cpp \
    googletest-release-1.8.1/googletest/src/gtest.cc \
    googletest-release-1.8.1/googletest/src/gtest-all.cc \
    googletest-release-1.8.1/googletest/src/gtest-death-test.cc \
    googletest-release-1.8.1/googletest/src/gtest-filepath.cc \
    googletest-release-1.8.1/googletest/src/gtest-port.cc \
    googletest-release-1.8.1/googletest/src/gtest-printers.cc \
    googletest-release-1.8.1/googletest/src/gtest-test-part.cc \
    googletest-release-1.8.1/googletest/src/gtest-typed-test.cc \

HEADERS += \
    ControlUnit.h \
    ../PMResoursesQt/LibRes/Group.h \
    ../PMResoursesQt/LibRes/Materials.h \
    ../PMResoursesQt/LibRes/Money.h \
    ../PMResoursesQt/LibRes/PlannedAss.h \
    ../PMResoursesQt/LibRes/Project.h \
    ../PMResoursesQt/LibRes/RealAss.h \
    ../PMResoursesQt/LibRes/Resourse.h \
   ../PMResoursesQt/LibRes/Staff.h \
    ../PMResoursesQt/LibRes/Task.h \
    CUInterface.h \
    DBInterface.h \
    DBPatch.h
