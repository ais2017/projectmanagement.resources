#ifndef CONTROLUNIT_H
#define CONTROLUNIT_H

#include "../PMResoursesQt/LibRes/Resourse.h"
#include "../PMResoursesQt/LibRes/Money.h"
#include "../PMResoursesQt/LibRes/Materials.h"
#include "../PMResoursesQt/LibRes/Staff.h"
#include "../PMResoursesQt/LibRes/Group.h"
#include "../PMResoursesQt/LibRes/Task.h"
#include "../PMResoursesQt/LibRes/PlannedAss.h"
#include "../PMResoursesQt/LibRes/RealAss.h"
#include "../PMResoursesQt/LibRes/Project.h"

#include <vector>
#include <map>
#include <string>
#include <utility>
#include "CUInterface.h"
#include "DBPatch.h"

using std::vector;
using std::map;
using std::string;
using std::pair;

class ControlUnit :
        public CUInterface
{
private:
        DBInterface *dbinterface;
    public:

    map<int,Resourse*> vecres;
    vector<Project> vecproj;
    //vector<pair<int,PlannedAss>> vecplan;
    //vector<pair<int,RealAss>> vecreal;
   // map<int,Staff*> vecstaff;

    string errorr;

    ControlUnit(DBInterface *dbinterfacex);

    virtual int clearProjects() override;
   virtual int fillAllResourses() override;
   virtual map<int,Resourse*> getAllResourses() override;
    virtual Project getProject(int idproj) override;

   virtual int authCheckPassword(string login, string password) override;
    virtual map<int,Resourse*> showAllResourses() override;
    virtual map<int,string> showAllProjects() override;
    virtual map<int,Resourse*> showProjectResourses(int idproj) override;
    virtual map<int,string> showProjectTasks(int idproj) override;
    virtual map<int,int> showResourseRealAssignments(int idres) override;
    virtual map<int,int> showResoursePlannedAssignments(int idres) override;


    virtual int chResCreateResourseId() override;
    virtual int chResCreateResourse(int idx, string nameRx = "no_name", int initnumberx = 0) override;
    virtual int chResCreateMoney(int idx, string nameRx ="no_name",int initnumberx = 0, string desc = "") override;
    virtual int chResCreateMaterials(int idx, string nameRx ="no_name",int initnumberx = 0, string desc = "") override;
    virtual int chResCreateStaff( int idx, string nameRx = "no_name", int initnumberx = 0, string descriptionx = "",
                         string nameSx = "no_name", string patronymicx  = "", string surnamex = "no_surname",
                         string positionx = "", string specialityx = "") override;
    virtual int chResCreateGroup(int idx, string nameRx = "no_name") override;
    //virtual int chResAddEmloyeeToGroup(int idname, Staff *st) override;

    virtual int chResChangeResourse(int mask2, int idname, string nameRx = "no_name", int initnumberx = 0) override;
    virtual int chResChangeMoney(int mask3, int idname, string nameRx ="no_name", int initnumberx = 0, string desc = "") override;
    virtual int chResChangeMaterials(int mask3,int idname, string nameRx ="no_name",int initnumberx = 0, string desc = "") override;
    virtual int chResChangeStaff(int mask8, int idname,  string nameRx = "no_name", int initnumberx = 0, string descriptionx = "",
                         string nameSx = "no_name", string patronymicx  = "", string surnamex = "no_surname",
                         string positionx = "", string specialityx = "") override;
    virtual int chResChangeGroup(int idname, string nameRx) override;
    virtual int chResCopyResourse(int idname) override; //возвращает новый id
    virtual int chResCheckUnique(string nameRx) override;

    virtual Resourse* getRes(int i) override{ return vecres[i];}
 //   virtual int printAll() override;

    virtual int chAmmChangeAmmount(int idname,int dif = 0) override;

    virtual int creGroAddEmloyee(int idemp,int idgr) override;
    virtual int creGroDeleteEmloyee(int idemp,int idgr) override;

    virtual int findTask(int idtask) ;
    virtual int findProject(int idproj) ;
    virtual int fillProjectResourses(int idproj) ;
    virtual int findRealAss(int idtask,int idres);
    virtual int findPlannedAss(int idtask,int idres);

    virtual int assResAssignResourseReal(int idres,int idproj, int idtask,int ammount) override;
    virtual int assResAssignResoursePlanned(int idres,int idproj, int idtask,int ammount) override;
    virtual int assResDeactivateReal(int idres,int idproj, int idtask) override;
    virtual int assResDeletePlanned(int idres,int idproj, int idtask) override;
};

#endif // CONTROLUNIT_H
