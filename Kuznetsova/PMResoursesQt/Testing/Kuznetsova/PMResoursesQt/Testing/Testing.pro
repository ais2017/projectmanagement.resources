CONFIG += console c++14

TARGET = Testing
LIBS += -lgcov
QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_CXXFLAGS += -std=c++0x

INCLUDEPATH += "googletest-release-1.8.1/googletest/include"
INCLUDEPATH += "googletest-release-1.8.1/googletest"
INCLUDEPATH += "../"


SOURCES += \
    Testing.cpp \
    ../LibRes/Group.cpp \
    ../LibRes/Materials.cpp \
    ../LibRes/Money.cpp \
    ../LibRes/PlannedAss.cpp \
    ../LibRes/Project.cpp \
    ../LibRes/RealAss.cpp \
    ../LibRes/Resourse.cpp \
    ../LibRes/Staff.cpp \
    ../LibRes/Task.cpp \
    googletest-release-1.8.1/googletest/src/gtest.cc \
    googletest-release-1.8.1/googletest/src/gtest-all.cc \
    googletest-release-1.8.1/googletest/src/gtest-death-test.cc \
    googletest-release-1.8.1/googletest/src/gtest-filepath.cc \
    googletest-release-1.8.1/googletest/src/gtest-port.cc \
    googletest-release-1.8.1/googletest/src/gtest-printers.cc \
    googletest-release-1.8.1/googletest/src/gtest-test-part.cc \
    googletest-release-1.8.1/googletest/src/gtest-typed-test.cc \

HEADERS += \
    ../LibRes/Group.h \
    ../LibRes/Materials.h \
    ../LibRes/Money.h \
    ../LibRes/PlannedAss.h \
    ../LibRes/Project.h \
    ../LibRes/RealAss.h \
    ../LibRes/Resourse.h \
    ../LibRes/Staff.h \
    ../LibRes/Task.h
