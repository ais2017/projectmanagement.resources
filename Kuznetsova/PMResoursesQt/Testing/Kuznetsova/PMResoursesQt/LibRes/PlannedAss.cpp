#include "PlannedAss.h"


PlannedAss::~PlannedAss()
{
}

std::ostream & PlannedAss::print(std::ostream &os) const
{
    return os << endl << "Desirable resourse: " << *resourse << endl << "Ammount: " << ammount << endl;
}

std::ostream & operator<<(std::ostream &os, const PlannedAss &t)
{
    return t.print(os);
}
