#pragma once
//#include "Resourse.h"
#include <vector>
#include "Staff.h"

class Group :
	public Resourse

{
protected:
	vector<Staff*> group;
public:

	Group(string nameRx = "no_name", int id = 0) :Resourse(nameRx, id, 0, 4), group(vector<Staff*>(0)) {}
	Group(const Group &g) :Resourse(g.nameR, g.idR, g.initnumber, 4), group(vector<Staff*>(g.group.size())= g.group) {}
	~Group();
	int addEmployee(Staff* em);
	int deleteEmployee(Staff em);
	std::ostream & print(std::ostream & os) const;
	int getGroupSize() { return group.size(); }
	vector<Staff*> getGroup() { return vector<Staff*>(group.size()) = group; }
    int setGroup(vector<Staff*> ve){group = ve; return 0;}

};

