#include "stdafx.h"
#include "Resourse.h"


//Resourse::Resourse() :idR(0), nameR("no_name"), typeR(0), initnumber(0) {}

Resourse::Resourse(string nameRx, int id, int initnumberx, int typeRx)
{
	if(id < 0)
		throw logic_error("Negative id!");
	idR = id;
	nameR = nameRx;
	typeR = typeRx;
	if(initnumberx < 0)
		throw logic_error("Negative ammount!");
	initnumber = initnumberx;

}

Resourse::~Resourse()
{
}


 std::ostream & Resourse::print(std::ostream &os) const
{

	return os << "Resourse ID : " << idR <<endl<< "Name: " << nameR << endl << "Initial ammount: " << initnumber << endl ;
}

std::ostream & operator<<(std::ostream &os, const Resourse &r)
{
	return r.print(os);
}
