#pragma once
#include "Task.h"
#include "Resourse.h"
#include <vector>

class Project

{
protected:

	string name;
	int index;

	vector<Resourse*> resourseList;
public:
        vector<Task> tasks;
	Project(int indexx = 0,string namex = "no_name" ) :name(namex), index(indexx),
        tasks(vector<Task>(0)), resourseList(vector<Resourse*>(0)) {}
	Project(const Project &g) :name(g.name), index(g.index),
        tasks(vector<Task>(g.tasks.size()) = g.tasks),
		resourseList(vector<Resourse*>(g.resourseList.size()) = g.resourseList) {}
	~Project();
	
	bool checkResourses(Resourse *em);
	int addResourse(Resourse *em);
    int addTask(int indexx = 0, string namex = "no_name");
	
	vector<Resourse*> getResourseList()
	{
		return vector<Resourse*>(resourseList.size()) = resourseList;
	}
    vector<Task> getTask()
	{
        return vector<Task>(tasks.size()) = tasks;
	}

	int getIndex() { return index; }
	string getName() { return name; }


	int setIndex(int indexx)
	{
		index = indexx;
		return 0;
	}
	int setName(string namex)
	{
		name = namex;
		return 0;
	}

	 std::ostream & print(std::ostream & os) const;
	 friend std::ostream& operator <<(std::ostream&, const Project &);
};
