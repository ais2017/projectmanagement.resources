//#include "stdafx.h"
#include "RealAss.h"



RealAss::~RealAss()
{
}

std::ostream & RealAss::print(std::ostream & os) const
{
	if (isActive)
		return os << endl << "Active" << endl << "Assigned resourse: " << *resourse << endl << "Ammount: " << ammount << endl;
	else
		return os << endl << "Deleted" << endl << "Assigned resourse: " << *resourse << endl << "Ammount: " << ammount << endl;
}

std::ostream & operator<<(std::ostream &os, const RealAss &t)
{
	return t.print(os);
}
