#include "Group.h"


Group::~Group()
{
}



int Group::addEmployee(Staff *em)
{
    if ((em->getId() == 0) || (em->getNameS() == "no_name") || (em->getName() == "no_login") || (em->getsurname() == "no_surname"))
	{
		throw logic_error("There is no such employee!");
	}

	if (group.size() > 0)
	{
		for (int i = 0; i < group.size(); i++)
		{
			if (group[i]->getId() == em->getId())
			{
				throw logic_error("There is an empoyee with this id already!");
			}

            if (group[i]->getsurname() == em->getsurname())
			{
                throw logic_error("There is an empoyee with this surname already!");
			}
		}
    }
 // cout<< em<<endl;
    group.push_back(em);
    group[group.size()-1] = &(*em);
   // cout<< group[group.size()-1]<<endl;
	initnumber += em->getInitNumber();
	return 0;
}

int Group::deleteEmployee(Staff em)
{
	if (group.size() > 0)
	{
		for (int i = 0; i < group.size(); i++)
		{
			if (group[i]->getId() == em.getId())
			{
				group.erase(group.begin() + i);
				initnumber -= em.getInitNumber();
				return 0;
			}
		}
	}
	throw logic_error("There is no such employee!");

}

std::ostream & Group::print(std::ostream &os) const
{
    os << endl<<"Name: "  <<nameR <<endl<<"ID: "<<idR<<endl;
	if (group.size() == 0)
		return os << endl << "No Employees in the group" << endl;
	else
	{
        os <<"Group of " <<group.size()<<" employees with "<< initnumber<<" working hours:"<<endl;
		for (int i = 0; i < group.size(); i++)
		{
			os << *group[i];
		}
	}
	return os;
}
