//#include "stdafx.h"
#include "Project.h"


Project::~Project()
{
}

bool Project::checkResourses(Resourse * em)
{
	if (resourseList.size() > 0)
	{
		for (int i = 0; i <  resourseList.size(); i++)
		{
			if (resourseList[i]->getId() == em->getId())
			{
				return 1;
			}
			if ((resourseList[i]->getId() == 0)&& (resourseList[i]->getType() == 3)&& (em->getType() == 3))
			{
				return 1;
			}
		}
	}
	return 0;
}

int Project::addResourse(Resourse * em)
{
	if (resourseList.size() > 0)
	{
		for (int i = 0; i < resourseList.size(); i++)
		{
			if (resourseList[i]->getId() == em->getId())
			{
				return 808;
			}
		}
	}
	resourseList.push_back(em);
	return 0;
}

int Project::addTask(int indexx, string namex)
{
    Task f(indexx,namex);
    tasks.push_back(f);
	return 0;
}



std::ostream & Project::print(std::ostream &os) const
{
	os << endl << "Project " << endl << "Name: " << name << endl << "index: " << index << endl;
	if (tasks.size() == 0)
		os << endl << "No tasks" << endl;
	else
	{
		os << endl << tasks.size() << " tasks: " << endl;
		for (int i = 0; i < tasks.size(); i++)
		{
            os << tasks[i];
		}
		os << endl;
	}

	if (resourseList.size() == 0)
		os << endl << "No required resourses" << endl;
	else
	{
		os << endl << resourseList.size() << "required resourses: " << endl;
		for (int i = 0; i < resourseList.size(); i++)
		{
			os << *resourseList[i];
		}
	}
	return os;
}

std::ostream & operator<<(std::ostream &os, const Project &t)
{
	return t.print(os);
}
