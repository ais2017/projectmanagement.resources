#pragma once
#include <string>
#include <iostream>
#include "Resourse.h"


class Materials :
	public Resourse
{
private:

	string description;

public:
	//Materials(string desc = "") : Resourse(), description(desc) {}
	Materials(string name = "no_name", int id = 0, int num = 0, string desc = "") :
		Resourse(name, id, num, 2), description(desc) {}
	Materials(const Materials &m) : Resourse(m.nameR, m.idR, m.initnumber,2), description(m.description) {};
	~Materials();


	string getDescription() { return description; }

	int setDescription(string desc)
	{
		description = desc;
		return 0;
	}

	std::ostream & print(std::ostream &) const;
	//friend std::ostream& operator <<(std::ostream&, const Materials &);

};