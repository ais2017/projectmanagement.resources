#include "ControlUnit.h"

ControlUnit::ControlUnit(DBInterface *dbinterfacex)
{
    dbinterface = dbinterfacex;
}


int ControlUnit::clearProjects()
{
    vecproj.clear();
        return 0;
}


int ControlUnit::fillAllResourses()
{
    if(vecres.size()!=0)
        return 1;
    vector<Resourse*> vec = dbinterface->getAllResoursesDB();
    for(int i=0; i< vec.size(); i++)
    {
        if(vec[i]->getType() == 0)
        {
            vecres[vec[i]->getId()] = new Resourse (sizeof *vec[i]);
            *vecres[vec[i]->getId()] = *vec[i];
        }
        if(vec[i]->getType() == 1)
        {
            vecres[vec[i]->getId()] = new Money;
            Money* m = dynamic_cast< Money*>(vecres[vec[i]->getId()]);
            Money* c = dynamic_cast< Money*>(vec[i]);
            *m = *c;
        }
        if(vec[i]->getType() == 2)
        {
            vecres[vec[i]->getId()] = new Materials ;
            Materials* m = dynamic_cast< Materials*>(vecres[vec[i]->getId()]);
            Materials* c = dynamic_cast< Materials*>(vec[i]);
            *m = *c;
        }
        if(vec[i]->getType() == 3)
        {
            vecres[vec[i]->getId()] = new Staff;
            Staff* m = dynamic_cast< Staff*>(vecres[vec[i]->getId()]);
            Staff* c = dynamic_cast< Staff*>(vec[i]);
            *m = *c;
        }
        if(vec[i]->getType() == 4)
        {
            vecres[vec[i]->getId()] = new Group;
            Group* m = dynamic_cast< Group*>(vecres[vec[i]->getId()]);
            Group* c = dynamic_cast< Group*>(vec[i]);
            *m = *c;
         }

    }

    map<int,Resourse*>::iterator it = vecres.begin();
    for(it;it!=vecres.end();it++)
    {
        if(it->second->getType() == 4)
        {
            Group* gr = dynamic_cast<Group*>(it->second);
            for(int j=0;j<gr->getGroupSize();j++)
            {
                int u=gr->getGroup()[0]->getId();
                 gr->deleteEmployee(*gr->getGroup()[0]);
                gr->addEmployee((Staff*)vecres[u]);

            }
        }

    }

    return 0;
}

map<int,Resourse*> ControlUnit::getAllResourses()
{
    //cout<<"vecres "<<vecres.size()<<endl;
    return vecres;
}

Project ControlUnit::getProject(int idproj)
{
    if(findProject(idproj)==-1)
        vecproj.push_back(dbinterface->getProjectDB(idproj));
    return vecproj[findProject(idproj)];
}

int ControlUnit::authCheckPassword(string login, string password)
{
    return dbinterface->checkPasswordDB(login, password);
}

map<int,Resourse*> ControlUnit::showAllResourses()
{
    typedef std::map<int,Resourse*>  mapT;
    mapT::iterator it = vecres.begin();
    map<int,Resourse*> a;
   // for(it;it!= vecres.end();it++)
    //{
        a.insert(it,vecres.end());
    //}
    return a;
}

map<int,string> ControlUnit::showAllProjects()
{
    map<int,string> a = dbinterface->showAllProjectsDB();
    return a;
}

map<int,Resourse*> ControlUnit::showProjectResourses(int idproj)
{
    map<int,Resourse*> a;
    //cout<<"shprres"<<endl;
    vector<int> vec = dbinterface->getProjectResoursesDB(idproj);
    //cout<<"shprres2"<<endl;
    for(int i=0; i<vec.size(); i++)
    {
        a[vec[i]] = vecres[vec[i]];
        //cout<<"fname: "<<a[vec[i]]->getName()<<endl;
        cout<<"cname: "<<vec[i]<<endl;
    }




    map<int,Resourse*>::iterator it2;
    for(it2= a.begin(); it2 !=a.end(); it2++)
    {
        cout<<"name: "<<it2->second->getName()<<"size "<<a.size()<<endl;
    }
    return a;
}

map<int,string> ControlUnit::showProjectTasks(int idproj)
{
    map<int,string> a = dbinterface->showProjectTasksDB(idproj);
    return a;
}

map<int,int> ControlUnit::showResourseRealAssignments(int idres)
{
    map<int,int> a = dbinterface->showResourseRealAssignmentsDB(idres);
    return a;
}

map<int,int> ControlUnit::showResoursePlannedAssignments(int idres)
{
    map<int,int> a = dbinterface->showResoursePlannedAssignmentsDB(idres);
    return a;
}

 int ControlUnit::chResCreateResourseId()
 {
     typedef std::map<int,Resourse*>  mapT;
     int i=1;
     mapT::iterator it = vecres.begin();
     Resourse r;

     while(1)
     {
         it = vecres.begin();
        if(vecres.find(i) == vecres.end())
            return i;
        i++;
     }
}

int ControlUnit::chResCheckUnique(string nameRx)
{
    typedef std::map<int,Resourse*>  mapT;
    mapT::iterator it = vecres.begin();

    for(it; it!= vecres.end();it++)
    {
         if(it->second->getName() == nameRx)
            return 1;
    }
    return 0;
}

int ControlUnit::chResCreateResourse(int idx, string nameRx,int initnumberx)
{
   //typedef std::map<int,Resourse*>  mapT;
   //int i=1;

   if(chResCheckUnique(nameRx))
       return 1;

   Resourse r(nameRx,idx,initnumberx,0);
   Resourse* r1 = new Resourse[sizeof(Resourse)];
   *r1 = r;
   vecres[idx] = r1;
   dbinterface->saveResourseDB(r1);
   return 0;
}

int ControlUnit::chResCreateMoney(int idx, string nameRx,int initnumberx, string desc)
{
   if(chResCheckUnique(nameRx))
       return 1;

   Money r(nameRx,idx,initnumberx,desc);
   Money * r1 = new Money[sizeof(Money)];
    *r1 = r;
   Resourse * res = r1;
   vecres[idx] = res;
   dbinterface->saveResourseDB(r1);
   return 0;
}

int ControlUnit::chResCreateMaterials(int idx, string nameRx,int initnumberx, string desc)
{
   if(chResCheckUnique(nameRx))
       return 1;

   Materials r(nameRx,idx,initnumberx,desc);
   Materials * r1 = new Materials[sizeof(Materials)];
    *r1 = r;
   Resourse * res = r1;
   vecres[idx] = res;
   dbinterface->saveResourseDB(res);
   return 0;
}

int ControlUnit::chResCreateStaff( int idx, string nameRx, int initnumberx, string descriptionx,
                                   string nameSx, string patronymicx, string surnamex,
                                   string positionx, string specialityx)
{
   if(chResCheckUnique(nameRx))
       return 1;

   Staff r( nameRx, idx, initnumberx,descriptionx, nameSx, patronymicx, surnamex,
             positionx,  specialityx);
  Staff * r1 = new Staff[sizeof(Staff)];
    *r1 = r;
   Resourse * res = r1;
   vecres[idx] = res;
 dbinterface->saveResourseDB(res);
   return 0;
}

int ControlUnit::chResCreateGroup(int idx, string nameRx)
{
   if(chResCheckUnique(nameRx))
       return 1;

   Group r(nameRx,idx);
   Group * r1 = new Group[sizeof(Group)];
    *r1 = r;
   Resourse * res = r1;
   vecres[idx] = res;
   dbinterface->saveResourseDB(r1);
   return 0;
}

int ControlUnit::chResChangeResourse(int mask2,int idname,string nameRx,int initnumberx)
{
    typedef std::map<int,Resourse*>  mapT;
    mapT::iterator it =  vecres.find(idname);

   int idx = idname;
   string nameRy = vecres[idname]->getName();
   int initnumbery = vecres[idname]->getInitNumber();

   if(mask2/10)
       nameRy = nameRx;
   if(mask2%10)
       initnumbery = initnumberx;


  if(chResCheckUnique(nameRy)&&(mask2/10))
      return 1;

  Resourse r(nameRy,idx,initnumbery,0);
  *vecres[idname] = r;
  dbinterface->saveResourseDB(vecres[idname]);
  return 0;
}


int ControlUnit::chResChangeMoney(int mask3,int idname,string nameRx,int initnumberx, string desc)
{
    typedef std::map<int,Resourse*>  mapT;
    mapT::iterator it =  vecres.find(idname);
    Money* mon = (Money*)vecres[idname];
   int idx = vecres[idname]->getId();
   string nameRy = vecres[idname]->getName();
   string descy = mon->getDescription();
   int initnumbery = vecres[idname]->getInitNumber();

   if(mask3/100)
       nameRy = nameRx;

   if((mask3/10)%10)
       initnumbery = initnumberx;

   if(mask3%10)
       descy = desc;


  if(chResCheckUnique(nameRy)&&(mask3/100))
      return 1;

  Money r(nameRy,idx,initnumbery,descy);
 // Money * r1 = new Money[sizeof(Money)];
 //  *r1 = r;
 // Resourse * res = r1;
  *mon = r;
  dbinterface->saveResourseDB(vecres[idname]);
  return 0;
}

int ControlUnit::chResChangeMaterials(int mask3,int idname,string nameRx,int initnumberx, string desc)
{
    typedef std::map<int,Resourse*>  mapT;
    mapT::iterator it =  vecres.find(idname);
    Materials* mon = (Materials*)vecres[idname];
   int idx = vecres[idname]->getId();
   string nameRy = vecres[idname]->getName();
   string descy = mon->getDescription();
   int initnumbery = vecres[idname]->getInitNumber();

   if(mask3/100)
       nameRy = nameRx;

   if((mask3/10)%10)
       initnumbery = initnumberx;

   if(mask3%10)
       descy = desc;


  if(chResCheckUnique(nameRy)&&(mask3/100))
      return 1;

  Materials r(nameRy,idx,initnumbery,descy);
    *mon = r;
  dbinterface->saveResourseDB(vecres[idname]);
  return 0;
}

int ControlUnit::chResChangeStaff( int mask8, int idname, string nameRx, int initnumberx, string descriptionx,
                                  string nameSx, string patronymicx, string surnamex,
                                  string positionx, string specialityx)
{
    typedef std::map<int,Resourse*>  mapT;
    mapT::iterator it =  vecres.find(idname);
    Staff* mon = dynamic_cast<Staff*>(vecres[idname]);
    //cout << mon<<endl;
   int idx = vecres[idname]->getId();
   string nameRy = vecres[idname]->getName();
   string descy = mon->getDescription();
   string nameSy = mon->getNameS();
   string patronymicy = mon->getPatronymic();
   string surnamey = mon->getsurname();
   string specialityy = mon->getSpeciality();
   string positiony = mon->getPosition();

   int initnumbery = vecres[idname]->getInitNumber();

   if(mask8/10000000)
       nameRy = nameRx;

   if((mask8/1000000)%10)
       initnumbery = initnumberx;

   if((mask8/100000)%10)
       descy = descriptionx;

   if((mask8/10000)%10)
       nameSy = nameSx;

   if((mask8/1000)%10)
       patronymicy = patronymicx;

   if((mask8/100)%10)
       surnamey = surnamex;

   if((mask8/10)%10)
       positiony = positionx;

   if(mask8%10)
       specialityy = specialityx;

  if(chResCheckUnique(nameRy)&&(mask8/10000000))
      return 1;


  Staff r( nameRy, idx, initnumbery,descy, nameSy, patronymicy, surnamey,
            positiony,  specialityy);

   *mon = r;
  Staff* r1 = new Staff[sizeof(Staff)];
  *r1 = r;
  //vecstaff[idx] = r1;
  dbinterface->saveResourseDB(vecres[idname]);

  return 0;
}

int ControlUnit::chResChangeGroup(int idname,string nameRx)
{
   // if (vecres[idname]->getName() == nameRx)
   //     return 0;
    if(chResCheckUnique(nameRx))
        return 1;


  vecres[idname]->setName(nameRx);
  dbinterface->saveResourseDB(vecres[idname]);
  return 0;
}

int ControlUnit::chResCopyResourse(int idname)
{
    //typedef std::map<int,Resourse*>  mapT;
    string oldname = vecres[idname]->getName();
    string newname = oldname+"_copy";
    int idx = chResCreateResourseId();

     while(chResCheckUnique(newname))
        newname = newname+"_copy";

     Resourse* uk =vecres[idname];

     if(vecres[idname]->getType() == 0)
     {
         Resourse r(newname,idx,vecres[idname]->getInitNumber(),0);
         Resourse* r1 = new Resourse[sizeof(Resourse)];
         *r1 = r;
         vecres[idx] = r1;
     }
     else
         if(vecres[idname]->getType() == 1)
         {
             Money* uk1 = (Money*)uk;
             Money r(newname,idx,uk1->getInitNumber(),uk1->getDescription());
             Money* r1 = new Money[sizeof(Money)];
             *r1 = r;
             uk = (Resourse*)(r1);
             vecres[idx] = uk;
         }
         else
             if(vecres[idname]->getType() == 2)
             {
                 Materials* uk1 = (Materials*)uk;
                 Materials r(newname,idx,uk1->getInitNumber(),uk1->getDescription());
                 Materials* r1 = new Materials[sizeof(Materials)];
                 *r1 = r;
                 uk = (Resourse*)(r1);
                 vecres[idx] = uk;
             }
             else
                 if(vecres[idname]->getType() == 3)
                 {
                     Staff* uk1 = (Staff*)uk;
                     Staff r(newname,idx,uk1->getInitNumber(),uk1->getDescription(),uk1->getNameS(),uk1->getPatronymic(),uk1->getsurname(),uk1->getPosition(),uk1->getSpeciality());
                     Staff* r1 = new Staff[sizeof(Staff)];
                     *r1 = r;
                     uk = (Resourse*)(r1);
                     vecres[idx] = uk;
                 }
                 else
                     if(vecres[idname]->getType() == 4)
                     {
                         Group* uk1 = (Group*)uk;
                         Group r = *uk1;
                         r.setName(newname);
                         r.setId(idx);
                         Resourse* r1 = new Group[sizeof(r)];
                         *r1 = r;
                          uk1 = (Group*)r1;
                         uk1->setGroup(r.getGroup());
                         r1 = (Resourse*)uk1;
                       //  cout<<"old group pointer"<<endl<<uk1->getGroupSize()<<endl;
                         vecres[idx] = r1;
                      //   Group* uk2 = (Group*)vecres[idx];
                       //  cout<<"new group pointer"<<endl<<uk2->getGroupSize()<<endl;
                     }

    dbinterface->saveResourseDB(vecres[idx]);
    return idx;

}

/*
int  ControlUnit::printAll()
{
    cout<<endl<<vecres.size()<<" elements in vecres:-----------------------------------------------------------------"<<endl;
    typedef std::map<int,Resourse*>  mapT;
    mapT::iterator it =  vecres.begin();
    for(it;it!=vecres.end();it++)
    {
        cout<<*(it->second)<<endl;
    }


    cout<<endl<<vecproj.size()<<" elements in vecproj:"<<endl;
    for(int i=0;i<vecproj.size();i++)
    {
         cout<<vecproj[i];
    }

    return 0;

}
*/

int ControlUnit::chAmmChangeAmmount(int idname,int dif)
{
    vecres[idname]->changeAmmount(dif);
    dbinterface->saveResourseDB(vecres[idname]);
    return 0;
}


int ControlUnit::creGroAddEmloyee(int idemp,int idgr)
{
    Group* gro = dynamic_cast<Group*>(vecres[idgr]);
    Staff* st = dynamic_cast<Staff*>(vecres[idemp]);
    gro->addEmployee(st);
    dbinterface->saveResourseDB(vecres[idgr]);
    return 0;
}

int ControlUnit::creGroDeleteEmloyee(int idemp,int idgr)
{
    Group* gro = dynamic_cast<Group*>(vecres[idgr]);
    Staff* st = dynamic_cast<Staff*>(vecres[idemp]);
    gro->deleteEmployee(*st);
    dbinterface->saveResourseDB(vecres[idgr]);
    return 0;
}

int ControlUnit::findTask(int idtask)
{
    for(int i=0;i<vecproj.size();i++)
    {
        for(int j=0;j<vecproj[i].tasks.size();j++)
            if(vecproj[i].tasks[j].getIndex()==idtask)
                return j;
    }
    return -1;
}

int ControlUnit::findProject(int idproj)
{
    for(int i=0;i<vecproj.size();i++)
    {
         if(vecproj[i].getIndex()==idproj)
         return i;
    }
    return -1;
}

int ControlUnit::findRealAss(int idtask,int idres)
{
   int pid =-1;
   int tid=-1;

   for(int i=0;i<vecproj.size();i++)
    {
        for(int j=0;j<vecproj[i].tasks.size();j++)
            if(vecproj[i].tasks[j].getIndex()==idtask)
            {
                tid=j;
                pid=i;
            }
    }

   if(tid == -1)
       return -1;
int hg=0;
   for(int i=0; i<vecproj[pid].tasks[tid].getReal().size();i++)
   {
       if(vecproj[pid].tasks[tid].getReal()[i].getResourse()->getId() == idres)
           hg=i;
   }

    return hg;
}

int ControlUnit::findPlannedAss(int idtask,int idres)
{
   int pid =-1;
   int tid=-1;

   for(int i=0;i<vecproj.size();i++)
    {
        for(int j=0;j<vecproj[i].tasks.size();j++)
            if(vecproj[i].tasks[j].getIndex()==idtask)
            {
                tid=j;
                pid=i;
            }
    }

   if(tid == -1)
       return -1;
    int hg =0;
   for(int i=0; i<vecproj[pid].tasks[tid].getPlanned().size();i++)
   {
       if(vecproj[pid].tasks[tid].getPlanned()[i].getResourse()->getId() == idres)
           hg=i;
   }

    return hg;
}

int ControlUnit::fillProjectResourses(int idproj)
{
    vector<int> vec = dbinterface->getProjectResoursesDB(idproj);
    for(int i=0; i<vec.size(); i++)
    {
        vecproj[findProject(idproj)].addResourse(vecres[vec[i]]);
    }
    return 0;
}

int ControlUnit::assResAssignResourseReal(int idres,int idproj, int idtask,int ammount)
{
    int n=0;
    if((ammount > vecres[idres]->getInitNumber()&&(vecres[idres]->getType()<3)))
    {
        errorr = "Not enough resourse";
        return 1;
    }
    int r=0;
    for(int i=0; i<dbinterface->getProjectResoursesDB(idproj).size();i++)
    {
        if((dbinterface->getProjectResoursesDB(idproj)[i] == idres )||((dbinterface->getProjectResoursesDB(idproj)[i] ==0)&&(vecres[idres]->getType()>=3)))
            r=1;
    }

    if(r==0)
    {
        errorr = "Resourse is not in the list";
        return 2;
    }

    if (findProject(idproj)==-1)
    {
        vecproj.push_back(dbinterface->getProjectDB(idproj));
        fillProjectResourses(idproj);
      /*  if(vecproj[vecproj.size()-1].checkResourses(vecres[idres])==0)
        {
            errorr = "Resourse is not in the list";
            return 2;
        }*/
        vecproj[vecproj.size()-1].tasks.push_back(dbinterface->getTaskDB(idtask));
        if(ammount <= vecres[idres]->getInitNumber())
        {

            vecproj[vecproj.size()-1].tasks[0].addReal(vecres[idres],ammount+dbinterface->getRealDB(idtask,idres));
            vecres[idres]->changeAmmount(-ammount);
            n=0;
        }
       /* else
        {
            errorr = "Not enough resourse";
            return 1;
        }*/
    }
    else
        if (findTask(idtask)==-1)
        {
            vecproj[findProject(idproj)].tasks.push_back(dbinterface->getTaskDB(idtask));
           /* if(vecproj[findProject(idproj)].checkResourses(vecres[idres])==0)
            {
                errorr = "Resourse is not in the list";
                return 2;
            }*/
            if(ammount <= vecres[idres]->getInitNumber())
            {
                vecproj[findProject(idproj)].tasks[findTask(idtask)].addReal(vecres[idres],ammount+dbinterface->getRealDB(idtask,idres));
                vecres[idres]->changeAmmount(-ammount);
                n=0;
            }
          /*  else
            {
                errorr = "Not enough resourse";
                return 1;
            }*/

        }
        else
        {

            if(ammount <= vecres[idres]->getInitNumber())
            {
               // if(findRealAss(idtask,idres) == -1)
                 //   n=vecproj[findProject(idproj)].tasks[findTask(idtask)].addReal(vecres[idres],ammount+dbinterface->getRealDB(idtask,idres));
               // else
                    n=vecproj[findProject(idproj)].tasks[findTask(idtask)].addReal(vecres[idres],ammount);

                vecres[idres]->changeAmmount(-ammount);

            }
          /*  else
            {
                errorr = "Not enough resourse";
                return 1;
            }*/
        }
    dbinterface->saveRealAssDb(idtask,vecproj[findProject(idproj)].tasks[findTask(idtask)].getReal()[n]);
    saveAll();
    return 0;
}

int ControlUnit::assResAssignResoursePlanned(int idres,int idproj, int idtask,int ammount)
{
    int n=0;


    int r=0;
    for(int i=0; i<dbinterface->getProjectResoursesDB(idproj).size();i++)
    {
        if((dbinterface->getProjectResoursesDB(idproj)[i] == idres )||((dbinterface->getProjectResoursesDB(idproj)[i] ==0)&&(vecres[idres]->getType()>=3)))
            r=1;
    }

    if(r==0)
    {
        errorr = "Resourse is not in the list";
         int f= findPlannedAss(110,77);
        return 2;
    }

    if (findProject(idproj)==-1)
    {
        vecproj.push_back(dbinterface->getProjectDB(idproj));
        fillProjectResourses(idproj);
      /*  if(vecproj[vecproj.size()-1].checkResourses(vecres[idres])==0)
        {
            errorr = "Resourse is not in the list";
            return 2;
        }*/
        vecproj[vecproj.size()-1].tasks.push_back(dbinterface->getTaskDB(idtask));
        //if(ammount <= vecres[idres]->getInitNumber())
        {
            vecproj[vecproj.size()-1].tasks[0].addPlanned(vecres[idres],ammount+dbinterface->getPlannedDB(idtask,idres));
           // vecres[idres]->changeAmmount(-ammount);
            n=0;
        }

    }
    else
        if (findTask(idtask)==-1)
        {
            vecproj[findProject(idproj)].tasks.push_back(dbinterface->getTaskDB(idtask));
            int f= findRealAss(65,77);
           /* if(vecproj[findProject(idproj)].checkResourses(vecres[idres])==0)
            {
                errorr = "Resourse is not in the list";
                return 2;
            }*/
           // if(ammount <= vecres[idres]->getInitNumber())
            {
                vecproj[findProject(idproj)].tasks[findTask(idtask)].addPlanned(vecres[idres],ammount+dbinterface->getPlannedDB(idtask,idres));
               // vecres[idres]->changeAmmount(-ammount);
                n=0;
            }

        }
        else
        {

            //if(ammount <= vecres[idres]->getInitNumber())
            {
                //if(findPlannedAss(idtask,idres) == -1)
                  //  n=vecproj[findProject(idproj)].tasks[findTask(idtask)].addPlanned(vecres[idres],ammount+dbinterface->getPlannedDB(idtask,idres));
                //else
                    n=vecproj[findProject(idproj)].tasks[findTask(idtask)].addPlanned(vecres[idres],ammount);


            }
        }
    dbinterface->savePlannedAssDb(idtask,vecproj[findProject(idproj)].tasks[findTask(idtask)].getPlanned()[n]);
    return 0;
}

int ControlUnit::assResDeactivateReal(int idres,int idproj, int idtask)
{
    int n=0;
    if (findProject(idproj)==-1)
    {
        vecproj.push_back(dbinterface->getProjectDB(idproj));
        vecproj[vecproj.size()-1].tasks.push_back(dbinterface->getTaskDB(idtask));
        vecproj[vecproj.size()-1].tasks[0].addReal(vecres[idres],dbinterface->showResourseRealAssignmentsDB(idres)[idtask]);
      //  cout<<"amm bef "<<vecres[idres]->getInitNumber()<<endl;
    //    cout<<"amm bef n "<<dbinterface->showResourseRealAssignmentsDB(idres)[idtask]<<endl;
       vecres[idres]->changeAmmount(dbinterface->showResourseRealAssignmentsDB(idres)[idtask]);
        vecproj[vecproj.size()-1].tasks[0].deactivateReal(&vecproj[vecproj.size()-1].tasks[0].getReal()[0]);
    }
    else
        if (findTask(idtask)==-1)
        {
            vecproj[findProject(idproj)].tasks.push_back(dbinterface->getTaskDB(idtask));
            vecproj[findProject(idproj)].tasks[findTask(idtask)].addReal(vecres[idres],dbinterface->showResourseRealAssignmentsDB(idres)[idtask]);
        //    cout<<"amm bef "<<vecres[idres]->getInitNumber()<<endl;
        //    cout<<"amm bef n "<<dbinterface->showResourseRealAssignmentsDB(idres)[idtask]<<endl;
           vecres[idres]->changeAmmount(dbinterface->showResourseRealAssignmentsDB(idres)[idtask]);
            vecproj[findProject(idproj)].tasks[findTask(idtask)].deactivateReal(&vecproj[findProject(idproj)].tasks[findTask(idtask)].getReal()[0]);
           // vecres[idres]->changeAmmount(dbinterface->showResourseRealAssignmentsDB(idres)[idtask]);
        }
        else
        {
            cout<<"amm bef "<<vecres[idres]->getInitNumber()<<endl;
            cout<<"amm bef n "<<dbinterface->showResourseRealAssignmentsDB(idres)[idtask]<<endl;
           vecres[idres]->changeAmmount(dbinterface->showResourseRealAssignmentsDB(idres)[idtask]);
            vecproj[findProject(idproj)].tasks[findTask(idtask)].deactivateReal(&vecproj[findProject(idproj)].tasks[findTask(idtask)].getReal()[findRealAss(idtask,idres)]);
            //vecres[idres]->changeAmmount(dbinterface->showResourseRealAssignmentsDB(idres)[idtask]);
        }
    dbinterface->saveRealAssDb(idtask,vecproj[findProject(idproj)].tasks[findTask(idtask)].getReal()[findRealAss(idtask,idres)]);

   // cout<<"amm aft "<<vecres[idres]->getInitNumber()<<endl;
  //  dbinterface->saveResourseDB(vecres[idres]);

    return 0;

}

int ControlUnit::assResDeletePlanned(int idres,int idproj, int idtask)
{
    int n=0;
    if ((findTask(idtask)!=-1)&&findPlannedAss(idtask,idres)!=-1)
    {
        vecproj[findProject(idproj)].tasks[findTask(idtask)].deletePlanned(&vecproj[findProject(idproj)].tasks[findTask(idtask)].getPlanned()[findPlannedAss(idtask,idres)]);
    }
    dbinterface->deletePlannedAssDb(idtask,idres);
    return 0;

}

int ControlUnit::addResToProject(int idproj,int idres)
{
    dbinterface->saveProjectResoursesDB(idproj,idres);
    return 0;
}

int ControlUnit::saveAll()
{
    map<int,Resourse*>::iterator it = vecres.begin();
    for(it;it != vecres.end(); it++)
    {
        dbinterface->saveResourseDB(it->second);
    }
    return 0;
}
