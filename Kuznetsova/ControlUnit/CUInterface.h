#ifndef CUINTERFACE_H
#define CUINTERFACE_H

#include "../PMResoursesQt/LibRes/Resourse.h"
#include "../PMResoursesQt/LibRes/Money.h"
#include "../PMResoursesQt/LibRes/Materials.h"
#include "../PMResoursesQt/LibRes/Staff.h"
#include "../PMResoursesQt/LibRes/Group.h"
#include "../PMResoursesQt/LibRes/Task.h"
#include "../PMResoursesQt/LibRes/PlannedAss.h"
#include "../PMResoursesQt/LibRes/RealAss.h"
#include "../PMResoursesQt/LibRes/Project.h"

#include <vector>
#include <map>
#include <string>
#include <utility>


using std::vector;
using std::map;
using std::string;
using std::pair;

class CUInterface
{
public:
    CUInterface(){}

    virtual int clearProjects() =0;
    virtual int fillAllResourses() = 0;
    virtual map<int,Resourse*> getAllResourses() = 0;
    virtual Project getProject(int idproj) =0;

    virtual int authCheckPassword(string login, string password) = 0;
    virtual map<int,Resourse*> showAllResourses() =0;
    virtual map<int,string> showAllProjects() =0;
    virtual map<int,Resourse*> showProjectResourses(int idproj) =0;
    virtual map<int,string> showProjectTasks(int idproj)  =0;
    virtual map<int,int> showResourseRealAssignments(int idres) =0;
    virtual map<int,int> showResoursePlannedAssignments(int idres) =0;

    virtual int chResCreateResourseId() = 0;
    virtual int chResCreateResourse(int idx, string nameRx = "no_name", int initnumberx = 0)= 0;
    virtual int chResCreateMoney(int idx, string nameRx ="no_name",int initnumberx = 0, string desc = "")= 0;
    virtual int chResCreateMaterials(int idx, string nameRx ="no_name",int initnumberx = 0, string desc = "")= 0;
    virtual int chResCreateStaff( int idx, string nameRx = "no_name", int initnumberx = 0, string descriptionx = "",
                         string nameSx = "no_name", string patronymicx  = "", string surnamex = "no_surname",
                         string positionx = "", string specialityx = "")= 0;
    virtual int chResCreateGroup(int idx, string nameRx = "no_name")= 0;
    //virtual int chResAddEmloyeeToGroup(int idname, Staff *st)= 0;

    virtual int chResChangeResourse(int mask2, int idname, string nameRx = "no_name", int initnumberx = 0)= 0;
    virtual int chResChangeMoney(int mask3, int idname, string nameRx ="no_name", int initnumberx = 0, string desc = "")= 0;
    virtual int chResChangeMaterials(int mask3,int idname, string nameRx ="no_name",int initnumberx = 0, string desc = "")= 0;
    virtual int chResChangeStaff(int mask8, int idname,  string nameRx = "no_name", int initnumberx = 0, string descriptionx = "",
                         string nameSx = "no_name", string patronymicx  = "", string surnamex = "no_surname",
                         string positionx = "", string specialityx = "")= 0;
    virtual int chResChangeGroup(int idname, string nameRx)= 0;
    virtual int chResCopyResourse(int idname)= 0; //возвращает новый id


    virtual int chResCheckUnique(string nameRx)= 0;
    virtual Resourse* getRes(int i) =0 ;

 //   virtual int printAll() =0;
    virtual int chAmmChangeAmmount(int idname,int dif = 0) =0;
    virtual int creGroAddEmloyee(int idemp,int idgr) =0;
    virtual int creGroDeleteEmloyee(int idemp,int idgr) =0;

   // virtual int findTask(int idtask) =0;
   // virtual int findProject(int idproj) = 0;
   // virtual int fillProjectResourses(int idproj) =0;


    virtual int assResAssignResourseReal(int idres,int idproj, int idtask,int ammount) = 0;
    virtual int assResAssignResoursePlanned(int idres,int idproj, int idtask,int ammount) = 0;
    virtual int assResDeactivateReal(int idres,int idproj, int idtask) =0;
    virtual int assResDeletePlanned(int idres,int idproj, int idtask) =0;

    virtual int addResToProject(int idproj,int idres) =0;
    virtual int saveAll() =0;


    virtual ~CUInterface(){}


};

#endif // CUINTERFACE_H
