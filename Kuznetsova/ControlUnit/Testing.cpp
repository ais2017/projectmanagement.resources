#include <iostream>
#include "ControlUnit.h"
#include "CUInterface.h"
#include "DBPatch.h"
#include "DBInterface.h"
#include <fstream>
#include "googletest-release-1.8.1/googletest/include/gtest/gtest.h"

//  To run statistics:
//  cd /home/anna/AIS/Kuznetsova/build-ControlUnit-Desktop-Debug
//  lcov -t "result" -o ex_test.info -c -d .
//  genhtml -o res ex_test.info


TEST(AuthTesting, AuthCheckPassword)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);

    EXPECT_EQ(cu->authCheckPassword("log1","pas2"), 1);
    EXPECT_EQ(cu->authCheckPassword("log2","pas2"), 0);
    EXPECT_EQ(cu->authCheckPassword("log4","pas4"), 1);
};

TEST(chResTesting, CheckCreateMoney)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMoney(77,"Ruble", 803, "russian ruble");
    Money* m = (Money*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

    cu->chResCreateMoney(99,"Dollar",800000, "american dollar");
    Money* m1 = (Money*)(cu->getAllResourses()[99]);
    EXPECT_EQ(m1->getDescription(), "american dollar");
    EXPECT_EQ(m1->getId(), 99);
    EXPECT_EQ(m1->getName(), "Dollar");
    EXPECT_EQ(m1->getInitNumber(),800000);
    EXPECT_EQ(m1->getType(), 1);
    EXPECT_EQ(cu->chResCreateMoney(77,"Ruble", 803, "russian ruble"), 1);
};


TEST(chResTesting, CheckCopyMoney)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMoney(77,"Ruble", 803, "russian ruble");

    Money* m = (Money*)(cu->getAllResourses()[cu->chResCopyResourse(77)]);
    EXPECT_EQ(m->getDescription(), "russian ruble");
   // EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble_copy");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);
};

TEST(chResTesting, CheckchangeMoney)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMoney(77,"Dollar",800000, "american dollar");
    cu->chResChangeMoney(111,77,"Ruble", 803, "russian ruble");
    Money* m = (Money*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

  cu->chResCreateMoney(771,"Dollar1",800000, "american dollar");
    EXPECT_EQ(cu->chResChangeMoney(111,771,"Ruble",800000, "american dollar"),1);
};

TEST(chResTesting, CheckCreateMoneyNegativeAmmount)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMoney(77,"Ruble", -803, "russian ruble"), logic_error);

};

TEST(chResTesting, CheckCreateMoneyNegativeID)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMoney(-77,"Ruble", 803, "russian ruble"), logic_error);
};

TEST(chResTesting, CheckChangeAmmount)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMoney(77,"Ruble", 803, "russian ruble");

    cu->chAmmChangeAmmount(77,95);
    Money* m = (Money*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 898);

    cu->chAmmChangeAmmount(77,-898);
    m = (Money*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 0);

    ASSERT_THROW(cu->chAmmChangeAmmount(77,-10), logic_error);
};

TEST(chResTesting, CheckCreateMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Ruble", 803, "russian ruble");
    Materials* m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);

    cu->chResCreateMaterials(99,"Dollar",800000, "american dollar");
    Materials* m1 = (Materials*)(cu->getAllResourses()[99]);
    EXPECT_EQ(m1->getDescription(), "american dollar");
    EXPECT_EQ(m1->getId(), 99);
    EXPECT_EQ(m1->getName(), "Dollar");
    EXPECT_EQ(m1->getInitNumber(),800000);
    EXPECT_EQ(m1->getType(), 2);
    EXPECT_EQ(cu->chResCreateMaterials(99,"Dollar",800000, "american dollar"), 1);
};

TEST(chResTesting, CheckCopyMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Ruble", 803, "russian ruble");
    Materials* m = (Materials*)(cu->getAllResourses()[cu->chResCopyResourse(77)]);
    EXPECT_EQ(m->getDescription(), "russian ruble");
   // EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble_copy");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);
};

TEST(chResTesting, CheckCopyMaterialsShow)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->showAllProjects();
    cu->showAllResourses();
    cu->showProjectResourses(2);
    cu->showProjectTasks(2);
    cu->showResoursePlannedAssignments(77);
    cu->showResourseRealAssignments(77);
};

TEST(chResTesting, CheckchangeMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Dollar",800000, "american dollar");
    cu->chResChangeMaterials(111,77,"Ruble", 803, "russian ruble");
    Materials* m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);
    cu->chResCreateMaterials(771,"Dollar1",800000, "american dollar");
    EXPECT_EQ(cu->chResChangeMaterials(111,771,"Ruble",800000, "american dollar"),1);

  //  ASSERT_THROW(cu->chResChangeMaterials(111,77,"Ruble", 803, "russian ruble"), logic_error);
};


TEST(chResTesting, CheckCreateMaterialsNegativeAmmount)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMaterials(77,"Ruble", -803, "russian ruble"), logic_error);

};

TEST(chResTesting, CheckCreateMaterialsNegativeID)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMaterials(-77,"Ruble", 803, "russian ruble"), logic_error);
};

TEST(chResTesting, CheckChangeAmmountMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Ruble", 803, "russian ruble");

    cu->chAmmChangeAmmount(77,95);
    Materials* m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 898);

    cu->chAmmChangeAmmount(77,-898);
    m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 0);

    ASSERT_THROW(cu->chAmmChangeAmmount(77,-10), logic_error);
};


TEST(chResTesting, CheckCreateStaff)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateStaff(20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* m = (Staff*)(cu->getAllResourses()[20]);

    EXPECT_EQ(m->getNameS(), "Ivan");
    EXPECT_EQ(m->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getsurname(), "kui_i");
    EXPECT_EQ(m->getPosition(), "director");
    EXPECT_EQ(m->getSpeciality(), "maths");
    EXPECT_EQ(m->getDescription(), "borodach");
    EXPECT_EQ(m->getId(), 20);
    EXPECT_EQ(m->getName(), "Kuibishev");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 3);
    EXPECT_EQ(cu->chResCreateStaff(20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths"), 1);


    ASSERT_THROW(m->changeAmmount(-10006), logic_error);
    ASSERT_THROW(m->setInitNumber(500), logic_error);
};

TEST(chResTesting, CheckCopyStaff)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateStaff(20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* m = (Staff*)(cu->getAllResourses()[cu->chResCopyResourse(20)]);

    EXPECT_EQ(m->getNameS(), "Ivan");
    EXPECT_EQ(m->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getsurname(), "kui_i");
    EXPECT_EQ(m->getPosition(), "director");
    EXPECT_EQ(m->getSpeciality(), "maths");
    EXPECT_EQ(m->getDescription(), "borodach");
  //  EXPECT_EQ(m->getId(), 20);
    EXPECT_EQ(m->getName(), "Kuibishev_copy");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 3);
    EXPECT_EQ(cu->chResCreateStaff(20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths"), 1);
    cu->chResCopyResourse(20);
};


TEST(chResTesting, CheckChangeStaff)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateStaff(20);
    cu->chResChangeStaff(11111111,20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");

    Staff* m = (Staff*)(cu->getAllResourses()[20]);

    EXPECT_EQ(m->getNameS(), "Ivan");
    EXPECT_EQ(m->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getsurname(), "kui_i");
    EXPECT_EQ(m->getPosition(), "director");
    EXPECT_EQ(m->getSpeciality(), "maths");
    EXPECT_EQ(m->getDescription(), "borodach");
    EXPECT_EQ(m->getId(), 20);
    EXPECT_EQ(m->getName(), "Kuibishev");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 3);

    cu->chResCreateStaff(201);
    EXPECT_EQ(cu->chResChangeStaff(11111111,20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths"),1);


};


TEST(creGroTesting, CheckCreateGroup)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(678,"Designers");
    Group* m = (Group*)(cu->getAllResourses()[678]);
    EXPECT_EQ(m->getGroup(), vector<Staff*>(0));
    EXPECT_EQ(m->getId(), 678);
    EXPECT_EQ(m->getName(), "Designers");
    EXPECT_EQ(m->getInitNumber(), 0);
    EXPECT_EQ(m->getType(), 4);
    EXPECT_EQ(cu->chResCreateGroup(678,"Designers"), 1);
     cu->chResCreateGroup(6780,"Designers1");
     EXPECT_EQ(cu->chResCreateGroup(678,"Designers"),1);
};

TEST(creGroTesting, CheckCopyGroup)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(678,"Designers");
    Group* m = (Group*)(cu->getAllResourses()[cu->chResCopyResourse(678)]);
    EXPECT_EQ(m->getGroup(), vector<Staff*>(0));
  //  EXPECT_EQ(m->getId(), 678);
    EXPECT_EQ(m->getName(), "Designers_copy");
    EXPECT_EQ(m->getInitNumber(), 0);
    EXPECT_EQ(m->getType(), 4);
};

TEST(creGroTesting, CheckChangeGroup)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(678);
    cu->chResChangeGroup(678,"Designers");
    Group* m = (Group*)(cu->getAllResourses()[678]);
    EXPECT_EQ(m->getGroup(), vector<Staff*>(0));
    EXPECT_EQ(m->getId(), 678);
    EXPECT_EQ(m->getName(), "Designers");
    EXPECT_EQ(m->getInitNumber(), 0);
    EXPECT_EQ(m->getType(), 4);
    EXPECT_EQ(cu->chResCreateGroup(678,"Designers"), 1);
    cu->chResCreateGroup(6781);
    EXPECT_EQ(cu->chResChangeGroup(6781,"Designers"),1);

};

TEST(creGroTesting, AddEmployee)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(678,"Designers");
    cu->chResCreateStaff(20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* st = (Staff*)(cu->getAllResourses()[20]);
    Group* m = (Group*)(cu->getAllResourses()[678]);

    cu->creGroAddEmloyee(20,678);

    EXPECT_EQ(m->getId(), 678);
    EXPECT_EQ(m->getName(), "Designers");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 4);
    EXPECT_EQ(m->getGroup().size(), 1);

    EXPECT_EQ(m->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(m->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(m->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(m->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(m->getGroup()[0]->getDescription(), "borodach");
    EXPECT_EQ(m->getGroup()[0]->getId(), 20);
    EXPECT_EQ(m->getGroup()[0]->getName(), "Kuibishev");
    EXPECT_EQ(m->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(m->getGroup()[0]->getType(), 3);


    Staff st2();

};



TEST(creGroTesting, AddEmployeeNullEmployee)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(678,"Designers");
    cu->chResCreateStaff(20);
    Staff* st = (Staff*)(cu->getAllResourses()[20]);
    Group* m = (Group*)(cu->getAllResourses()[678]);
    ASSERT_THROW(cu->creGroAddEmloyee(20,678), logic_error);
}


TEST(creGroTesting, DeleteEmployee)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(678,"Designers");
    cu->chResCreateStaff(20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* st = (Staff*)(cu->getAllResourses()[20]);
    Group* m = (Group*)(cu->getAllResourses()[678]);

    cu->creGroAddEmloyee(20,678);

    cu->creGroDeleteEmloyee(20,678);

    EXPECT_EQ(m->getGroup().size(), 0);
};


TEST(creGroTesting, DeleteEmployeeNotExisting)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(678,"Designers");
    cu->chResCreateStaff(20, "Kuibishev", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    cu->chResCreateStaff(202, "Kuibishev2", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* st = (Staff*)(cu->getAllResourses()[20]);
    Group* m = (Group*)(cu->getAllResourses()[678]);

    cu->creGroAddEmloyee(20,678);

    EXPECT_EQ(m->getGroup().size(), 1);

    ASSERT_THROW(cu->creGroDeleteEmloyee(202,678), logic_error);
}


TEST(chResTesting, CheckCreateResourse)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateResourse(34,"Empty", 56);
    Resourse* m = cu->getAllResourses()[34];
    //Resourse m("Empty", 34, 56, 1);
    EXPECT_EQ(m->getId(), 34);
    EXPECT_EQ(m->getName(), "Empty");
    EXPECT_EQ(m->getInitNumber(), 56);
    EXPECT_EQ(m->getType(), 0);
    EXPECT_EQ(cu->chResCreateResourse(34,"Empty", 56),1);
};

TEST(chResTesting, CheckCopyResourse)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateResourse(34,"Empty", 56);
    Resourse* m = cu->getAllResourses()[cu->chResCopyResourse(34)];
    //Resourse m("Empty", 34, 56, 1);
   // EXPECT_EQ(m->getId(), 34);
    EXPECT_EQ(m->getName(), "Empty_copy");
    EXPECT_EQ(m->getInitNumber(), 56);
    EXPECT_EQ(m->getType(), 0);
};

TEST(chResTesting, CheckChangeResourse)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateResourse(34);
    cu->chResChangeResourse(11,34,"Empty", 56);
    Resourse* m = cu->getAllResourses()[34];
    //Resourse m("Empty", 34, 56, 1);
    EXPECT_EQ(m->getId(), 34);
    EXPECT_EQ(m->getName(), "Empty");
    EXPECT_EQ(m->getInitNumber(), 56);
    EXPECT_EQ(m->getType(), 0);

    cu->chResCreateResourse(341);
    EXPECT_EQ(cu->chResChangeResourse(11,341,"Empty", 56),1);

};


TEST(chResTesting, CheckCreateid)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    EXPECT_EQ(cu->chResCreateResourseId(),1);
    cu->chResCreateMaterials(1,"Ruble", 803, "russian ruble");
    Materials* m = (Materials*)(cu->getAllResourses()[1]);
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 1);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);
    EXPECT_EQ(cu->chResCreateResourseId(),2);
}


TEST(rAssResTesting, RealAssTesting)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResourseReal(77,999,111,31);
    Project u = cu->getProject(999);
    Money* m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803-31);
    EXPECT_EQ(m->getType(), 1);

    cu->assResAssignResourseReal(77,999,110,1);
    m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803-31-1);
    EXPECT_EQ(m->getType(), 1);
};

TEST(rAssResTesting, RealAssDeactivateTesting)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResourseReal(77,999,111,31);
    Project u = cu->getProject(999);
    Money* m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803-31);
    EXPECT_EQ(m->getType(), 1);

    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getReal()[0].getIsActive(),1);

    m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    cu->assResDeactivateReal(77,999,111);
    //EXPECT_EQ(u.tasks[1].getReal().size(),0);
    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getReal()[0].getIsActive(),0);
    EXPECT_EQ(m->getInitNumber(), 803);
    cu->assResAssignResourseReal(77,999,111,31);
    cu->clearProjects();
    cu->assResDeactivateReal(77,999,111);
    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getReal()[0].getIsActive(),0);
    EXPECT_EQ(cu->getRes(77)->getInitNumber(),803);
    cu->assResAssignResourseReal(77,999,111,31);

    cu->clearProjects();
    cu->assResAssignResourseReal(77,999,110,31);
    cu->assResDeactivateReal(77,999,111);
    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[1].getReal()[0].getIsActive(),0);
    EXPECT_EQ(cu->getRes(77)->getInitNumber(),803-31);
    delete cu;
};


TEST(rAssResTesting, RealAssNotInTheList)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    EXPECT_EQ(cu->assResAssignResourseReal(922,999,111,31),2);
    cu->assResAssignResourseReal(922,999,110,1);
    cu->assResAssignResourseReal(922,2,3,1);

};

TEST(rAssResTesting, RealAssExistingTesting)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResourseReal(77,2,3,31);
    cu->assResAssignResourseReal(77,2,3,31);
    Project u = cu->getProject(2);
    Materials* m = (Materials*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803-31-31);
    EXPECT_EQ(m->getType(), 1);
    cu->fillAllResourses();
};

TEST(rAssResTesting, RealAssFewResourse)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    EXPECT_EQ(cu->assResAssignResourseReal(77,2,3,3122), 1);
};

TEST(pAssResTesting, PlannedAssTesting)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResoursePlanned(77,999,111,31);
    Project u = cu->getProject(999);
    Money* m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

    cu->assResAssignResoursePlanned(77,999,110,1);
    m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);
};

TEST(pAssResTesting, PlannedAssDeletePlanned)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResoursePlanned(77,999,111,31);
    Project u = cu->getProject(999);
    Money* m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getPlanned().size(),1);

   // m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    cu->assResDeletePlanned(77,999,111);
    //EXPECT_EQ(u.tasks[1].getPlanned().size(),0);
    u = cu->getProject(999);
     EXPECT_EQ(u.tasks[0].getPlanned().size(),0);
};

TEST(pAssResTesting, PlannedAssExistingTesting)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResoursePlanned(77,2,3,31);
    cu->assResAssignResoursePlanned(77,2,3,31);
    Project u = cu->getProject(2);
    Materials* m = (Materials*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);
    cu->fillAllResourses();
};

TEST(pAssResTesting, PlannedAssNotInTheList)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    EXPECT_EQ(cu->assResAssignResoursePlanned(922,999,111,31),2);
};

using namespace std;
/*
int main()
{

        DBInterface *db = new DBPatch;
       // DBPatch db;
        CUInterface *cu = new ControlUnit(db);
        cu->fillAllResourses();
     //   ControlUnit* cur = dynamic_cast<ControlUnit*>(cu);
         cout << "Hello World!" << endl;

         if (!cu->authCheckPassword("log2","pas2"))
            cout << "Authorization 1 successful"<< endl;
         else
             cout << "Authorization 1 failed!"<< endl;

         if (!cu->authCheckPassword("log1","pas2"))
            cout << "Authorization 2 successful"<< endl;
         else
             cout << "Authorization 2 failed!"<< endl;

         if (!cu->authCheckPassword("log4","pas4"))
            cout << "Authorization 3 successful"<< endl;
         else
             cout << "Authorization 3 failed!"<< endl;


         int r0 =cu->chResCreateResourseId();
         cu->chResCreateResourse(r0,"res",56);
 //       cout << *cu->getRes(r0)<< " )))" <<endl;
    //    cu->chResCreateStaff(0);

        int rmo =cu->chResCreateResourseId();
        cu->chResCreateMoney(rmo,"money",45,"much");
  //     cout << *cu->getRes(rmo)<< " )))" <<endl;
       cu->chResChangeMoney(11,rmo,"dollar",3578,"green");
  //    cout << *cu->getRes(rmo) <<endl;

      int rma =cu->chResCreateResourseId();
       cu->chResCreateMaterials(rma,"kirpich",48,"red");
     // cout << *cu->getRes(rma)<< " )))" <<endl;

      int k = cu->chResCreateResourseId();
      cu->chResCreateStaff( k,"an_ton",14,"hi","anton","antonovich","ivanov","visokaya");
 //     cout << *cu->getRes(k)<< " )))" <<endl;

     int g = cu->chResCreateResourseId();
      cu->chResCreateGroup(g,"group");
 //    cout << *cu->getRes(g)<< " )))cre" <<endl;

     cu->creGroAddEmloyee(k,g);
     cu->creGroAddEmloyee(20,g);
//      cout << *cu->getRes(g)<< " gr" <<endl;

      cu->chResChangeGroup(g,"gruppa");
  //    cout << *cu->getRes(g)<< " oldgroup" <<endl;

      int g1=cu->chResCopyResourse(g);
  //   cout << *cu->getRes(g1)<< " " <<endl;

     cu->chResChangeStaff(11111111,k,"sss",4,"head","semen","petrovich","simonov","zavuch","teacher");
  //   cout << *cu->getRes(k)<< " newstaff" <<endl;

 //    cout << *cu->getRes(g1)<< " newgroup" <<endl;

    cu->assResAssignResourseReal(rmo,2,3,3001);
    cu->assResAssignResourseReal(rmo,2,3,2);
    cu->assResAssignResourseReal(rmo,999,111,67);
    cu->assResAssignResourseReal(rma,2,3,25);
//    cu->assResAssignResourseReal(g,2,3,25);

    cu->assResAssignResoursePlanned(k,2,3,1);
    cu->assResAssignResoursePlanned(k,2,3,4);
    cu->assResAssignResoursePlanned(rmo,2,3,2);

    map<int,Resourse*> shar = cu->showAllResourses();
    map<int,int> shrra = cu->showResourseRealAssignments(rmo);
    map<int,int> shrpa = cu->showResoursePlannedAssignments(k);
    cu->assResDeactivateReal(rmo,2,3);
    cu->assResDeletePlanned(k,2,3);
    map<int,int> shrpa1 = cu->showResoursePlannedAssignments(k);
    cu->creGroDeleteEmloyee(k,g);
    cu->printAll();

    delete cu;
    return 0;
}

*/

int main(int argc, char* argv[])
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
