#include "DBPatch.h"

DBPatch::DBPatch()
{
    Resourse m("empty",0);
    Staff m1("kui", 20, 44, "borodach", "Ivan", "Nikolaevich", "Kuibishev", "director", "maths");
    Money m2("Ruble",77, 803, "russian ruble");
    Materials m3("concrete",922,1020,"concrete concrete");
    Staff m5("petr_ov", 23, 20, "good worker", "Sergey", "Petrovich", "Ivanov", "manager", "it");
    Group m4("progers",121);

    Resourse* p = new Resourse [sizeof(m)];
    *p = m;
    resdb.push_back(p);
    Staff* p1 = new Staff [sizeof(m1)];
    *p1 = m1;
    Resourse *c1 = dynamic_cast<Resourse*>(p1);
    resdb.push_back(c1);
    Money* p2 = new Money [sizeof(m2)];
    *p2 = m2;
    Resourse *c2 = dynamic_cast<Resourse*>(p2);
    resdb.push_back(c2);
    Materials* p3 = new Materials [sizeof(m3)];
    *p3 = m3;
    Resourse *c3 = dynamic_cast<Resourse*>(p3);
    resdb.push_back(c3);
    Group* p4 = new Group [sizeof(m4)];
    *p4 = m4;
    Resourse *c4 = dynamic_cast<Resourse*>(p4);
    resdb.push_back(c4);
    Staff* p5 = new Staff [sizeof(m5)];
    *p5 = m5;
    Resourse *c5 = dynamic_cast<Resourse*>(p5);
    resdb.push_back(c5);

    //Group*g = (Group*)resdb[4];
    //Staff* st = (Staff*)resdb[1];

    pair<int,int> a;
    a.first = 2;
    a.second = 0;
    projresdb.push_back(a);
    a.second =77;
    projresdb.push_back(a);
    a.first = 999;
    a.second = 922;
    projresdb.push_back(a);
    a.second =77;
    projresdb.push_back(a);

    p4->addEmployee(p1);
}

int DBPatch::checkPasswordDB(string login, string password)
{

    if ((login =="")||(password ==""))
        return 1;

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 1;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select password from  auth where login=\""+login+"\"";

    mysql_query (connect,query.c_str());
   //mysql_query("SELECT some_field FROM some_table WHERE some_other_field='" . mysql_escape_string($some_value) . "'");
     //cout<<query<<endl;
    unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    //cout<<"numrows "<<numrows<<endl;
    row= mysql_fetch_row(res_set);
    if(numrows == 0)
        return 1;
    //cout<<row[0]<<endl;
    if(row[0] == password)
        return 0;

    mysql_close (connect);
    return 1;
}


vector<Resourse*> DBPatch::getAllResoursesDB()
{
    vector<Resourse*> a;

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return a;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    MYSQL_RES *res_set1;
    MYSQL_ROW row1;
    unsigned long *lengths;

    string query = "select * from resourses";

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
  //  cout<<"numrows "<<numrows<<endl;

    int id;
    int initnum;
    int type;
    string name;
    string desc;
    string names;
    string surname;
    string patron;
    string posit;
    string special;
    Resourse* ptr;


    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);
        lengths = mysql_fetch_lengths(res_set);
        for(int j=1;j<10;j++)
        {
            if(lengths[j]==0)
                row[j] = "";
        }
        id = atoi(row[0]);
        initnum = atoi(row[1]);
        type = atoi(row[2]);
        name = row[3];
        desc = row[4];
        names = row[5];
        surname = row[6];
        patron = row[7];
        posit = row[8];
        special = row[9];

        if(type == 0)
        {
            ptr = new Resourse;
            *ptr = Resourse(name,id,initnum,0);
            a.push_back(ptr);
        }

        if(type == 1)
        {
            Money* m = new Money;
            *m = Money(name,id,initnum,desc);
            ptr = dynamic_cast<Resourse*>(m);
            a.push_back(ptr);
        }
        if(type == 2)
        {
            Materials* m = new Materials;
            *m = Materials(name,id,initnum,desc);
            ptr = dynamic_cast<Resourse*>(m);
            a.push_back(ptr);
        }
        if(type ==3)
        {
            Staff* m = new Staff;
            *m = Staff(name,id,initnum,desc,names,patron,surname,posit,special);
            ptr = dynamic_cast<Resourse*>(m);
            a.push_back(ptr);
        }
        if(type == 4)
        {
            Group* m = new Group;
            *m = Group(name,id);

            string query1 = "select idres from groups where idgroup ="+to_string(id);
            mysql_query (connect,query1.c_str());
             res_set1 = mysql_store_result(connect);
            unsigned int numrows1 = mysql_num_rows(res_set1);
            for(int j=0; j<numrows1; j++)
            {
                row1= mysql_fetch_row(res_set1);
                string namer = "name"+to_string(j);
                Staff* st = new Staff(namer,atoi(row1[0]),3,"","name","","name");
                m->addEmployee(st);

            }
            ptr = dynamic_cast<Resourse*>(m);
            a.push_back(ptr);
        }
    }
    mysql_close (connect);
    return a;
}


int DBPatch::saveResourseDB(Resourse* res)
{

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 1;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    MYSQL_RES *res_set1;
    MYSQL_ROW row1;
    unsigned long *lengths;

    string query = "delete from resourses where idres ="+to_string(res->getId());
    mysql_query (connect,query.c_str());

    int id = res->getId();
    int initnum = res->getInitNumber();
    int type = res->getType();
    string name = res->getName();

    if(type == 0)
    {
        string query2 = "insert into resourses values("+to_string(res->getId())+", "+to_string(res->getInitNumber())+", "+to_string(res->getType())+", \""+ res->getName()+"\", NULL, NULL, NULL, NULL,NULL,NULL )";
        mysql_query (connect,query2.c_str());
        //cout<<query2<<endl;
    }

    if(type == 1)
    {
        Money*m = dynamic_cast<Money*>(res);
        string query2 = "insert into resourses values("+to_string(m->getId())+", "+to_string(m->getInitNumber())+", "+to_string(m->getType())+", \""+ m->getName()+"\", \""+ m->getDescription()+"\", NULL, NULL, NULL, NULL,NULL )";
       int y = mysql_query (connect,query2.c_str());
        cout<<query2<<y<<endl;
    }
    if(type == 2)
    {
        Materials*m = dynamic_cast<Materials*>(res);
        string query2 = "insert into resourses values("+to_string(m->getId())+", "+to_string(m->getInitNumber())+", "+to_string(m->getType())+", \""+ m->getName()+"\", \""+ m->getDescription()+"\", NULL, NULL, NULL,NULL,NULL )";
       int y = mysql_query (connect,query2.c_str());
        //cout<<query2<<endl;
    }
    if(type ==3)
    {

        Staff*m = dynamic_cast<Staff*>(res);
        string query2 = "insert into resourses values("+to_string(m->getId())+", "+to_string(m->getInitNumber())+", "+to_string(m->getType())+", \""+ m->getName()+"\", \""+ m->getDescription()+"\", \""+ m->getNameS()+"\", \""+ m->getsurname()+"\", \""+ m->getPatronymic()+"\", \""+ m->getPosition()+"\", \""+ m->getSpeciality()+"\")";
        mysql_query (connect,query2.c_str());
    }
    if(type == 4)
    {
        Group*m = dynamic_cast<Group*>(res);
        string query2 = "insert into resourses values("+to_string(res->getId())+", "+to_string(res->getInitNumber())+", "+to_string(res->getType())+", \""+ m->getName()+"\", NULL, NULL, NULL,NULL,NULL, NULL )";
        mysql_query (connect,query2.c_str());
        //cout<<query2<<endl;

        query = "delete from groups where idgroup = "+to_string(res->getId());
        mysql_query (connect,query.c_str());

        for(int k=0; k<m->getGroupSize(); k++)
        {
            query2 = "insert into groups values("+to_string(res->getId())+", "+to_string(m->getGroup()[k]->getId())+ " )";
            mysql_query (connect,query2.c_str());
        }

    }


     mysql_close (connect);
     return 0;
}


Task DBPatch::getTaskDB(int idtask)
{
    Task a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return a;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select idtask, tname from tasks where idtask =" + to_string(idtask);

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    //cout<<"numrows "<<numrows<<endl;
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);
        //cout<<row[0]<<endl;
        return Task(atoi(row[0]),row[1]);
    }
    mysql_close (connect);
    return a;

}

map<int,string> DBPatch::showAllProjectsDB()
{
    map<int,string> a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return a;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select * from projects";

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    cout<<"numrows "<<numrows<<endl;
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);
        cout<<row[0]<<endl;
        a[atoi(row[0])] = row[1];
    }
    mysql_close (connect);
    return a;
}


map<int,string> DBPatch::showProjectTasksDB(int idproj)
{
    map<int,string> a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return a;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select idtask, tname from tasks where idproj =" + to_string(idproj);

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    //cout<<"numrows "<<numrows<<endl;
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);
        //cout<<row[0]<<endl;
        a[atoi(row[0])] = row[1];
    }
    mysql_close (connect);
    return a;


}


map<int,int> DBPatch::showResourseRealAssignmentsDB(int idres)
{
    map<int,int>  a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return a;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select idtask, ammount from realass where active=1 and idres =" + to_string(idres);

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);

        a[atoi(row[0])] = atoi(row[1]);
    }
    mysql_close (connect);

    return a;
}


map<int,int> DBPatch::showResoursePlannedAssignmentsDB(int idres)
{
    map<int,int>  a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return a;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select idtask, ammount from planass where idres =" + to_string(idres);

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);

        a[atoi(row[0])] = atoi(row[1]);
    }
    mysql_close (connect);

    return a;
}



Project DBPatch::getProjectDB(int idproj)
{
   Project a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return a;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select pname from projects where id =" + to_string(idproj);

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    //cout<<"numrows "<<numrows<<endl;
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);

        return Project(idproj,row[0]);
    }
    mysql_close (connect);
    return a;

}



vector<int> DBPatch::getProjectResoursesDB(int idproj)
{
    //cout<<"gpr1"<<endl;
    vector<int> vec;
   // map<int,string> a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return vec;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
       // cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select idres from projres where idproj =" + to_string(idproj);

    mysql_query (connect,query.c_str());
     //cout<<"gpr2"<<endl;
    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    //cout<<"numrows "<<numrows<<endl;
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);
        vec.push_back(atoi(row[0]));
    }
    mysql_close (connect);
     //cout<<"gpr3"<<vec[0]<<"  "<<vec[1] << "   " << vec[2]<<endl;
    return vec;
}


int DBPatch::saveProjectResoursesDB(int idproj,int idres)
{
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 1;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }


    string query = "insert into projres values(\"" + to_string(idproj)+"\",\""+to_string(idres)+"\")";
    mysql_query (connect,query.c_str());

    mysql_close (connect);
    return 0;
}


int DBPatch::saveRealAssDb(int idtask, RealAss ra)
{
    int idres = ra.getResourse()->getId();
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 1;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from  realass where idtask = " + to_string(idtask)+" and idres = "+to_string(idres);
    mysql_query (connect,query.c_str());
    query = "insert into realass values(\"" + to_string(idtask)+"\",\""+to_string(idres)+"\","+to_string(ra.getAmmount())+", "+to_string(ra.getIsActive())+")";
    mysql_query (connect,query.c_str());

    mysql_close (connect);
    return 0;
}


int DBPatch::savePlannedAssDb(int idtask, PlannedAss ra)
{
    int idres = ra.getResourse()->getId();
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 1;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from  planass where idtask = " + to_string(idtask)+" and idres = "+to_string(idres);
    mysql_query (connect,query.c_str());
    query = "insert into planass values(\"" + to_string(idtask)+"\",\""+to_string(idres)+"\","+to_string(ra.getAmmount())+")";
    mysql_query (connect,query.c_str());

    mysql_close (connect);
    return 0;
}


int DBPatch::deletePlannedAssDb(int idtask,int idres)
{
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 1;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    string query = "delete from planass where idtask = " + to_string(idtask)+" and idres = "+to_string(idres);
    mysql_query (connect,query.c_str());

    mysql_close (connect);
    return 0;
}



int DBPatch::getRealDB(int idtask,int idres)
{
   Project a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 0;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select ammount from realass where active = 1 and idtask =" + to_string(idtask) + " and idres =" + to_string(idres);

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    //cout<<"numrows "<<numrows<<endl;
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);
        mysql_close (connect);
        return atoi(row[0]);

    }
    mysql_close (connect);
    return 0;
}



int DBPatch::getPlannedDB(int idtask,int idres)
{
   Project a;
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL Initialization failed";
    return 0;
    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }

    MYSQL_RES *res_set;
    MYSQL_ROW row;
    string query = "select ammount from planass where idtask =" + to_string(idtask) + " and idres =" + to_string(idres);

    mysql_query (connect,query.c_str());

    //unsigned int i =0;
       res_set = mysql_store_result(connect);
    unsigned int numrows = mysql_num_rows(res_set);
    cout<<"numrows "<<numrows<<endl;
    for(int i=0; i<numrows; i++)
    {
        row= mysql_fetch_row(res_set);
        mysql_close (connect);
        return atoi(row[0]);

    }
    mysql_close (connect);
    return 0;
}
