#include "DBInterface.h"
#include </usr/include/mysql/mysql.h>
#include </usr/include/mysql/my_global.h>
#include </usr/include/mysql/my_config.h>
#define SERVER "127.0.0.1"
#define USER "root"
#define PASSWORD "anna"
#define DATABASE "resourses"
#ifndef DBPATCH_H
#define DBPATCH_H

class DBPatch :
        public DBInterface
{
public:
    vector<Resourse*>resdb;
    vector<pair<int,RealAss>> rassdb;
    vector<pair<int,PlannedAss>> passdb;
    vector<pair<int,int>> projresdb;

    DBPatch();
    virtual int checkPasswordDB(string login, string password) override;
    virtual vector<Resourse *> getAllResoursesDB() override;

    virtual int saveResourseDB(Resourse* res) override;

    virtual map<int,string> showAllProjectsDB() override;
    virtual map<int,string> showProjectTasksDB(int idproj) override;
    virtual  map<int,int> showResourseRealAssignmentsDB(int idres) override;
    virtual  map<int,int> showResoursePlannedAssignmentsDB(int idres) override;

    virtual Project getProjectDB(int idproj) override;
    virtual Task getTaskDB(int idtask) override;
    virtual vector<int> getProjectResoursesDB(int idproj) override;
    virtual int saveProjectResoursesDB(int idproj,int idres) override;

    virtual int saveRealAssDb(int idtask, RealAss ra) override;
    virtual int savePlannedAssDb(int idtask, PlannedAss ra)  override;
    virtual int deletePlannedAssDb(int idtask,int idres) override;
    virtual int getRealDB(int idtask, int idres) override;
    virtual int getPlannedDB(int idtask, int idres) override;
};

#endif // DBPATCH_H
