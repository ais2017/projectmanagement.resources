/****************************************************************************
** Meta object code from reading C++ file 'model_main.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../GraficCU/model_main.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'model_main.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_model_main_t {
    QByteArrayData data[19];
    char stringdata[357];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_model_main_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_model_main_t qt_meta_stringdata_model_main = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 18),
QT_MOC_LITERAL(2, 30, 0),
QT_MOC_LITERAL(3, 31, 28),
QT_MOC_LITERAL(4, 60, 16),
QT_MOC_LITERAL(5, 77, 4),
QT_MOC_LITERAL(6, 82, 17),
QT_MOC_LITERAL(7, 100, 20),
QT_MOC_LITERAL(8, 121, 20),
QT_MOC_LITERAL(9, 142, 20),
QT_MOC_LITERAL(10, 163, 19),
QT_MOC_LITERAL(11, 183, 29),
QT_MOC_LITERAL(12, 213, 17),
QT_MOC_LITERAL(13, 231, 21),
QT_MOC_LITERAL(14, 253, 21),
QT_MOC_LITERAL(15, 275, 23),
QT_MOC_LITERAL(16, 299, 18),
QT_MOC_LITERAL(17, 318, 18),
QT_MOC_LITERAL(18, 337, 18)
    },
    "model_main\0on_showres_clicked\0\0"
    "on_listWidgetres_itemClicked\0"
    "QListWidgetItem*\0item\0on_create_clicked\0"
    "on_addemploy_clicked\0on_delemploy_clicked\0"
    "on_changeres_clicked\0on_showproj_clicked\0"
    "on_listWidgetproj_itemClicked\0"
    "on_assign_clicked\0on_assignplan_clicked\0"
    "on_pushButton_clicked\0on_addrestoproj_clicked\0"
    "on_copyres_clicked\0on_delreal_clicked\0"
    "on_delplan_clicked\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_model_main[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   89,    2, 0x08,
       3,    1,   90,    2, 0x08,
       6,    0,   93,    2, 0x08,
       7,    0,   94,    2, 0x08,
       8,    0,   95,    2, 0x08,
       9,    0,   96,    2, 0x08,
      10,    0,   97,    2, 0x08,
      11,    1,   98,    2, 0x08,
      12,    0,  101,    2, 0x08,
      13,    0,  102,    2, 0x08,
      14,    0,  103,    2, 0x08,
      15,    0,  104,    2, 0x08,
      16,    0,  105,    2, 0x08,
      17,    0,  106,    2, 0x08,
      18,    0,  107,    2, 0x08,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void model_main::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        model_main *_t = static_cast<model_main *>(_o);
        switch (_id) {
        case 0: _t->on_showres_clicked(); break;
        case 1: _t->on_listWidgetres_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 2: _t->on_create_clicked(); break;
        case 3: _t->on_addemploy_clicked(); break;
        case 4: _t->on_delemploy_clicked(); break;
        case 5: _t->on_changeres_clicked(); break;
        case 6: _t->on_showproj_clicked(); break;
        case 7: _t->on_listWidgetproj_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 8: _t->on_assign_clicked(); break;
        case 9: _t->on_assignplan_clicked(); break;
        case 10: _t->on_pushButton_clicked(); break;
        case 11: _t->on_addrestoproj_clicked(); break;
        case 12: _t->on_copyres_clicked(); break;
        case 13: _t->on_delreal_clicked(); break;
        case 14: _t->on_delplan_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject model_main::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_model_main.data,
      qt_meta_data_model_main,  qt_static_metacall, 0, 0}
};


const QMetaObject *model_main::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *model_main::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_model_main.stringdata))
        return static_cast<void*>(const_cast< model_main*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int model_main::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
