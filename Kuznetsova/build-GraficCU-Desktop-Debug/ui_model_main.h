/********************************************************************************
** Form generated from reading UI file 'model_main.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODEL_MAIN_H
#define UI_MODEL_MAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_model_main
{
public:
    QWidget *centralWidget;
    QPushButton *showres;
    QListWidget *listWidgetres;
    QPlainTextEdit *additionalinfow;
    QPushButton *create;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *position_2;
    QLabel *Name;
    QLabel *number;
    QLabel *description;
    QLabel *names;
    QLabel *surname;
    QLabel *Name_3;
    QLabel *speciality;
    QLabel *position;
    QVBoxLayout *verticalLayout;
    QLineEdit *typew;
    QLineEdit *namew;
    QLineEdit *numberw;
    QLineEdit *descriptionw;
    QLineEdit *namesw;
    QLineEdit *surnamew;
    QLineEdit *patronymicw;
    QLineEdit *specialityw;
    QLineEdit *positionw;
    QLineEdit *idmembersw;
    QLabel *label_6;
    QPushButton *addemploy;
    QPushButton *delemploy;
    QPushButton *changeres;
    QListWidget *listWidgetproj;
    QPushButton *showproj;
    QListWidget *listWidgettask;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QPushButton *assign;
    QLineEdit *assignw;
    QLabel *label_11;
    QPushButton *assignplan;
    QListWidget *listWidgetprojres;
    QListWidget *listWidgetassreal;
    QListWidget *listWidgetassplan;
    QLabel *label_12;
    QLabel *label_13;
    QPushButton *pushButton;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QLabel *position_3;
    QLabel *password;
    QVBoxLayout *verticalLayout_3;
    QLineEdit *loginw;
    QLineEdit *passwordw;
    QPushButton *addrestoproj;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *copyres;
    QPushButton *delreal;
    QPushButton *delplan;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *model_main)
    {
        if (model_main->objectName().isEmpty())
            model_main->setObjectName(QStringLiteral("model_main"));
        model_main->resize(838, 507);
        centralWidget = new QWidget(model_main);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        showres = new QPushButton(centralWidget);
        showres->setObjectName(QStringLiteral("showres"));
        showres->setGeometry(QRect(-1, 199, 121, 27));
        listWidgetres = new QListWidget(centralWidget);
        listWidgetres->setObjectName(QStringLiteral("listWidgetres"));
        listWidgetres->setGeometry(QRect(-1, 229, 121, 192));
        additionalinfow = new QPlainTextEdit(centralWidget);
        additionalinfow->setObjectName(QStringLiteral("additionalinfow"));
        additionalinfow->setGeometry(QRect(129, 229, 211, 191));
        create = new QPushButton(centralWidget);
        create->setObjectName(QStringLiteral("create"));
        create->setGeometry(QRect(570, 300, 121, 27));
        QPalette palette;
        QBrush brush(QColor(170, 255, 127, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        create->setPalette(palette);
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(600, 0, 206, 295));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        position_2 = new QLabel(layoutWidget);
        position_2->setObjectName(QStringLiteral("position_2"));

        verticalLayout_2->addWidget(position_2);

        Name = new QLabel(layoutWidget);
        Name->setObjectName(QStringLiteral("Name"));

        verticalLayout_2->addWidget(Name);

        number = new QLabel(layoutWidget);
        number->setObjectName(QStringLiteral("number"));

        verticalLayout_2->addWidget(number);

        description = new QLabel(layoutWidget);
        description->setObjectName(QStringLiteral("description"));

        verticalLayout_2->addWidget(description);

        names = new QLabel(layoutWidget);
        names->setObjectName(QStringLiteral("names"));

        verticalLayout_2->addWidget(names);

        surname = new QLabel(layoutWidget);
        surname->setObjectName(QStringLiteral("surname"));

        verticalLayout_2->addWidget(surname);

        Name_3 = new QLabel(layoutWidget);
        Name_3->setObjectName(QStringLiteral("Name_3"));

        verticalLayout_2->addWidget(Name_3);

        speciality = new QLabel(layoutWidget);
        speciality->setObjectName(QStringLiteral("speciality"));

        verticalLayout_2->addWidget(speciality);

        position = new QLabel(layoutWidget);
        position->setObjectName(QStringLiteral("position"));

        verticalLayout_2->addWidget(position);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        typew = new QLineEdit(layoutWidget);
        typew->setObjectName(QStringLiteral("typew"));

        verticalLayout->addWidget(typew);

        namew = new QLineEdit(layoutWidget);
        namew->setObjectName(QStringLiteral("namew"));

        verticalLayout->addWidget(namew);

        numberw = new QLineEdit(layoutWidget);
        numberw->setObjectName(QStringLiteral("numberw"));

        verticalLayout->addWidget(numberw);

        descriptionw = new QLineEdit(layoutWidget);
        descriptionw->setObjectName(QStringLiteral("descriptionw"));

        verticalLayout->addWidget(descriptionw);

        namesw = new QLineEdit(layoutWidget);
        namesw->setObjectName(QStringLiteral("namesw"));

        verticalLayout->addWidget(namesw);

        surnamew = new QLineEdit(layoutWidget);
        surnamew->setObjectName(QStringLiteral("surnamew"));

        verticalLayout->addWidget(surnamew);

        patronymicw = new QLineEdit(layoutWidget);
        patronymicw->setObjectName(QStringLiteral("patronymicw"));

        verticalLayout->addWidget(patronymicw);

        specialityw = new QLineEdit(layoutWidget);
        specialityw->setObjectName(QStringLiteral("specialityw"));

        verticalLayout->addWidget(specialityw);

        positionw = new QLineEdit(layoutWidget);
        positionw->setObjectName(QStringLiteral("positionw"));

        verticalLayout->addWidget(positionw);


        horizontalLayout->addLayout(verticalLayout);

        idmembersw = new QLineEdit(centralWidget);
        idmembersw->setObjectName(QStringLiteral("idmembersw"));
        idmembersw->setGeometry(QRect(680, 330, 131, 27));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(580, 330, 121, 21));
        addemploy = new QPushButton(centralWidget);
        addemploy->setObjectName(QStringLiteral("addemploy"));
        addemploy->setGeometry(QRect(570, 360, 131, 27));
        delemploy = new QPushButton(centralWidget);
        delemploy->setObjectName(QStringLiteral("delemploy"));
        delemploy->setGeometry(QRect(700, 360, 121, 27));
        QPalette palette1;
        QBrush brush1(QColor(255, 139, 139, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        delemploy->setPalette(palette1);
        delemploy->setMouseTracking(true);
        changeres = new QPushButton(centralWidget);
        changeres->setObjectName(QStringLiteral("changeres"));
        changeres->setGeometry(QRect(690, 300, 131, 27));
        listWidgetproj = new QListWidget(centralWidget);
        listWidgetproj->setObjectName(QStringLiteral("listWidgetproj"));
        listWidgetproj->setGeometry(QRect(0, 31, 101, 141));
        showproj = new QPushButton(centralWidget);
        showproj->setObjectName(QStringLiteral("showproj"));
        showproj->setGeometry(QRect(0, 0, 101, 27));
        listWidgettask = new QListWidget(centralWidget);
        listWidgettask->setObjectName(QStringLiteral("listWidgettask"));
        listWidgettask->setGeometry(QRect(110, 30, 101, 141));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(130, 10, 67, 17));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(200, 10, 121, 17));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(360, 230, 161, 17));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(360, 250, 161, 17));
        assign = new QPushButton(centralWidget);
        assign->setObjectName(QStringLiteral("assign"));
        assign->setGeometry(QRect(290, 110, 101, 31));
        assignw = new QLineEdit(centralWidget);
        assignw->setObjectName(QStringLiteral("assignw"));
        assignw->setGeometry(QRect(230, 120, 51, 41));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(220, 100, 111, 17));
        assignplan = new QPushButton(centralWidget);
        assignplan->setObjectName(QStringLiteral("assignplan"));
        assignplan->setGeometry(QRect(290, 140, 101, 31));
        listWidgetprojres = new QListWidget(centralWidget);
        listWidgetprojres->setObjectName(QStringLiteral("listWidgetprojres"));
        listWidgetprojres->setGeometry(QRect(220, 30, 121, 61));
        listWidgetassreal = new QListWidget(centralWidget);
        listWidgetassreal->setObjectName(QStringLiteral("listWidgetassreal"));
        listWidgetassreal->setGeometry(QRect(360, 290, 71, 131));
        listWidgetassplan = new QListWidget(centralWidget);
        listWidgetassplan->setObjectName(QStringLiteral("listWidgetassplan"));
        listWidgetassplan->setGeometry(QRect(440, 290, 71, 131));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(360, 270, 61, 17));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(440, 270, 61, 17));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(530, 30, 61, 61));
        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(350, 30, 181, 64));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        position_3 = new QLabel(layoutWidget1);
        position_3->setObjectName(QStringLiteral("position_3"));

        verticalLayout_4->addWidget(position_3);

        password = new QLabel(layoutWidget1);
        password->setObjectName(QStringLiteral("password"));

        verticalLayout_4->addWidget(password);


        horizontalLayout_3->addLayout(verticalLayout_4);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        loginw = new QLineEdit(layoutWidget1);
        loginw->setObjectName(QStringLiteral("loginw"));

        verticalLayout_3->addWidget(loginw);

        passwordw = new QLineEdit(layoutWidget1);
        passwordw->setObjectName(QStringLiteral("passwordw"));

        verticalLayout_3->addWidget(passwordw);


        horizontalLayout_3->addLayout(verticalLayout_3);

        addrestoproj = new QPushButton(centralWidget);
        addrestoproj->setObjectName(QStringLiteral("addrestoproj"));
        addrestoproj->setGeometry(QRect(129, 199, 211, 27));
        layoutWidget2 = new QWidget(centralWidget);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(150, 430, 519, 19));
        horizontalLayout_4 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget2);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_4->addWidget(label_2);

        label = new QLabel(layoutWidget2);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_4->addWidget(label);

        label_3 = new QLabel(layoutWidget2);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_4->addWidget(label_3);

        label_4 = new QLabel(layoutWidget2);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        label_5 = new QLabel(layoutWidget2);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_4->addWidget(label_5);

        copyres = new QPushButton(centralWidget);
        copyres->setObjectName(QStringLiteral("copyres"));
        copyres->setGeometry(QRect(350, 200, 161, 27));
        delreal = new QPushButton(centralWidget);
        delreal->setObjectName(QStringLiteral("delreal"));
        delreal->setGeometry(QRect(400, 110, 101, 31));
        delplan = new QPushButton(centralWidget);
        delplan->setObjectName(QStringLiteral("delplan"));
        delplan->setGeometry(QRect(400, 140, 101, 31));
        model_main->setCentralWidget(centralWidget);
        layoutWidget->raise();
        layoutWidget->raise();
        layoutWidget->raise();
        showres->raise();
        listWidgetres->raise();
        additionalinfow->raise();
        create->raise();
        idmembersw->raise();
        label_6->raise();
        addemploy->raise();
        delemploy->raise();
        changeres->raise();
        listWidgetproj->raise();
        showproj->raise();
        listWidgettask->raise();
        label_7->raise();
        label_8->raise();
        label_9->raise();
        label_10->raise();
        assign->raise();
        assignw->raise();
        label_11->raise();
        assignplan->raise();
        listWidgetprojres->raise();
        listWidgetassreal->raise();
        listWidgetassplan->raise();
        label_12->raise();
        label_13->raise();
        pushButton->raise();
        addrestoproj->raise();
        copyres->raise();
        delreal->raise();
        delplan->raise();
        menuBar = new QMenuBar(model_main);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 838, 25));
        model_main->setMenuBar(menuBar);
        mainToolBar = new QToolBar(model_main);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        model_main->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(model_main);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        model_main->setStatusBar(statusBar);

        retranslateUi(model_main);

        QMetaObject::connectSlotsByName(model_main);
    } // setupUi

    void retranslateUi(QMainWindow *model_main)
    {
        model_main->setWindowTitle(QApplication::translate("model_main", "model_main", 0));
        showres->setText(QApplication::translate("model_main", "Show Resourses", 0));
        create->setText(QApplication::translate("model_main", "Create Resourse", 0));
        position_2->setText(QApplication::translate("model_main", "Type: ", 0));
        Name->setText(QApplication::translate("model_main", "Name/login: ", 0));
        number->setText(QApplication::translate("model_main", "Number: ", 0));
        description->setText(QApplication::translate("model_main", "Description: ", 0));
        names->setText(QApplication::translate("model_main", "Name: ", 0));
        surname->setText(QApplication::translate("model_main", "Surname: ", 0));
        Name_3->setText(QApplication::translate("model_main", "Patronymic: ", 0));
        speciality->setText(QApplication::translate("model_main", "Speciality: ", 0));
        position->setText(QApplication::translate("model_main", "Position: ", 0));
        label_6->setText(QApplication::translate("model_main", "Member's ID", 0));
        addemploy->setText(QApplication::translate("model_main", "Add Employee", 0));
        delemploy->setText(QApplication::translate("model_main", "Delete Employee", 0));
        changeres->setText(QApplication::translate("model_main", "Change Resourse", 0));
        showproj->setText(QApplication::translate("model_main", "Show Projects", 0));
        label_7->setText(QApplication::translate("model_main", "Tasks:", 0));
        label_8->setText(QApplication::translate("model_main", "Project resourses:", 0));
        label_9->setText(QApplication::translate("model_main", "Resourse assignments", 0));
        label_10->setText(QApplication::translate("model_main", "Task: ammount", 0));
        assign->setText(QApplication::translate("model_main", "Assign real", 0));
        label_11->setText(QApplication::translate("model_main", "Ammount:", 0));
        assignplan->setText(QApplication::translate("model_main", "Assign plan", 0));
        label_12->setText(QApplication::translate("model_main", "Real:", 0));
        label_13->setText(QApplication::translate("model_main", "Planned:", 0));
        pushButton->setText(QApplication::translate("model_main", "Log in", 0));
        position_3->setText(QApplication::translate("model_main", "Login", 0));
        password->setText(QApplication::translate("model_main", "Password", 0));
        addrestoproj->setText(QApplication::translate("model_main", "Add Resoure to project", 0));
        label_2->setText(QApplication::translate("model_main", "Type 0: Empty", 0));
        label->setText(QApplication::translate("model_main", "Type 1: Money", 0));
        label_3->setText(QApplication::translate("model_main", "Type 2: Materials", 0));
        label_4->setText(QApplication::translate("model_main", "Type 3: Staff", 0));
        label_5->setText(QApplication::translate("model_main", "Type 4: Group", 0));
        copyres->setText(QApplication::translate("model_main", "Copy resourse", 0));
        delreal->setText(QApplication::translate("model_main", "Delete real", 0));
        delplan->setText(QApplication::translate("model_main", "Delete plan", 0));
    } // retranslateUi

};

namespace Ui {
    class model_main: public Ui_model_main {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODEL_MAIN_H
