#include <iostream>

#include <fstream>
#include "googletest-release-1.8.1/googletest/include/gtest/gtest.h"
#include "model_main.h"
#include <QApplication>
#include <stdio.h>
//#include <sqlite3.h>
//#include <QString>

#include </usr/include/mysql/mysql.h>
#include </usr/include/mysql/my_global.h>
#include </usr/include/mysql/my_config.h>

#define SERVER "127.0.0.1"
#define USER "root"
#define PASSWORD "anna"
#define DATABASE "resourses"
using namespace std;

//  To run statistics:
//  cd /home/anna/AIS/Kuznetsova/build-GraficCU-Desktop-Debug
//  lcov -t "result" -o ex_test.info -c -d .
//  genhtml -o res ex_test.info


TEST(AuthTesting, AuthCheckPassword)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);

    EXPECT_EQ(cu->authCheckPassword("log1","pas2"), 1);
    //cout<<"chpas1"<<endl;
    EXPECT_EQ(cu->authCheckPassword("root","root"), 0);
    //cout<<"chpas2"<<endl;
    EXPECT_EQ(cu->authCheckPassword("log4","pas4"), 1);
};

TEST(chResTesting, CheckCreateMoney)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
     cu->fillAllResourses();
    cu->chResCreateMoney(78,"RubleT", 803, "test");
    Money* m = (Money*)(cu->getAllResourses()[78]);
    EXPECT_EQ(m->getDescription(), "test");
    EXPECT_EQ(m->getId(), 78);
    EXPECT_EQ(m->getName(), "RubleT");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }



    EXPECT_EQ(cu->chResCreateMoney(78,"RubleT", 803, "russian ruble"), 1);
    string query = "delete from resourses where idres =78";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};


TEST(chResTesting, CheckCopyMoney)
{
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Ruble\"";
    mysql_query (connect,query.c_str());
    query = "delete from resourses where rname = \"Ruble_copy\"";
        mysql_query (connect,query.c_str());
        query = "delete from resourses where rname = \"Dollar\"";
            mysql_query (connect,query.c_str());


    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMoney(78,"RubleT", 803, "test");

    Money* m = (Money*)(cu->getAllResourses()[cu->chResCopyResourse(78)]);
    EXPECT_EQ(m->getDescription(), "test");
    EXPECT_EQ(m->getId(), 1);
    EXPECT_EQ(m->getName(), "RubleT_copy");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);





    EXPECT_EQ(cu->chResCreateMoney(78,"RubleT", 803, "test"), 1);
    query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(chResTesting, CheckchangeMoney)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMoney(78,"Dollar",800000, "american dollar");
    cu->chResChangeMoney(111,78,"Ruble", 803, "test");
    Money* m = (Money*)(cu->getAllResourses()[78]);
    EXPECT_EQ(m->getDescription(), "test");
    EXPECT_EQ(m->getId(), 78);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

  cu->chResCreateMoney(771,"Dollar1",800000, "test");
    EXPECT_EQ(cu->chResChangeMoney(111,771,"Ruble",800000, "test"),1);


    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }



    EXPECT_EQ(cu->chResCreateMoney(78,"Ruble", 803, "russian ruble"), 1);
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(chResTesting, CheckCreateMoneyNegativeAmmount)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMoney(77,"Ruble", -803, "russian ruble"), logic_error);

};

TEST(chResTesting, CheckCreateMoneyNegativeID)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMoney(-77,"Ruble", 803, "russian ruble"), logic_error);
};

TEST(chResTesting, CheckChangeAmmount)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMoney(77,"Ruble", 803, "test");

    cu->chAmmChangeAmmount(77,95);
    Money* m = (Money*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 898);

    cu->chAmmChangeAmmount(77,-898);
    m = (Money*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 0);

    ASSERT_THROW(cu->chAmmChangeAmmount(77,-10), logic_error);


    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }



    EXPECT_EQ(cu->chResCreateMoney(78,"Ruble", 803, "russian ruble"), 1);
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(chResTesting, CheckCreateMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Ruble", 803, "test");
    Materials* m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getDescription(), "test");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);

    cu->chResCreateMaterials(99,"Dollar",800000, "test");
    Materials* m1 = (Materials*)(cu->getAllResourses()[99]);
    EXPECT_EQ(m1->getDescription(), "test");
    EXPECT_EQ(m1->getId(), 99);
    EXPECT_EQ(m1->getName(), "Dollar");
    EXPECT_EQ(m1->getInitNumber(),800000);
    EXPECT_EQ(m1->getType(), 2);
    EXPECT_EQ(cu->chResCreateMaterials(99,"Dollar",800000, "test"), 1);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(chResTesting, CheckCopyMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Ruble", 803, "test");
    Materials* m = (Materials*)(cu->getAllResourses()[cu->chResCopyResourse(77)]);
    EXPECT_EQ(m->getDescription(), "test");
   // EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble_copy");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(chResTesting, CheckCopyMaterialsShow)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();

    cu->showAllProjects();

    cu->showAllResourses();

    cu->showProjectResourses(2);

    cu->showProjectTasks(2);

    cu->showResoursePlannedAssignments(77);

    cu->showResourseRealAssignments(77);

};

TEST(chResTesting, CheckchangeMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Dollar",800000, "american dollar");
    cu->chResChangeMaterials(111,77,"Ruble", 803, "test");
    Materials* m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getDescription(), "test");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);
    cu->chResCreateMaterials(771,"Dollar1",800000, "test");
    EXPECT_EQ(cu->chResChangeMaterials(111,771,"Ruble",800000, "test"),1);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);

  //  ASSERT_THROW(cu->chResChangeMaterials(111,77,"Ruble", 803, "russian ruble"), logic_error);
};


TEST(chResTesting, CheckCreateMaterialsNegativeAmmount)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMaterials(77,"Ruble", -803, "russian ruble"), logic_error);

};

TEST(chResTesting, CheckCreateMaterialsNegativeID)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    ASSERT_THROW(cu->chResCreateMaterials(-77,"Ruble", 803, "russian ruble"), logic_error);
};

TEST(chResTesting, CheckChangeAmmountMaterials)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateMaterials(77,"Ruble", 803, "test");

    cu->chAmmChangeAmmount(77,95);
    Materials* m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 898);

    cu->chAmmChangeAmmount(77,-898);
    m = (Materials*)(cu->getAllResourses()[77]);
    EXPECT_EQ(m->getInitNumber(), 0);

    ASSERT_THROW(cu->chAmmChangeAmmount(77,-10), logic_error);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};


TEST(chResTesting, CheckCreateStaff)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateStaff(21, "Kuibish", 44, "test", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* m = (Staff*)(cu->getAllResourses()[21]);

    EXPECT_EQ(m->getNameS(), "Ivan");
    EXPECT_EQ(m->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getsurname(), "kui_i");
    EXPECT_EQ(m->getPosition(), "director");
    EXPECT_EQ(m->getSpeciality(), "maths");
    EXPECT_EQ(m->getDescription(), "test");
    EXPECT_EQ(m->getId(), 21);
    EXPECT_EQ(m->getName(), "Kuibish");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 3);
    EXPECT_EQ(cu->chResCreateStaff(21, "Kuibish", 44, "test", "Ivan", "Nikolaevich", "kui_i", "director", "maths"), 1);


    ASSERT_THROW(m->changeAmmount(-10006), logic_error);
    ASSERT_THROW(m->setInitNumber(500), logic_error);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(chResTesting, CheckCopyStaff)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateStaff(21, "Kuibish", 44, "test", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* m = (Staff*)(cu->getAllResourses()[cu->chResCopyResourse(21)]);

    EXPECT_EQ(m->getNameS(), "Ivan");
    EXPECT_EQ(m->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getsurname(), "kui_i");
    EXPECT_EQ(m->getPosition(), "director");
    EXPECT_EQ(m->getSpeciality(), "maths");
    EXPECT_EQ(m->getDescription(), "test");
  //  EXPECT_EQ(m->getId(), 21);
    EXPECT_EQ(m->getName(), "Kuibish_copy");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 3);
    EXPECT_EQ(cu->chResCreateStaff(21, "Kuibish", 44, "test", "Ivan", "Nikolaevich", "kui_i", "director", "maths"), 1);
    cu->chResCopyResourse(21);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};


TEST(chResTesting, CheckChangeStaff)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateStaff(21);
    cu->chResChangeStaff(11111111,21, "Kuibish", 44, "test", "Ivan", "Nikolaevich", "kui_i", "director", "maths");

    Staff* m = (Staff*)(cu->getAllResourses()[21]);

    EXPECT_EQ(m->getNameS(), "Ivan");
    EXPECT_EQ(m->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getsurname(), "kui_i");
    EXPECT_EQ(m->getPosition(), "director");
    EXPECT_EQ(m->getSpeciality(), "maths");
    EXPECT_EQ(m->getDescription(), "test");
    EXPECT_EQ(m->getId(), 21);
    EXPECT_EQ(m->getName(), "Kuibish");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 3);

    cu->chResCreateStaff(211);
    EXPECT_EQ(cu->chResChangeStaff(11111111,21, "Kuibish", 44, "test", "Ivan", "Nikolaevich", "kui_i", "director", "maths"),1);


    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where description = \"test\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);

};


TEST(creGroTesting, CheckCreateGroup)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(679,"Draw");
    Group* m = (Group*)(cu->getAllResourses()[679]);
    EXPECT_EQ(m->getGroup(), vector<Staff*>(0));
    EXPECT_EQ(m->getId(), 679);
    EXPECT_EQ(m->getName(), "Draw");
    EXPECT_EQ(m->getInitNumber(), 0);
    EXPECT_EQ(m->getType(), 4);
    EXPECT_EQ(cu->chResCreateGroup(679,"Draw"), 1);
     cu->chResCreateGroup(6790,"Draw1");
     EXPECT_EQ(cu->chResCreateGroup(679,"Draw"),1);

     MYSQL *connect;
     connect=mysql_init(NULL);
     if (!connect)
     {
     cout<<"MySQL testInitialization failed";

     }
     connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
     if (connect)
     {
         //cout<<"connection Succeeded\n";
     }
     else
     {
         cout<<"connection failed\n";
     }
     string query = "delete from resourses where id = 679";
     mysql_query (connect,query.c_str());
     mysql_close (connect);
};

TEST(creGroTesting, CheckCopyGroup)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(679,"Draw");
    Group* m = (Group*)(cu->getAllResourses()[cu->chResCopyResourse(679)]);
    EXPECT_EQ(m->getGroup(), vector<Staff*>(0));
  //  EXPECT_EQ(m->getId(), 679);
    EXPECT_EQ(m->getName(), "Draw_copy");
    EXPECT_EQ(m->getInitNumber(), 0);
    EXPECT_EQ(m->getType(), 4);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Draw_copy\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(creGroTesting, CheckChangeGroup)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(679);
    cu->chResChangeGroup(679,"Draw");
    Group* m = (Group*)(cu->getAllResourses()[679]);
    EXPECT_EQ(m->getGroup(), vector<Staff*>(0));
    EXPECT_EQ(m->getId(), 679);
    EXPECT_EQ(m->getName(), "Draw");
    EXPECT_EQ(m->getInitNumber(), 0);
    EXPECT_EQ(m->getType(), 4);
    EXPECT_EQ(cu->chResCreateGroup(679,"Draw"), 1);
    cu->chResCreateGroup(6791);
    EXPECT_EQ(cu->chResChangeGroup(6791,"Draw"),1);
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Draw\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);

};

TEST(creGroTesting, AddEmployee)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(679,"Draw");
    cu->chResCreateStaff(21, "Kuibish", 44, "test", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* st = (Staff*)(cu->getAllResourses()[21]);
    Group* m = (Group*)(cu->getAllResourses()[679]);

    cu->creGroAddEmloyee(21,679);

    EXPECT_EQ(m->getId(), 679);
    EXPECT_EQ(m->getName(), "Draw");
    EXPECT_EQ(m->getInitNumber(), 44);
    EXPECT_EQ(m->getType(), 4);
    EXPECT_EQ(m->getGroup().size(), 1);

    EXPECT_EQ(m->getGroup()[0]->getNameS(), "Ivan");
    EXPECT_EQ(m->getGroup()[0]->getPatronymic(), "Nikolaevich");
    EXPECT_EQ(m->getGroup()[0]->getsurname(), "kui_i");
    EXPECT_EQ(m->getGroup()[0]->getPosition(), "director");
    EXPECT_EQ(m->getGroup()[0]->getSpeciality(), "maths");
    EXPECT_EQ(m->getGroup()[0]->getDescription(), "test");
    EXPECT_EQ(m->getGroup()[0]->getId(), 21);
    EXPECT_EQ(m->getGroup()[0]->getName(), "Kuibish");
    EXPECT_EQ(m->getGroup()[0]->getInitNumber(), 44);
    EXPECT_EQ(m->getGroup()[0]->getType(), 3);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Draw\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
    Staff st2();

};



TEST(creGroTesting, AddEmployeeNullEmployee)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(679,"Draw");
    cu->chResCreateStaff(21);
    Staff* st = (Staff*)(cu->getAllResourses()[21]);
    Group* m = (Group*)(cu->getAllResourses()[679]);
    ASSERT_THROW(cu->creGroAddEmloyee(21,679), logic_error);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Draw\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
}


TEST(creGroTesting, DeleteEmployee)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(679,"Draw");
    cu->chResCreateStaff(21, "Kuibish", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* st = (Staff*)(cu->getAllResourses()[21]);
    Group* m = (Group*)(cu->getAllResourses()[679]);

    cu->creGroAddEmloyee(21,679);

    cu->creGroDeleteEmloyee(21,679);

    EXPECT_EQ(m->getGroup().size(), 0);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Draw\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
};


TEST(creGroTesting, DeleteEmployeeNotExisting)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateGroup(679,"Draw");
    cu->chResCreateStaff(21, "Kuibish", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    cu->chResCreateStaff(212, "Kuibish2", 44, "borodach", "Ivan", "Nikolaevich", "kui_i", "director", "maths");
    Staff* st = (Staff*)(cu->getAllResourses()[21]);
    Group* m = (Group*)(cu->getAllResourses()[679]);
    cu->getProject(3);



    cu->creGroAddEmloyee(21,679);

    EXPECT_EQ(m->getGroup().size(), 1);

    ASSERT_THROW(cu->creGroDeleteEmloyee(212,679), logic_error);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {

        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Draw\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);
}


TEST(chResTesting, CheckCreateResourse)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateResourse(135,"nul", 56);
    Resourse* m = cu->getAllResourses()[135];
    //Resourse m("nul", 135, 56, 1);
    EXPECT_EQ(m->getId(), 135);
    EXPECT_EQ(m->getName(), "nul");
    EXPECT_EQ(m->getInitNumber(), 56);
    EXPECT_EQ(m->getType(), 0);
    EXPECT_EQ(cu->chResCreateResourse(135,"nul", 56),1);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"nul\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);

};

TEST(chResTesting, CheckCopyResourse)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateResourse(135,"nul", 56);
    Resourse* m = cu->getAllResourses()[cu->chResCopyResourse(135)];
    //Resourse m("nul", 135, 56, 1);
   // EXPECT_EQ(m->getId(), 135);
    EXPECT_EQ(m->getName(), "nul_copy");
    EXPECT_EQ(m->getInitNumber(), 56);
    EXPECT_EQ(m->getType(), 0);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"nul\"";
    mysql_query (connect,query.c_str());
    query = "delete from resourses where rname = \"nul_copy\"";
        mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(chResTesting, CheckChangeResourse)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->chResCreateResourse(135);
    cu->chResChangeResourse(11,135,"nul", 56);
    Resourse* m = cu->getAllResourses()[135];
    //Resourse m("nul", 135, 56, 1);
    EXPECT_EQ(m->getId(), 135);
    EXPECT_EQ(m->getName(), "nul");
    EXPECT_EQ(m->getInitNumber(), 56);
    EXPECT_EQ(m->getType(), 0);

    cu->chResCreateResourse(1351);
    EXPECT_EQ(cu->chResChangeResourse(11,1351,"nul", 56),1);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"nul\"";
    mysql_query (connect,query.c_str());
    mysql_close (connect);

};


TEST(chResTesting, CheckCreateid)
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    EXPECT_EQ(cu->chResCreateResourseId(),1);
    cu->chResCreateMaterials(1,"Ruble", 803, "russian ruble");
    Materials* m = (Materials*)(cu->getAllResourses()[1]);
    cu->addResToProject(3,3);
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 1);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 2);
    EXPECT_EQ(cu->chResCreateResourseId(),2);


    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where rname = \"Ruble\"";
    mysql_query (connect,query.c_str());
    query = "delete from resourses where rname = \"no_name\"";
        mysql_query (connect,query.c_str());
    mysql_close (connect);
}


TEST(rAssResTesting, RealAssTesting)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);

    cu->fillAllResourses();
    cu->fillAllResourses();
    cu->chResCreateMoney(77,"Ruble", 803000, "russian ruble");
  //  cout << "fi1"<<endl;
    cu->assResAssignResourseReal(77,999,111,31);
 //   cout << "fi2"<<endl;
    Project u = cu->getProject(999);
  //  cout << "fi3"<<endl;
    Money* m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 802969);
    EXPECT_EQ(m->getType(), 1);
//cout << "fi4"<<endl;
   // cu->clearProjects();
    cu->assResAssignResourseReal(77,999,110,1);
    //cout << "fi5"<<endl;
    m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 802968);
    EXPECT_EQ(m->getType(), 1);
    cu->clearProjects();
    cu->assResAssignResourseReal(77,999,110,1);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from realass";
        mysql_query (connect,query.c_str());
        query = "delete from resourses where id =77";
                mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(rAssResTesting, RealAssDeactivateTesting)
{

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where idres =77";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());


    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResourseReal(77,999,111,31);
    Project u = cu->getProject(999);
    Money* m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803-31);
    EXPECT_EQ(m->getType(), 1);

    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getReal()[0].getIsActive(),1);

    m = (Money*)(u.tasks[0].getReal()[0].getResourse());
    cu->assResDeactivateReal(77,999,111);
    //EXPECT_EQ(u.tasks[1].getReal().size(),0);
    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getReal()[0].getIsActive(),0);
    EXPECT_EQ(m->getInitNumber(), 803);
    cu->assResAssignResourseReal(77,999,111,31);
    cu->clearProjects();
    cu->assResDeactivateReal(77,999,111);
    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getReal()[0].getIsActive(),0);
    EXPECT_EQ(cu->getRes(77)->getInitNumber(),803);
    cu->assResAssignResourseReal(77,999,111,31);

    cu->clearProjects();
    cu->assResAssignResourseReal(77,999,110,31);
    cu->assResDeactivateReal(77,999,111);
    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[1].getReal()[0].getIsActive(),0);
    EXPECT_EQ(cu->getRes(77)->getInitNumber(),803-31);
    delete cu;


   query = "delete from realass";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());
    mysql_close (connect);
};


TEST(rAssResTesting, RealAssNotInTheList)
{

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    EXPECT_EQ(cu->assResAssignResourseReal(922,3,2,31),2);
    cu->assResAssignResourseReal(922,3,10,1);
    cu->assResAssignResourseReal(922,3,2,1);

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from realass";
        mysql_query (connect,query.c_str());
    mysql_close (connect);

};

TEST(rAssResTesting, RealAssExistingTesting)
{


    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where idres =77";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResourseReal(77,3,2,31);
    cu->assResAssignResourseReal(77,3,2,31);
    Project u = cu->getProject(3);
    Materials* m = (Materials*)(u.tasks[0].getReal()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803-62);
    EXPECT_EQ(m->getType(), 1);

    query = "delete from realass";
        mysql_query (connect,query.c_str());
    mysql_close (connect);

};

TEST(rAssResTesting, RealAssFewResourse)
{
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where idres =77";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    EXPECT_EQ(cu->assResAssignResourseReal(77,3,2,3122), 1);

    query = "delete from realass";
        mysql_query (connect,query.c_str());
    mysql_close (connect);

};

TEST(pAssResTesting, PlannedAssTesting)
{

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where idres =77";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());


    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
     cu->showProjectResourses(3);
    cu->assResAssignResoursePlanned(77,999,111,31);
    Project u = cu->getProject(999);
    Money* m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

    cu->assResAssignResoursePlanned(77,999,110,1);
    m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

query = "delete from planass";
        mysql_query (connect,query.c_str());
    mysql_close (connect);
};

TEST(pAssResTesting, PlannedAssDeletePlanned)
{
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where idres =77";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResoursePlanned(77,999,111,31);
    Project u = cu->getProject(999);
    Money* m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

    u = cu->getProject(999);
    EXPECT_EQ(u.tasks[0].getPlanned().size(),1);

   // m = (Money*)(u.tasks[0].getPlanned()[0].getResourse());
    cu->assResDeletePlanned(77,999,111);
    //EXPECT_EQ(u.tasks[1].getPlanned().size(),0);
    u = cu->getProject(999);
     EXPECT_EQ(u.tasks[0].getPlanned().size(),0);

    query = "delete from planass";
         mysql_query (connect,query.c_str());
     mysql_close (connect);
};

TEST(pAssResTesting, PlannedAssExistingTesting)
{

    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where idres =77";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    cu->assResAssignResoursePlanned(77,3,2,31);
    cu->assResAssignResoursePlanned(77,3,2,31);
    Project u = cu->getProject(3);
    Materials* m = (Materials*)(u.tasks[0].getPlanned()[0].getResourse());
    EXPECT_EQ(m->getDescription(), "russian ruble");
    EXPECT_EQ(m->getId(), 77);
    EXPECT_EQ(m->getName(), "Ruble");
    EXPECT_EQ(m->getInitNumber(), 803);
    EXPECT_EQ(m->getType(), 1);

   query = "delete from planass";
        mysql_query (connect,query.c_str());
    mysql_close (connect);


};

TEST(pAssResTesting, PlannedAssNotInTheList)
{
    MYSQL *connect;
    connect=mysql_init(NULL);
    if (!connect)
    {
    cout<<"MySQL testInitialization failed";

    }
    connect=mysql_real_connect(connect,SERVER, USER, PASSWORD,"resourses" ,0,NULL,0);
    if (connect)
    {
        //cout<<"connection Succeeded\n";
    }
    else
    {
        cout<<"connection failed\n";
    }
    string query = "delete from resourses where idres =77";
        mysql_query (connect,query.c_str());
        query = "insert into resourses values(77,803,1,\"Ruble\",\"russian ruble\",null,null,null,null,null)";
            mysql_query (connect,query.c_str());
            query = "insert into planass values(10,77,50000)";
                mysql_query (connect,query.c_str());
                query = "insert into realass values(110,77,50)";
                    mysql_query (connect,query.c_str());
                    query = "insert into realass values(111,922,3)";
                        mysql_query (connect,query.c_str());

    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);
    cu->fillAllResourses();
    EXPECT_EQ(cu->assResAssignResoursePlanned(922,3,10,31),2);
};

using namespace std;
/*
int main()
{

        DBInterface *db = new DBPatch;
       // DBPatch db;
        CUInterface *cu = new ControlUnit(db);
        cu->fillAllResourses();
     //   ControlUnit* cur = dynamic_cast<ControlUnit*>(cu);
         cout << "Hello World!" << endl;

         if (!cu->authCheckPassword("log2","pas2"))
            cout << "Authorization 1 successful"<< endl;
         else
             cout << "Authorization 1 failed!"<< endl;

         if (!cu->authCheckPassword("log1","pas2"))
            cout << "Authorization 2 successful"<< endl;
         else
             cout << "Authorization 2 failed!"<< endl;

         if (!cu->authCheckPassword("log4","pas4"))
            cout << "Authorization 3 successful"<< endl;
         else
             cout << "Authorization 3 failed!"<< endl;


         int r0 =cu->chResCreateResourseId();
         cu->chResCreateResourse(r0,"res",56);
 //       cout << *cu->getRes(r0)<< " )))" <<endl;
    //    cu->chResCreateStaff(0);

        int rmo =cu->chResCreateResourseId();
        cu->chResCreateMoney(rmo,"money",45,"much");
  //     cout << *cu->getRes(rmo)<< " )))" <<endl;
       cu->chResChangeMoney(11,rmo,"dollar",3578,"green");
  //    cout << *cu->getRes(rmo) <<endl;

      int rma =cu->chResCreateResourseId();
       cu->chResCreateMaterials(rma,"kirpich",48,"red");
     // cout << *cu->getRes(rma)<< " )))" <<endl;

      int k = cu->chResCreateResourseId();
      cu->chResCreateStaff( k,"an_ton",14,"hi","anton","antonovich","ivanov","visokaya");
 //     cout << *cu->getRes(k)<< " )))" <<endl;

     int g = cu->chResCreateResourseId();
      cu->chResCreateGroup(g,"group");
 //    cout << *cu->getRes(g)<< " )))cre" <<endl;

     cu->creGroAddEmloyee(k,g);
     cu->creGroAddEmloyee(20,g);
//      cout << *cu->getRes(g)<< " gr" <<endl;

      cu->chResChangeGroup(g,"gruppa");
  //    cout << *cu->getRes(g)<< " oldgroup" <<endl;

      int g1=cu->chResCopyResourse(g);
  //   cout << *cu->getRes(g1)<< " " <<endl;

     cu->chResChangeStaff(11111111,k,"sss",4,"head","semen","petrovich","simonov","zavuch","teacher");
  //   cout << *cu->getRes(k)<< " newstaff" <<endl;

 //    cout << *cu->getRes(g1)<< " newgroup" <<endl;

    cu->assResAssignResourseReal(rmo,2,3,3001);
    cu->assResAssignResourseReal(rmo,2,3,2);
    cu->assResAssignResourseReal(rmo,999,111,67);
    cu->assResAssignResourseReal(rma,2,3,25);
//    cu->assResAssignResourseReal(g,2,3,25);

    cu->assResAssignResoursePlanned(k,2,3,1);
    cu->assResAssignResoursePlanned(k,2,3,4);
    cu->assResAssignResoursePlanned(rmo,2,3,2);

    map<int,Resourse*> shar = cu->showAllResourses();
    map<int,int> shrra = cu->showResourseRealAssignments(rmo);
    map<int,int> shrpa = cu->showResoursePlannedAssignments(k);
    cu->assResDeactivateReal(rmo,2,3);
    cu->assResDeletePlanned(k,2,3);
    map<int,int> shrpa1 = cu->showResoursePlannedAssignments(k);
    cu->creGroDeleteEmloyee(k,g);
    cu->printAll();

    delete cu;
    return 0;
}

*/

int main(int argc, char* argv[])
{
    DBInterface *db = new DBPatch;
    CUInterface *cu = new ControlUnit(db);

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


