#-------------------------------------------------
#
# Project created by QtCreator 2018-12-26T15:14:41
#
#-------------------------------------------------

QT       += core gui

#CONFIG += console c++14
QMAKE_CXXFLAGS += -std=c++0x
#QMAKE_CXXFLAGS += -l sqlite3
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

LIBS += -lgcov
QMAKE_CXXFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0
QMAKE_LFLAGS += -g -Wall -fprofile-arcs -ftest-coverage -O0

QMAKE_LIBDIR_FLAGS +=-L/usr/include/mysql
QMAKE_LIBDIR_FLAGS +=-lmysqlclient
QMAKE_LIBDIR_FLAGS +=-I/usr/include/mysql

INCLUDEPATH += "googletest-release-1.8.1/googletest/include"
INCLUDEPATH += "googletest-release-1.8.1/googletest"
INCLUDEPATH += "../"

TARGET = GraficCU
TEMPLATE = app


SOURCES += main.cpp\
        model_main.cpp \
    ../PMResoursesQt/LibRes/Group.cpp \
    ../PMResoursesQt/LibRes/Materials.cpp \
    ../PMResoursesQt/LibRes/Money.cpp \
    ../PMResoursesQt/LibRes/PlannedAss.cpp \
    ../PMResoursesQt/LibRes/Project.cpp \
    ../PMResoursesQt/LibRes/RealAss.cpp \
    ../PMResoursesQt/LibRes/Resourse.cpp \
    ../PMResoursesQt/LibRes/Staff.cpp \
    ../PMResoursesQt/LibRes/Task.cpp \
    ../ControlUnit/ControlUnit.cpp \
    ../ControlUnit/CUInterface.cpp \
    ../ControlUnit/DBPatch.cpp \
    Testing.cpp \
    googletest-release-1.8.1/googletest/src/gtest.cc \
    googletest-release-1.8.1/googletest/src/gtest-all.cc \
    googletest-release-1.8.1/googletest/src/gtest-death-test.cc \
    googletest-release-1.8.1/googletest/src/gtest-filepath.cc \
    googletest-release-1.8.1/googletest/src/gtest-port.cc \
    googletest-release-1.8.1/googletest/src/gtest-printers.cc \
    googletest-release-1.8.1/googletest/src/gtest-test-part.cc \
    googletest-release-1.8.1/googletest/src/gtest-typed-test.cc

HEADERS  += model_main.h \
    ../PMResoursesQt/LibRes/Group.h \
    ../PMResoursesQt/LibRes/Materials.h \
    ../PMResoursesQt/LibRes/Money.h \
    ../PMResoursesQt/LibRes/PlannedAss.h \
    ../PMResoursesQt/LibRes/Project.h \
    ../PMResoursesQt/LibRes/RealAss.h \
    ../PMResoursesQt/LibRes/Resourse.h \
    ../PMResoursesQt/LibRes/Staff.h \
    ../PMResoursesQt/LibRes/Task.h \
    ../ControlUnit/ControlUnit.h \
    ../ControlUnit/CUInterface.h \
    ../ControlUnit/DBInterface.h \
    ../ControlUnit/DBPatch.h \
    ../../../../../usr/include/mysql/decimal.h \
    ../../../../../usr/include/mysql/errmsg.h \
    ../../../../../usr/include/mysql/keycache.h \
    ../../../../../usr/include/mysql/m_ctype.h \
    ../../../../../usr/include/mysql/m_string.h \
    ../../../../../usr/include/mysql/my_alloc.h \
    ../../../../../usr/include/mysql/my_attribute.h \
    ../../../../../usr/include/mysql/my_compiler.h \
    ../../../../../usr/include/mysql/my_config.h \
    ../../../../../usr/include/mysql/my_dbug.h \
    ../../../../../usr/include/mysql/my_dir.h \
    ../../../../../usr/include/mysql/my_getopt.h \
    ../../../../../usr/include/mysql/my_global.h \
    ../../../../../usr/include/mysql/my_global2.h \
    ../../../../../usr/include/mysql/my_list.h \
    ../../../../../usr/include/mysql/my_net.h \
    ../../../../../usr/include/mysql/my_pthread.h \
    ../../../../../usr/include/mysql/mysql.h \
    ../../../../../usr/include/mysql/mysql_com.h \
    ../../../../../usr/include/mysql/mysqld_ername.h \
    ../../../../../usr/include/mysql/mysqld_error.h \
    ../../../../../usr/include/mysql/mysql_embed.h \
    ../../../../../usr/include/mysql/mysql_time.h \
    ../../../../../usr/include/mysql/mysql_version.h \
    ../../../../../usr/include/mysql/my_sys.h \
    ../../../../../usr/include/mysql/my_xml.h \
    ../../../../../usr/include/mysql/plugin.h \
    ../../../../../usr/include/mysql/plugin_audit.h \
    ../../../../../usr/include/mysql/plugin_ftparser.h \
    ../../../../../usr/include/mysql/sql_common.h \
    ../../../../../usr/include/mysql/sql_state.h \
    ../../../../../usr/include/mysql/sslopt-case.h \
    ../../../../../usr/include/mysql/sslopt-longopts.h \
    ../../../../../usr/include/mysql/sslopt-vars.h \
    ../../../../../usr/include/mysql/typelib.h \
    ../../../../../usr/include/mysql/mysql/client_plugin.h \
    ../../../../../usr/include/mysql/mysql/innodb_priv.h \
    ../../../../../usr/include/mysql/mysql/plugin.h \
    ../../../../../usr/include/mysql/mysql/plugin_audit.h \
    ../../../../../usr/include/mysql/mysql/plugin_auth.h \
    ../../../../../usr/include/mysql/mysql/plugin_auth_common.h \
    ../../../../../usr/include/mysql/mysql/plugin_ftparser.h \
    ../../../../../usr/include/mysql/mysql/service_my_snprintf.h \
    ../../../../../usr/include/mysql/mysql/services.h \
    ../../../../../usr/include/mysql/mysql/service_thd_alloc.h \
    ../../../../../usr/include/mysql/mysql/service_thd_wait.h \
    ../../../../../usr/include/mysql/mysql/service_thread_scheduler.h \
    ../../../../../usr/include/mysql/mysql/thread_pool_priv.h \
    ../../../../../usr/include/mysql/mysql/psi/mysql_file.h \
    ../../../../../usr/include/mysql/mysql/psi/mysql_thread.h \
    ../../../../../usr/include/mysql/mysql/psi/psi.h \
    ../../../../../usr/include/mysql/mysql/psi/psi_abi_v1.h \
    ../../../../../usr/include/mysql/mysql/psi/psi_abi_v2.h

FORMS    += model_main.ui

OTHER_FILES += \
    ../PMResoursesQt/LibRes/Group.o \
    ../PMResoursesQt/LibRes/Materials.o \
    ../PMResoursesQt/LibRes/Money.o \
    ../PMResoursesQt/LibRes/PlannedAss.o \
    ../PMResoursesQt/LibRes/Project.o \
    ../PMResoursesQt/LibRes/RealAss.o \
    ../PMResoursesQt/LibRes/Resourse.o \
    ../PMResoursesQt/LibRes/ss.o \
    ../PMResoursesQt/LibRes/Staff.o \
    ../PMResoursesQt/LibRes/Task.o \
    ../PMResoursesQt/LibRes/LibRes.pro.user \
    ../PMResoursesQt/LibRes/Makefile


SUBDIRS += \
    ../PMResoursesQt/LibRes/LibRes.pro
