#include "model_main.h"
#include "ui_model_main.h"//

model_main::model_main(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::model_main)
{
    DBInterface *db = new DBPatch;
    cu = new ControlUnit(db);
    cu->fillAllResourses();
    allres = cu->getAllResourses();
 //   qDebug() <<  QString::fromStdString(cu->showAllResourses()[77]->getName());
    ui->setupUi(this);
    ui->showres->setEnabled(0);
    ui->showproj->setEnabled(0);
    ui->create->setEnabled(0);
    ui->addemploy->setEnabled(0);
    ui->delemploy->setEnabled(0);
    ui->changeres->setEnabled(0);
    ui->assign->setEnabled(0);
    ui->assignplan->setEnabled(0);
    ui->addrestoproj->setEnabled(0);
    ui->copyres->setEnabled(0);

}

model_main::~model_main()
{
 //   cu->saveAll();
    delete ui;
}


void model_main::on_showres_clicked()
{
    ui->listWidgetres->clear();
    allres = cu->getAllResourses();
     map<int,Resourse*>::iterator it = allres.begin();
    int i=1;
     QStringList lst;
    for(it; it!=allres.end(); it++)
    {
        lst << QString::number(it->second->getId()) + ": "+ QString::fromStdString(it->second->getName());

    }
    ui->listWidgetres->addItems(lst);
    ui->listWidgetres->sortItems();

    ui->statusBar->showMessage("Resourses updated");

}

void model_main::on_listWidgetres_itemClicked(QListWidgetItem *item)
{
    allres = cu->getAllResourses();
    map<int,Resourse*>::iterator it = allres.begin();
   int i=1;
    Resourse* res;
   for(it; it!=allres.end(); it++)
   {
       if(QString::fromStdString(it->second->getName()) == item->text().split(": ")[1])
       {
           res = it->second;
       }
   }
   map<int,int> realass =  cu->showResourseRealAssignments(res->getId());
   map<int,int> planass =  cu->showResoursePlannedAssignments(res->getId());

    ui->listWidgetassreal->clear();
   ui->listWidgetassplan->clear();

   if(realass.size() !=0)
    {
        map<int,int>::iterator it1 = realass.begin();
        QStringList lst1;
       for(it1; it1!=realass.end(); it1++)
       {

           lst1 << QString::number(it1->first) + ": "+ QString::number(it1->second);

       }
       ui->listWidgetassreal->addItems(lst1);
       ui->listWidgetassreal->sortItems();
   }

   if(planass.size() !=0)
   {
      map<int,int>::iterator it2 = planass.begin();
      QStringList lst2;
      for(it2; it2!= planass.end(); it2++)
      {
          lst2 << QString::number(it2->first) + ": "+ QString::number(it2->second);

      }
      ui->listWidgetassplan->addItems(lst2);
      ui->listWidgetassplan->sortItems();
   }

   QString bigstr;

   if(res->getType() == 1)
   {
       Money*m = dynamic_cast<Money*>(res);
       bigstr += "Money \n "+QString::fromStdString(m->getName())+ "\n id: " +
               QString::number(m->getId()) + "\nAmmount available: " + QString::number(m->getInitNumber()) +
               + "\nDescription: "+QString::fromStdString(m->getDescription());
   }
   if(res->getType() == 2)
   {
       Materials*m = dynamic_cast<Materials*>(res);
       bigstr += "Materials \n "+QString::fromStdString(m->getName())+ "\n id: " +
               QString::number(m->getId()) + "\nAmmount available: " + QString::number(m->getInitNumber()) +
               + "\nDescription: "+QString::fromStdString(m->getDescription());
   }
   if(res->getType() == 3)
   {
       Staff*m = dynamic_cast<Staff*>(res);
       bigstr += "Staff \n login: " +QString::fromStdString(m->getName())+ "\n id: " +
               QString::number(m->getId()) + "\nWorking hours a week available: " + QString::number(m->getInitNumber())
               + "\nName: " + QString::fromStdString(m->getNameS()) + "\nPatronynic: " +  QString::fromStdString(m->getPatronymic())
               +"\nSurname: " +  QString::fromStdString(m->getsurname()) + "\nPosition: " +  QString::fromStdString(m->getPosition())
               +"\nSpeciality: " +  QString::fromStdString(m->getSpeciality())+ "\nDescription: "
               + QString::fromStdString(m->getDescription());
   }
   if(res->getType() == 4)
   {
       Group*m = dynamic_cast<Group*>(res);
       bigstr += "Group \nName: "+QString::fromStdString(m->getName())+ "\n id: " +
               QString::number(m->getId()) + "\nWorking hours a week available: " + QString::number(m->getInitNumber())+
               "\nMembers: ";
       for(int i=0; i<m->getGroup().size();i++)
       {
           bigstr+= QString::number(m->getGroup()[i]->getId());
           bigstr+= ", ";
       }
   }
   if(res->getType() == 0)
   {
       bigstr += "Empty resourse \n \nName: "+QString::fromStdString(res->getName())+ "\n id: " +
               QString::number(res->getId()) + "\nAmmount available: " + QString::number(res->getInitNumber());
   }



    ui->additionalinfow->setPlainText(bigstr);
}


void model_main::on_create_clicked()
{
    try
    {
        if(ui->typew->text() == "0")
        {
            if((!ui->namew->text().isEmpty())&&(!ui->numberw->text().isEmpty()))
            {
                int id= cu-> chResCreateResourseId();
                if(cu->chResCreateResourse(id,ui->namew->text().toStdString(),ui->numberw->text().toInt()))
                    ui->statusBar->showMessage("Creation 0 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
        }
        if(ui->typew->text() == "1")
        {
            if((!ui->namew->text().isEmpty())&&(!ui->numberw->text().isEmpty()))
            {
                int id= cu-> chResCreateResourseId();
                if(!cu->chResCreateMoney(id,ui->namew->text().toStdString(),ui->numberw->text().toInt(),ui->descriptionw->text().toStdString()))
                    ui->statusBar->showMessage("Creation 1 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
        }
        if(ui->typew->text() == "2")
        {
            if((!ui->namew->text().isEmpty())&&(!ui->numberw->text().isEmpty()))
            {
                int id= cu-> chResCreateResourseId();
                if(!cu->chResCreateMaterials(id,ui->namew->text().toStdString(),ui->numberw->text().toInt(),ui->descriptionw->text().toStdString()))
                    ui->statusBar->showMessage("Creation 2 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
        }
        if(ui->typew->text() == "3")
        {
            if((!ui->namew->text().isEmpty())&&(!ui->numberw->text().isEmpty())&&(!ui->namesw->text().isEmpty())&&(!ui->surnamew->text().isEmpty()))
            {
                int id= cu-> chResCreateResourseId();
                if(!cu->chResCreateStaff(id,ui->namew->text().toStdString(),ui->numberw->text().toInt(),
                                         ui->descriptionw->text().toStdString(),ui->namesw->text().toStdString(),ui->patronymicw->text().toStdString(),
                                         ui->surnamew->text().toStdString(),ui->positionw->text().toStdString(),ui->specialityw->text().toStdString()))
                    ui->statusBar->showMessage("Creation 3 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
        }
        if(ui->typew->text() == "4")
        {
            if((!ui->namew->text().isEmpty()))
            {
                int id= cu-> chResCreateResourseId();
                if(!cu->chResCreateGroup(id,ui->namew->text().toStdString()))
                    ui->statusBar->showMessage("Creation 4 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
        }

        ui->namew->clear();
        ui->surnamew->clear();
        ui->namesw->clear();
        ui->numberw->clear();
        ui->descriptionw->clear();
        ui->patronymicw->clear();
        ui->positionw->clear();
        ui->typew->clear();
        ui->specialityw->clear();
    }
    catch(logic_error e)
    {
       QString ms =  QString::fromUtf8(e.what());
         ui->statusBar->showMessage(ms);
    }

}

int model_main::findEmp(string nameg,int idemp)
{
    map<int,Resourse*>::iterator it = allres.begin();
    Group *res = nullptr;
   for(it; it!=allres.end(); it++)
   {
       if(it->second->getName() == nameg)
       {
           res = dynamic_cast<Group*>(it->second);
       }
   }

   if(res!=nullptr)
   {
       for(int i=0;i<res->getGroup().size();i++)
       {
           if(res->getGroup()[i]->getId() == idemp)
               return i;
       }
   }
   return -1;
}

void model_main::on_addemploy_clicked()
{
    if(ui->listWidgetres->selectedItems().size()!=0)
    {
        allres = cu->getAllResourses();
        map<int,Resourse*>::iterator it = allres.begin();
        Resourse *res;
        for(it; it!=allres.end(); it++)
        {
            if(it->second->getName() == ui->listWidgetres->currentItem()->text().split(": ")[1].toStdString())
            {
                res = it->second;
            }
        }
        Staff* st;
        if(res->getType() == 4)
        {

            if((allres[ui->idmembersw->text().toInt()]!= nullptr)&&(allres[ui->idmembersw->text().toInt()]->getType() == 3))
            {
                try
                {
                    if(!cu->creGroAddEmloyee(ui->idmembersw->text().toInt(),res->getId()))
                        ui->statusBar->showMessage("Adding successful");
                    else
                        ui->statusBar->showMessage("Error");
                }
                catch(logic_error e)
                {
                   QString ms =  QString::fromUtf8(e.what());
                     ui->statusBar->showMessage(ms);
                }

            }
            else
                ui->statusBar->showMessage("It is not an employee id!");
        }
        else
            ui->statusBar->showMessage("Select a group!");
    }
    else
        ui->statusBar->showMessage("Select a group");
}


void model_main::on_delemploy_clicked()
{
    allres = cu->getAllResourses();
    map<int,Resourse*>::iterator it = allres.begin();
    Resourse *res;
   for(it; it!=allres.end(); it++)
   {
       if(it->second->getName() == ui->listWidgetres->currentItem()->text().split(": ")[1].toStdString())
       {
           res = it->second;
       }
   }
   Staff* st;
    if(res->getType() == 4)
    {
        int num;
        if(num=findEmp(res->getName(),ui->idmembersw->text().toInt())!=-1)
        {
            if(!cu->creGroDeleteEmloyee(ui->idmembersw->text().toInt(),res->getId()))
                ui->statusBar->showMessage("Deleting successful");
            else
                ui->statusBar->showMessage("Error!");
        }
        else
            ui->statusBar->showMessage("Error: wrong id!");
    }
}

void model_main::on_changeres_clicked()
{
    if((ui->listWidgetres->selectedItems().size() != 0)&&(ui->listWidgetres->selectedItems().size() != 0))
    {
        try
        {
            int id = 0;
            int type=0;
            allres = cu->getAllResourses();
            map<int,Resourse*>::iterator it = allres.begin();
            for(it; it!=allres.end(); it++)
            {
                if(it->second->getName() == ui->listWidgetres->currentItem()->text().split(": ")[1].toStdString())
                {
                    id = it->second->getId();
                    type = it->second->getType();
                }
            }

            if(type == 0)
            {
                int mask = 11;
                if(ui->namew->text().isEmpty())
                    mask = mask-10;
                if(ui->numberw->text().isEmpty())
                    mask = mask-1;

                if(!cu->chResChangeResourse(mask,id,ui->namew->text().toStdString(),ui->numberw->text().toInt()))
                    ui->statusBar->showMessage("Changing 0 successful");
                else
                    ui->statusBar->showMessage("Error");

            }
            if(type == 1)
            {
                int mask = 111;
                if(ui->namew->text().isEmpty())
                    mask = mask-100;
                if(ui->numberw->text().isEmpty())
                    mask = mask-10;
                if(ui->descriptionw->text().isEmpty())
                    mask = mask-1;

                if(!cu->chResChangeMoney(mask,id,ui->namew->text().toStdString(),ui->numberw->text().toInt(),ui->descriptionw->text().toStdString()))
                    ui->statusBar->showMessage("Changing 1 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
            if(type == 2)
            {
                int mask = 111;
                if(ui->namew->text().isEmpty())
                    mask = mask-100;
                if(ui->numberw->text().isEmpty())
                    mask = mask-10;
                if(ui->descriptionw->text().isEmpty())
                    mask = mask-1;

                if(!cu->chResChangeMaterials(mask,id,ui->namew->text().toStdString(),ui->numberw->text().toInt(),ui->descriptionw->text().toStdString()))
                    ui->statusBar->showMessage("Changing 2 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
            if(type == 3)
            {
                int mask = 11111111;
                if(ui->namew->text().isEmpty())
                    mask = mask-10000000;
                if(ui->numberw->text().isEmpty())
                    mask = mask-1000000;
                if(ui->descriptionw->text().isEmpty())
                    mask = mask-100000;
                if(ui->namesw->text().isEmpty())
                    mask = mask-10000;
                if(ui->patronymicw->text().isEmpty())
                    mask = mask-1000;
                if(ui->surnamew->text().isEmpty())
                    mask = mask-100;
                if(ui->positionw->text().isEmpty())
                    mask = mask-10;
                if(ui->specialityw->text().isEmpty())
                    mask = mask-1;


                if(!cu->chResChangeStaff(mask,id,ui->namew->text().toStdString(),ui->numberw->text().toInt(),
                                         ui->descriptionw->text().toStdString(),ui->namesw->text().toStdString(),ui->patronymicw->text().toStdString(),
                                         ui->surnamew->text().toStdString(),ui->positionw->text().toStdString(),ui->specialityw->text().toStdString()))
                    ui->statusBar->showMessage("Changing 3 successful");
                else
                    ui->statusBar->showMessage("Error");
            }
            if(type == 4)
            {
                if((!ui->namew->text().isEmpty()))
                {
                    if(!cu->chResChangeGroup(id,ui->namew->text().toStdString()))
                        ui->statusBar->showMessage("Changing 4 successful");
                    else
                        ui->statusBar->showMessage("Error");
                }
            }
            ui->namew->clear();
            ui->surnamew->clear();
            ui->namesw->clear();
            ui->numberw->clear();
            ui->descriptionw->clear();
            ui->patronymicw->clear();
            ui->positionw->clear();
            ui->typew->clear();
            ui->specialityw->clear();
        }
        catch(logic_error e)
        {
            QString ms =  QString::fromUtf8(e.what());
            ui->statusBar->showMessage(ms);
        }
    }
    else
        ui->statusBar->showMessage("Select a resourse");

}



void model_main::on_showproj_clicked()
{
    ui->listWidgetproj->clear();

    map<int,string> proj =  cu->showAllProjects();
    map<int,string>::iterator it = proj.begin();

     QStringList lst;
    for(it; it!=proj.end(); it++)
    {
        lst << QString::number(it->first) + ": "+ QString::fromStdString(it->second);

    }
    ui->listWidgetproj->addItems(lst);
    ui->listWidgetproj->sortItems();

    ui->statusBar->showMessage("Projects updated");
}

void model_main::on_listWidgetproj_itemClicked(QListWidgetItem *item)
{
    map<int,string> u ;
    QString bigstr;
    u = cu->showProjectTasks(item->text().split(": ")[0].toInt());
    ui->listWidgettask->clear();
    map<int,string>::iterator it = u.begin();

    QStringList lst;
    for(it; it!=u.end(); it++)
    {
        lst << QString::number(it->first) + ": "+ QString::fromStdString(it->second);

    }
    ui->listWidgettask->addItems(lst);
    ui->listWidgettask->sortItems();



    ui->listWidgetprojres->clear();
    map<int,Resourse*>all = cu->showProjectResourses(item->text().split(": ")[0].toInt());
   cout<<"listclicl"<<endl;
     map<int,Resourse*>::iterator it2 = all.begin();

    int i=1;
     lst.clear();

     it2++;
     cout<<"sec"<<it2->second->getName()<<endl;

    for(it2= all.begin(); it2 !=all.end(); it2++)
    {
        lst << QString::number(it2->first) + ": "+ QString::fromStdString(it2->second->getName());
        cout<<"listclicl a="<<it2->first<<": "<< it2->second->getName()<<"   "<<all.size()<<endl;
        i++;
    }
    ui->listWidgetprojres->addItems(lst);
    ui->listWidgetprojres->sortItems();
    cout<<"endlst "<<lst[0].toStdString()<<endl;
}



void model_main::on_assign_clicked()
{
    if((ui->listWidgettask->selectedItems().size() != 0)&&(ui->listWidgetprojres->selectedItems().size() != 0))
    {

            if((ui->assignw->text().isEmpty()) || (ui->assignw->text().toInt() == 0))
            {
                ui->statusBar->showMessage("Error: wrong ammount");
            }
            else
            {
                allres = cu->getAllResourses();
                if(ui->listWidgetprojres->currentItem()->text().split(": ")[0].toInt() == 0)
                {
                    if(allres[ui->listWidgetres->currentItem()->text().split(": ")[0].toInt()]->getType() >= 3)
                    {
                        int iff=cu->assResAssignResourseReal(ui->listWidgetres->currentItem()->text().split(": ")[0].toInt(),ui->listWidgetproj->currentItem()->text().split(": ")[0].toInt(),ui->listWidgettask->currentItem()->text().split(": ")[0].toInt(),ui->assignw->text().toInt());
                        if(iff == 0)
                            ui->statusBar->showMessage("Assignment successful");
                        else
                            if(iff == 1)
                                ui->statusBar->showMessage("Error: not enough resourse!");
                            else
                                if(iff == 2)
                                    ui->statusBar->showMessage("Error: resourse is not in the list!");
                    }
                    else
                        ui->statusBar->showMessage("Error: choose an employee below!");
                }
                else
                {
                    int iff=cu->assResAssignResourseReal(ui->listWidgetprojres->currentItem()->text().split(": ")[0].toInt(),ui->listWidgetproj->currentItem()->text().split(": ")[0].toInt(),ui->listWidgettask->currentItem()->text().split(": ")[0].toInt(),ui->assignw->text().toInt());
                    if(iff == 0)
                        ui->statusBar->showMessage("Assignment successful");
                    else
                        if(iff == 1)
                            ui->statusBar->showMessage("Error: not enough resourse!");
                        else
                            if(iff == 2)
                                ui->statusBar->showMessage("Error: resourse is not in the list!");
                }
            }
            //rui->showres->raise();
            ui->assignw->clear();
        }
    else
    ui->statusBar->showMessage("Error: choose task and resourse!");

}


void model_main::on_assignplan_clicked()
{
    if((ui->listWidgettask->selectedItems().size() != 0)&&(ui->listWidgetprojres->selectedItems().size() != 0))
    {
        if((ui->assignw->text().isEmpty()) || (ui->assignw->text().toInt() == 0))
        {
            ui->statusBar->showMessage("Error: wrong ammount");
        }
        else
        {
            allres = cu->getAllResourses();
            if(ui->listWidgetprojres->currentItem()->text().split(": ")[0].toInt() == 0)
            {
                if(allres[ui->listWidgetres->currentItem()->text().split(": ")[0].toInt()]->getType() >= 3)
                {
                    int iff=cu->assResAssignResoursePlanned(ui->listWidgetres->currentItem()->text().split(": ")[0].toInt(),ui->listWidgetproj->currentItem()->text().split(": ")[0].toInt(),ui->listWidgettask->currentItem()->text().split(": ")[0].toInt(),ui->assignw->text().toInt());
                    if(iff == 0)
                        ui->statusBar->showMessage("Assignment successful");
                    else
                        if(iff == 2)
                            ui->statusBar->showMessage("Error: resourse is not in the list!");

                }
                else
                    ui->statusBar->showMessage("Error: choose an employee below!");
            }
            else
            {
                int iff=cu->assResAssignResoursePlanned(ui->listWidgetprojres->currentItem()->text().split(": ")[0].toInt(),ui->listWidgetproj->currentItem()->text().split(": ")[0].toInt(),ui->listWidgettask->currentItem()->text().split(": ")[0].toInt(),ui->assignw->text().toInt());
                if(iff == 0)
                    ui->statusBar->showMessage("Assignment successful");
                else
                    if(iff == 2)
                        ui->statusBar->showMessage("Error: resourse is not in the list!");
            }
        }
        //rui->showres->raise();
        ui->assignw->clear();
    }
    else
        ui->statusBar->showMessage("Error: Select task and resourse!");


}


void model_main::on_pushButton_clicked()
{
    if(!cu->authCheckPassword(ui->loginw->text().toStdString(),ui->passwordw->text().toStdString()))
    {
        ui->statusBar->showMessage("Authentification successful");
        ui->showres->setEnabled(1);
        ui->showproj->setEnabled(1);
        ui->create->setEnabled(1);
        ui->addemploy->setEnabled(1);
        ui->delemploy->setEnabled(1);
        ui->changeres->setEnabled(1);
        ui->assign->setEnabled(1);
        ui->assignplan->setEnabled(1);
        ui->addrestoproj->setEnabled(1);
        ui->pushButton->setEnabled(0);
        ui->copyres->setEnabled(1);
    }
    else
    {
        ui->statusBar->showMessage("Error: Authentification failed!");
    }
}


void model_main::on_addrestoproj_clicked()
{
    if((ui->listWidgetproj->selectedItems().size() !=0)&&(ui->listWidgetres ->selectedItems().size() !=0))
    {
        if(!cu->addResToProject(ui->listWidgetproj->currentItem()->text().split(": ")[0].toInt(),ui->listWidgetres->currentItem()->text().split(": ")[0].toInt()))
            ui->statusBar->showMessage("Resourse added successfully");
        else
            ui->statusBar->showMessage("Error");
    }
    else
        ui->statusBar->showMessage("Nothing is chosen");
}

void model_main::on_copyres_clicked()
{
    if((ui->listWidgetres->selectedItems().size() != 0))
    {
        cu->chResCopyResourse(ui->listWidgetres->currentItem()->text().split(": ")[0].toInt());
    }
    ui->statusBar->showMessage("Resourse copied");
}

void model_main::on_delreal_clicked()
{
    if((ui->listWidgetassreal->selectedItems().size() ==0))
    {
        ui->statusBar->showMessage("Select an assignment");
    }
    else
    {

        map<int,string> proj = cu->showAllProjects();
        map<int,string>::iterator it = proj.begin();
        int idproj;

        for(it; it!=proj.end();it++)
        {
            map<int,string> task = cu->showProjectTasks(it->first);
            map<int,string>::iterator it1 = task.begin();
            for(it1; it1!=task.end();it1++)
            {
                if(it1->first == ui->listWidgetassreal->currentItem()->text().split(": ")[0].toInt())
                    idproj = it->first;
            }
        }


        cu->assResDeactivateReal(ui->listWidgetres->currentItem()->text().split(": ")[0].toInt(),idproj,ui->listWidgetassreal->currentItem()->text().split(": ")[0].toInt());
    }
}

void model_main::on_delplan_clicked()
{
    if((ui->listWidgetassplan->selectedItems().size() ==0))
    {
        ui->statusBar->showMessage("Select an assignment");
    }
    else
    {

        map<int,string> proj = cu->showAllProjects();
        map<int,string>::iterator it = proj.begin();
        int idproj;

        for(it; it!=proj.end();it++)
        {
            map<int,string> task = cu->showProjectTasks(it->first);
            map<int,string>::iterator it1 = task.begin();
            for(it1; it1!=task.end();it1++)
            {
                if(it1->first == ui->listWidgetassplan->currentItem()->text().split(": ")[0].toInt())
                    idproj = it->first;
            }
        }


        cu->assResDeletePlanned(ui->listWidgetres->currentItem()->text().split(": ")[0].toInt(),idproj,ui->listWidgetassplan->currentItem()->text().split(": ")[0].toInt());
    }
}
