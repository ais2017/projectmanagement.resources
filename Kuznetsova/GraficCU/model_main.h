#ifndef MODEL_MAIN_H
#define MODEL_MAIN_H

#include <QMainWindow>
#include<QDebug>
#include <QListWidget>

#include "../ControlUnit/CUInterface.h"
#include "../ControlUnit/ControlUnit.h"
#include "../ControlUnit/DBInterface.h"
#include "../ControlUnit/DBPatch.h"
#include <string.h>
#include <stdexcept>

namespace Ui {
class model_main;
}

class model_main : public QMainWindow
{
    Q_OBJECT

public:
    explicit model_main(QWidget *parent = 0);
    CUInterface *cu;
    map<int,Resourse*> allres;
    ~model_main();


private slots:
    void on_showres_clicked();

    void on_listWidgetres_itemClicked(QListWidgetItem *item);

    void on_create_clicked();


    void on_addemploy_clicked();

    void on_delemploy_clicked();

    
    void on_changeres_clicked();
    
    void on_showproj_clicked();

    void on_listWidgetproj_itemClicked(QListWidgetItem *item);


    void on_assign_clicked();

    void on_assignplan_clicked();

    void on_pushButton_clicked();

    void on_addrestoproj_clicked();

    void on_copyres_clicked();

    void on_delreal_clicked();
    
    void on_delplan_clicked();

private:
    Ui::model_main *ui;
    int findEmp(string nameg,int idemp);
};

#endif // MODEL_MAIN_H
